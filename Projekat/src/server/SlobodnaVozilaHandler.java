package server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import model.Rezervacije;
import model.Vozila;
import utils.URIUtil;

public class SlobodnaVozilaHandler implements HttpHandler {
	private List<Rezervacije> rezervacije;
	private List<Vozila> vozila;
	private ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * @param rezervacije
	 * @param vozila
	 */
	public SlobodnaVozilaHandler(List<Rezervacije> rezervacije, List<Vozila> vozila) {
		super();
		this.rezervacije = rezervacije;
		this.vozila = vozila;
	}
	
	/* (non-Javadoc)
	 * @see com.sun.net.httpserver.HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
	 */
	@Override
	public void handle(HttpExchange ex) throws IOException {
		// Dobavljanje podatka o metodu zahteva
		String reqMethod = ex.getRequestMethod();
		// Bez obzira koji je zahtev primljen moraju se podesiti zagavlja odgovora na zahtev
		Headers resHeaders = ex.getResponseHeaders();
		resHeaders.set("Content-Type", "application/json; charset=UTF-8");
		resHeaders.set("Access-Control-Allow-Origin", "*");
		resHeaders.set("Access-Control-Allow-Methods", "GET, POST");
		// URI nam je potreban da bismo izvadili parametre (ako postoje)
		URI reqURI = ex.getRequestURI();
		HashMap<String, String> params = URIUtil.queryToMap(reqURI); // mapa moze biti prazna
		Writer os = new OutputStreamWriter(ex.getResponseBody(), "UTF-8"); // priprema odgovora
		// Iscitavanje podataka iz tela zahteva ukoliko ga ima
		InputStream in = ex.getRequestBody();
		byte[] body = in.readAllBytes();
		String bodyStr = new String(body, "UTF-8");
		// U zavisnosti od tipa upita se obradjuju zahtevi
		switch (reqMethod) {
		case "GET":
			System.out.println("Obrada GET zahteva!");
			
			try {
				getSlobodnaVozila(ex, os, params);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("GET zahtev obradjen!");
			break;
		default:
			System.out.println("Neodgovarajuci metod zahteva!");
		}
		os.close(); // nakon zatvaranja stream-a se salje odgovor
	}
	
	private void getSlobodnaVozila(HttpExchange ex, Writer os, HashMap<String, String> params) throws JsonProcessingException, IOException {
		if (params.isEmpty()) {
			ex.sendResponseHeaders(200, mapper.writeValueAsString(vozila).getBytes().length);
			os.write(mapper.writeValueAsString(vozila));
		} else {  // Dobavi registracione tablice vozila cije se rezervacije poklapaju sa prosledjenim datumima nove rezervacije
			if (params.containsKey("datumPocetkaRezervacije") && params.containsKey("datumKrajaRezervacije")) {
				Integer brojac = 0;
				ArrayList<Rezervacije> preklopljeneRezervacije = new ArrayList<Rezervacije>();
				boolean postojiPreklapanje = false;
				for (Rezervacije rezervacija : rezervacije) {
					brojac = brojac + 1;
					if (rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
						String StringDatumPocetkaRezervacije = rezervacija.getDatumPocetkaRezervacije();
						String[] parsiranDatumPocetkaRezervacije = StringDatumPocetkaRezervacije.split("-");
						LocalDate DatumPocetkaRezervacije = LocalDate.of(Integer.parseInt(parsiranDatumPocetkaRezervacije[0]), 
														Integer.parseInt(parsiranDatumPocetkaRezervacije[1]),
														Integer.parseInt(parsiranDatumPocetkaRezervacije[2]));
						
						String StringDatumKrajaRezervacije = rezervacija.getDatumKrajaRezervacije();
						String[] parsiranDatumKrajaRezervacije = StringDatumKrajaRezervacije.split("-");
						LocalDate DatumKrajaRezervacije = LocalDate.of(Integer.parseInt(parsiranDatumKrajaRezervacije[0]), 
														Integer.parseInt(parsiranDatumKrajaRezervacije[1]),
														Integer.parseInt(parsiranDatumKrajaRezervacije[2]));
						
						String StringProsledjenDatumPocetkaRezervacije = params.get("datumPocetkaRezervacije");
						String[] parsiranProsledjenDatumPocetkaRezervacije = StringProsledjenDatumPocetkaRezervacije.split("-");
						LocalDate ProsledjenDatumPocetkaRezervacije = LocalDate.of(Integer.parseInt(parsiranProsledjenDatumPocetkaRezervacije[0]), 
														Integer.parseInt(parsiranProsledjenDatumPocetkaRezervacije[1]),
														Integer.parseInt(parsiranProsledjenDatumPocetkaRezervacije[2]));
						
						String StringProsledjenDatumKrajaRezervacije = params.get("datumKrajaRezervacije");
						String[] parsiranProsledjenDatumKrajaRezervacije = StringProsledjenDatumKrajaRezervacije.split("-");
						LocalDate ProsledjenDatumKrajaRezervacije = LocalDate.of(Integer.parseInt(parsiranProsledjenDatumKrajaRezervacije[0]), 
														Integer.parseInt(parsiranProsledjenDatumKrajaRezervacije[1]),
														Integer.parseInt(parsiranProsledjenDatumKrajaRezervacije[2]));
						
						System.out.println(brojac + ". rezervacija je aktivna.");
						
						
						if (ProsledjenDatumPocetkaRezervacije.isBefore(DatumPocetkaRezervacije) && 
							(ProsledjenDatumKrajaRezervacije.isAfter(DatumPocetkaRezervacije) || 
							ProsledjenDatumKrajaRezervacije.equals(DatumPocetkaRezervacije))) {
							
							postojiPreklapanje = true;
							System.out.println("preklapanje prvi uslov!");
							preklopljeneRezervacije.add(rezervacija);
						} else if ((ProsledjenDatumPocetkaRezervacije.isBefore(DatumKrajaRezervacije) ||
									ProsledjenDatumPocetkaRezervacije.equals(DatumKrajaRezervacije)) && 
									ProsledjenDatumKrajaRezervacije.isAfter(DatumKrajaRezervacije)) {
							
							postojiPreklapanje = true;
							System.out.println("preklapanje drugi uslov!");
							preklopljeneRezervacije.add(rezervacija);
						} else if ((ProsledjenDatumPocetkaRezervacije.isAfter(DatumPocetkaRezervacije) ||
									ProsledjenDatumPocetkaRezervacije.equals(DatumPocetkaRezervacije)) && 
									(ProsledjenDatumKrajaRezervacije.isBefore(DatumKrajaRezervacije) ||
									ProsledjenDatumKrajaRezervacije.equals(DatumKrajaRezervacije))) {
							
							postojiPreklapanje = true;
							System.out.println("preklapanje treci uslov!");
							preklopljeneRezervacije.add(rezervacija);
						} 
				} else {
					System.out.println(brojac + ". rezervacija nije aktivna!");
				}
					}
				if (!preklopljeneRezervacije.isEmpty()) {
					System.out.println("Ima aktivnih rezervacija");
					if (!postojiPreklapanje) {
						System.out.println("Nema preklapanja!");
						ex.sendResponseHeaders(200, mapper.writeValueAsString("Nema preklopljenih rezervacija.").getBytes().length);
						os.write(mapper.writeValueAsString("Nema preklopljenih rezervacija."));
					} else {
						System.out.println("Ima preklapanja!");
						ArrayList<String> preklopljeneRezervacijeRegistracioniBrojevi = new ArrayList<String>();
						for (Rezervacije rezervacija : preklopljeneRezervacije) {
							boolean postojiRegistracionaTablica = false;
							for (String registracionaTablica : preklopljeneRezervacijeRegistracioniBrojevi) {
								if (registracionaTablica.equals(rezervacija.getRezervisanoVozilo().getRegistracioniBroj())) {
									postojiRegistracionaTablica = true;
								}
							}
							if (!postojiRegistracionaTablica) {
								preklopljeneRezervacijeRegistracioniBrojevi.add(rezervacija.getRezervisanoVozilo().getRegistracioniBroj());
							}
						}
						ArrayList<Vozila> slobodniBiciklovi = new ArrayList<Vozila>();
						for (Vozila vozilo : vozila) {
							if(vozilo.getClass().getName().equals("model.Bicikli")) {
								if(vozilo.getLogickiObrisanoVozilo() == false) {
									System.out.println("Ispituje se bicikl sa registracionom tablicom: " + vozilo.getRegistracioniBroj() + ".");
									boolean biciklJeSlobodan = true;
									for (String zauzetaRegistracionaTablica : preklopljeneRezervacijeRegistracioniBrojevi) {
										System.out.println("Zauzeto je vozilo sa registracionom tablicom: " + zauzetaRegistracionaTablica + ".");
										if(vozilo.getRegistracioniBroj().equals(zauzetaRegistracionaTablica)) {
											System.out.println("Bicikl je zauzet.");
											biciklJeSlobodan = false;
										}
									}
									if(biciklJeSlobodan) {
										System.out.println("Bicikl je slobodan.");
										slobodniBiciklovi.add(vozilo);
									}
								}
							}
						}
						ArrayList<Vozila> slobodnaPutnickaVozila = new ArrayList<Vozila>();
						for (Vozila vozilo : vozila) {
							if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
								if(vozilo.getLogickiObrisanoVozilo() == false) {
									System.out.println("Ispituje se putnicko vozilo sa registracionom tablicom: " + vozilo.getRegistracioniBroj() + ".");
									boolean putnickoVoziloJeSlobodno = true;
									for (String zauzetaRegistracionaTablica : preklopljeneRezervacijeRegistracioniBrojevi) {
										System.out.println("Zauzeto je vozilo sa registracionom tablicom: " + zauzetaRegistracionaTablica + ".");
										if(vozilo.getRegistracioniBroj().equals(zauzetaRegistracionaTablica)) {
											System.out.println("Putnicko vozilo je zauzeto.");
											putnickoVoziloJeSlobodno = false;
										}
									}
									if(putnickoVoziloJeSlobodno) {
										System.out.println("Putnicko vozilo je slobodno.");
										slobodnaPutnickaVozila.add(vozilo);
									}
								}
							}
						}
						ArrayList<Vozila> slobodnaTeretnaVozila = new ArrayList<Vozila>();
						for (Vozila vozilo : vozila) {
							if(vozilo.getClass().getName().equals("model.TeretnoVozilo")) {
								if(vozilo.getLogickiObrisanoVozilo() == false) {
									System.out.println("Ispituje se teretno vozilo sa registracionom tablicom: " + vozilo.getRegistracioniBroj() + ".");
									boolean teretnoVoziloJeSlobodno = true;
									for (String zauzetaRegistracionaTablica : preklopljeneRezervacijeRegistracioniBrojevi) {
										System.out.println("Zauzeto je vozilo sa registracionom tablicom: " + zauzetaRegistracionaTablica + ".");
										if(vozilo.getRegistracioniBroj().equals(zauzetaRegistracionaTablica)) {
											System.out.println("Teretno vozilo je zauzeto.");
											teretnoVoziloJeSlobodno = false;
										}
									}
									if(teretnoVoziloJeSlobodno) {
										System.out.println("Teretno vozilo je slobodno.");
										slobodnaTeretnaVozila.add(vozilo);
									}
								}
							}
						}
						
						if (params.containsKey("vrstaVozila")) {
							if(params.get("vrstaVozila").equals("Bicikl")) {
								System.out.println("Trazeni su biciklovi.");
								if(!slobodniBiciklovi.isEmpty()) {
									ex.sendResponseHeaders(200, mapper.writeValueAsString(slobodniBiciklovi).getBytes().length);
									os.write(mapper.writeValueAsString(slobodniBiciklovi));
								} else {
									ex.sendResponseHeaders(200, mapper.writeValueAsString("Svi biciklovi su zauzeti.").getBytes().length);
									os.write(mapper.writeValueAsString("Svi biciklovi su zauzeti."));
								}
							} else if (params.get("vrstaVozila").equals("PutnickoVozilo")) {
								System.out.println("Trazena su putnicka vozila.");
								if(!slobodnaPutnickaVozila.isEmpty()) {
									ex.sendResponseHeaders(200, mapper.writeValueAsString(slobodnaPutnickaVozila).getBytes().length);
									os.write(mapper.writeValueAsString(slobodnaPutnickaVozila));
								} else {
									ex.sendResponseHeaders(200, mapper.writeValueAsString("Sva putnicka vozila su zauzeta.").getBytes().length);
									os.write(mapper.writeValueAsString("Sva putnicka vozila su zauzeta."));
								}
							} else {
								System.out.println("Trazena su teretna vozila.");
								if(!slobodnaTeretnaVozila.isEmpty()) {
									ex.sendResponseHeaders(200, mapper.writeValueAsString(slobodnaTeretnaVozila).getBytes().length);
									os.write(mapper.writeValueAsString(slobodnaTeretnaVozila));
								} else {
									ex.sendResponseHeaders(200, mapper.writeValueAsString("Sva teretna vozila su zauzeta.").getBytes().length);
									os.write(mapper.writeValueAsString("Sva teretna vozila su zauzeta."));
								}
							}
						} else {
							ex.sendResponseHeaders(200, mapper.writeValueAsString("Ima preklopljenih rezervacija.").getBytes().length);
							os.write(mapper.writeValueAsString("Ima preklopljenih rezervacija."));
						}
					}
				} else {
					System.out.println("Ima aktivnih rezervacija, ali nema preklapanja!");
					if (params.containsKey("vrstaVozila")) {
						if(params.get("vrstaVozila").equals("Bicikl")) {
							ArrayList<Vozila> slobodniBiciklovi2 = new ArrayList<Vozila>();
							for (Vozila vozilo : vozila) {
								if(vozilo.getClass().getName().equals("model.Bicikli") && vozilo.getLogickiObrisanoVozilo() == false) {
									slobodniBiciklovi2.add(vozilo);
								}
							}
							if(slobodniBiciklovi2.isEmpty()) {
								ex.sendResponseHeaders(200, mapper.writeValueAsString("Svi biciklovi su 'obrisani'.").getBytes().length);
								os.write(mapper.writeValueAsString("Svi biciklovi su 'obrisani'."));
							} else {
								System.out.println("Svi biciklovi su slobodni sto se tice preklapanja vremena."
										+ "Nisu slobodni oni koji su kao vozila 'obrisana'.");
								ex.sendResponseHeaders(200, mapper.writeValueAsString(slobodniBiciklovi2).getBytes().length);
								os.write(mapper.writeValueAsString(slobodniBiciklovi2));
							}
						} else if (params.get("vrstaVozila").equals("PutnickoVozilo")) {
							ArrayList<Vozila> slobodnaPutnickaVozila2 = new ArrayList<Vozila>();
							for (Vozila vozilo : vozila) {
								if(vozilo.getClass().getName().equals("model.PutnickoVozilo") && vozilo.getLogickiObrisanoVozilo() == false) {
									slobodnaPutnickaVozila2.add(vozilo);
								}
							}
							if(slobodnaPutnickaVozila2.isEmpty()) {
								ex.sendResponseHeaders(200, mapper.writeValueAsString("Sva putnicka vozila su 'obrisana'.").getBytes().length);
								os.write(mapper.writeValueAsString("Sva putnicka vozila su 'obrisana'."));
							} else {
								System.out.println("Sva putnicka vozila su slobodna sto se tice preklapanja vremena."
										+ "Nisu slobodna ona koja su kao vozila 'obrisana'.");
								ex.sendResponseHeaders(200, mapper.writeValueAsString(slobodnaPutnickaVozila2).getBytes().length);
								os.write(mapper.writeValueAsString(slobodnaPutnickaVozila2));
							}
						} else {
							ArrayList<Vozila> slobodnaTeretnaVozila2 = new ArrayList<Vozila>();
							for (Vozila vozilo : vozila) {
								if(vozilo.getClass().getName().equals("model.TeretnoVozilo") && vozilo.getLogickiObrisanoVozilo() == false) {
									slobodnaTeretnaVozila2.add(vozilo);
								}
							}
							if(slobodnaTeretnaVozila2.isEmpty()) {
								ex.sendResponseHeaders(200, mapper.writeValueAsString("Sva teretna vozila su 'obrisana'.").getBytes().length);
								os.write(mapper.writeValueAsString("Sva teretna vozila su 'obrisana'."));
							} else {
								System.out.println("Sva teretna vozila su slobodna sto se tice preklapanja vremena."
										+ "Nisu slobodna ona koja su kao vozila 'obrisana'.");
								ex.sendResponseHeaders(200, mapper.writeValueAsString(slobodnaTeretnaVozila2).getBytes().length);
								os.write(mapper.writeValueAsString(slobodnaTeretnaVozila2));
							}
						}
					} else {
						ex.sendResponseHeaders(200, mapper.writeValueAsString("Ima aktivnih rezervacija, ali nema preklapanja. Posaljite vrstu vozila za vise informacija!").getBytes().length);
						os.write(mapper.writeValueAsString("Ima aktivnih rezervacija, ali nema preklapanja. Posaljite vrstu vozila za vise informacija!"));
					}
				}
			} else if(params.containsKey("registracionaTablica")) {
				String registracionaTablica = params.get("registracionaTablica");
				boolean tablicaJeSlobodna = true;

				for(Vozila vozilo : vozila) {
					if(vozilo.getRegistracioniBroj().equals(registracionaTablica)) {
						tablicaJeSlobodna = false;
						System.out.println("Registraciona tablica: " + vozilo.getRegistracioniBroj() + " je zauzeta.");
						ex.sendResponseHeaders(200, mapper.writeValueAsString("Registraciona tablica je zauzeta.").getBytes().length);
						os.write(mapper.writeValueAsString("Registraciona tablica je zauzeta."));
					}
				}
				
				if(tablicaJeSlobodna) {
					System.out.println("Registraciona tablica: " + registracionaTablica + " je slobodna.");
					ex.sendResponseHeaders(200, mapper.writeValueAsString("Registraciona tablica je slobodna.").getBytes().length);
					os.write(mapper.writeValueAsString("Registraciona tablica je slobodna."));
				}	
				
			} else {
				ex.sendResponseHeaders(400, 0);
				os.write("");
			}	
		}
	}
	
}
