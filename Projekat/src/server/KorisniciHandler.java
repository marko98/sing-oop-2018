package server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import liste.KorisniciLista;
import model.IznajmljivaciVozila;
import model.Korisnici;
import utils.URIUtil;

public class KorisniciHandler implements HttpHandler {
	private List<Korisnici> korisnici;
	private ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * @param korisnici
	 */
	public KorisniciHandler(List<Korisnici> korisnici) {
		super();
		this.korisnici = korisnici;
	}

	/* (non-Javadoc)
	 * @see com.sun.net.httpserver.HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
	 */
	public void handle(HttpExchange ex) throws IOException {
		// Dobavljanje podatka o metodu zahteva
		String reqMethod = ex.getRequestMethod();
		// Bez obzira koji je zahtev primljen moraju se podesiti zagavlja odgovora na zahtev
		Headers resHeaders = ex.getResponseHeaders();
		resHeaders.set("Content-Type", "application/json; charset=UTF-8");
		resHeaders.set("Access-Control-Allow-Origin", "*");
		resHeaders.set("Access-Control-Allow-Methods", "GET, POST");
		// URI nam je potreban da bismo izvadili parametre (ako treba)
		URI reqURI = ex.getRequestURI();
		HashMap<String, String> params = URIUtil.queryToMap(reqURI); // mapa moze biti prazna
		Writer os = new OutputStreamWriter(ex.getResponseBody(), "UTF-8"); // priprema odgovora
		// Iscitavanje podataka iz tela zahteva ukoliko ga ima
		InputStream in = ex.getRequestBody();
		byte[] body = in.readAllBytes();
		String bodyStr = new String(body, "UTF-8");
		// U zavisnosti od tipa upita se obradjuju zahtevi
		switch (reqMethod) {
		case "GET":
			System.out.println("Obrada GET zahteva!");
			
			try {
				getKorisnici(ex, os, params);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("GET zahtev obradjen!");
			break;
		case "POST":
			System.out.println("Obrada POST zahteva!");
			try {
				postKorisnici(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("POST zahtev obradjen!");
			break;
		default:
			System.out.println("Neodgovarajuci metod zahteva!");
		}
		os.close(); // nakon zatvaranja stream-a se salje odgovor
	}
	
	private void getKorisnici(HttpExchange ex, Writer os, HashMap<String, String> params) throws JsonProcessingException, IOException {
		if (params.isEmpty()) {
			ex.sendResponseHeaders(200, mapper.writeValueAsString(korisnici).getBytes().length);
			os.write(mapper.writeValueAsString(korisnici)); 
		} else {
			if (params.containsKey("korisnickoIme") && params.containsKey("sifra")) {
				
				String korisnickoIme = params.get("korisnickoIme");
				String sifra = params.get("sifra");
				boolean pronadjenKorisnik = false;
				
				for(Korisnici korisnik : korisnici) {
					if(korisnik.getKorisnickoIme().equals(korisnickoIme) && korisnik.getSifra().equals(sifra)) {
						pronadjenKorisnik = true;
						if(korisnik.getClass().getName().equals("model.Sluzbenici")) {
							System.out.println("Pronadjen je sluzbenik: " + korisnik.getIme() + " " + korisnik.getPrezime());
							ex.sendResponseHeaders(200, mapper.writeValueAsString(korisnik.getIme() + "," + korisnik.getPrezime() + "," + "Sluzbenik" + "," + korisnik.getKorisnickoIme()).getBytes().length);
							os.write(mapper.writeValueAsString(korisnik.getIme() + "," + korisnik.getPrezime() + "," + "Sluzbenik" + "," + korisnik.getKorisnickoIme())); 
						} else {
							System.out.println("Pronadjen je iznajmljivac vozila: " + korisnik.getIme() + " " + korisnik.getPrezime());
							ex.sendResponseHeaders(200, mapper.writeValueAsString(korisnik.getIme() + "," + korisnik.getPrezime() + "," + "Iznajmljivac vozila" + "," + korisnik.getKorisnickoIme()).getBytes().length);
							os.write(mapper.writeValueAsString(korisnik.getIme() + "," + korisnik.getPrezime() + "," + "Iznajmljivac vozila" + "," + korisnik.getKorisnickoIme())); 
						}
					}
				}
				if(!pronadjenKorisnik) {
					System.out.println("Nije pronadjen korisnik.");
					ex.sendResponseHeaders(200, mapper.writeValueAsString("Nije pronadjen korisnik.").getBytes().length);
					os.write(mapper.writeValueAsString("Nije pronadjen korisnik.")); 
				}
			} else if (params.containsKey("korisnickoIme")) {
				String korisnickoIme = params.get("korisnickoIme");
				
				boolean pronadjenKorisnik = false;
				
				for(Korisnici korisnik : korisnici) {
					if(korisnik.getKorisnickoIme().equals(korisnickoIme)) {
						pronadjenKorisnik = true;
						System.out.println("Pronadjen je korisnik: " + korisnik.getIme() + " " + korisnik.getPrezime());
						ex.sendResponseHeaders(200, mapper.writeValueAsString(korisnik).getBytes().length);
						os.write(mapper.writeValueAsString(korisnik)); 
					}
				}
				if(!pronadjenKorisnik) {
					System.out.println("Nije pronadjen korisnik.");
					ex.sendResponseHeaders(200, mapper.writeValueAsString("Nije pronadjen korisnik.").getBytes().length);
					os.write(mapper.writeValueAsString("Nije pronadjen korisnik.")); 
				}
				
			}
		}
	}
	private void postKorisnici(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {
		IznajmljivaciVozila reqIznajmljivacVozila = mapper.readValue(bodyStr, IznajmljivaciVozila.class);
		// Dodati samo ako u listi ne postoji vec iznajmljivac sa tim korisnickim imenom
		boolean duplicate = false;
		
		for (Korisnici korisnik : korisnici) {
			if (korisnik.getKorisnickoIme().equals(reqIznajmljivacVozila.getKorisnickoIme())) {
				// Pokusaj da se doda iznajmljivac sa korisnickim imenom koje vec postoji
				duplicate = true;
				break;
			}
		}

		if (!duplicate) {
			korisnici.add(reqIznajmljivacVozila);
			KorisniciLista listaKorisnika = new KorisniciLista(new ArrayList<Korisnici>(korisnici));
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);	
			mapper.writeValue(new File("korisnici.json"), listaKorisnika);
			
			ex.sendResponseHeaders(200, "Server: primljeni su podaci i dodati u listu iznajmljivaca vozila".getBytes().length);
			os.write("Server: primljeni su podaci i dodati u listu iznajmljivaca vozila");
			}
		else {
			System.out.println("Duplikat - iznajmljivaci vozila");  
			
			ex.sendResponseHeaders(200, "Server: duplikat - vec postoji iznajmljivac vozila sa istim korisnickim imenom".getBytes().length);
			os.write("Server: duplikat - vec postoji iznajmljivac vozila sa istim korisnickim imenom");
		}
		os.close();		
	}
}
	