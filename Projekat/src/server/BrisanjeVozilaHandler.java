package server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import liste.VozilaLista;
import model.Rezervacije;
import model.Vozila;
import utils.URIUtil;

public class BrisanjeVozilaHandler implements HttpHandler {
	private List<Rezervacije> rezervacije;
	private List<Vozila> vozila;
	private ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * @param rezervacije
	 * @param vozila
	 */
	public BrisanjeVozilaHandler(List<Rezervacije> rezervacije, List<Vozila> vozila) {
		super();
		this.rezervacije = rezervacije;
		this.vozila = vozila;
	}
	
	/* (non-Javadoc)
	 * @see com.sun.net.httpserver.HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
	 */
	@Override
	public void handle(HttpExchange ex) throws IOException {
		// Dobavljanje podatka o metodu zahteva
		String reqMethod = ex.getRequestMethod();
		// Bez obzira koji je zahtev primljen moraju se podesiti zagavlja odgovora na zahtev
		Headers resHeaders = ex.getResponseHeaders();
		resHeaders.set("Content-Type", "application/json; charset=UTF-8");
		resHeaders.set("Access-Control-Allow-Origin", "*");
		resHeaders.set("Access-Control-Allow-Methods", "GET, POST, PUT");
		// URI nam je potreban da bismo izvadili parametre (ako postoje)
		URI reqURI = ex.getRequestURI();
		HashMap<String, String> params = URIUtil.queryToMap(reqURI); // mapa moze biti prazna
		Writer os = new OutputStreamWriter(ex.getResponseBody(), "UTF-8"); // priprema odgovora
		// Iscitavanje podataka iz tela zahteva ukoliko ga ima
		InputStream in = ex.getRequestBody();
		byte[] body = in.readAllBytes();
		String bodyStr = new String(body, "UTF-8");
		// U zavisnosti od tipa upita se obradjuju zahtevi
		switch (reqMethod) {
		case "GET":
			System.out.println("Obrada GET zahteva!");
			try {
				getBrisanjeVozila(ex, os, params);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("GET zahtev obradjen!");
			break;
		case "POST":
			System.out.println("Obrada POST zahteva!");
			try {
				postBrisanjeVozila(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("POST zahtev obradjen!");
			break;
		case "PUT":
			System.out.println("Obrada PUT zahteva!");
			try {
				putBrisanjeVozila(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case "OPTIONS":
			System.out.println("Obrada OPITONS zahteva.");
			ex.sendResponseHeaders(200, 0);
			os.write("");
			System.out.println("OPTIONS zahtev obradjen!");
			break;
		default:
			System.out.println("Neodgovarajuci metod zahteva!");
			ex.sendResponseHeaders(400, 0);
			os.write("");
			System.out.println("Obradjen neodgovarajuci metod zahteva");
		}
		os.close(); // nakon zatvaranja stream-a se salje odgovor
	}
	
	private void getBrisanjeVozila(HttpExchange ex, Writer os, HashMap<String, String> params) throws JsonProcessingException, IOException {
		ArrayList<String> tablicePostojecihVozila = new ArrayList<String>();
		ArrayList<Vozila> slobodnaVozila = new ArrayList<Vozila>();
		for(Vozila vozilo : vozila) {
			if(vozilo.getLogickiObrisanoVozilo() == false) {
				tablicePostojecihVozila.add(vozilo.getRegistracioniBroj());
			}
		}
		for (String tablicaPostojecegVozila : tablicePostojecihVozila) {
			boolean voziloImaRezervacija = false;
			for (Rezervacije rezervacija : rezervacije) {
				if(rezervacija.getRezervisanoVozilo().getRegistracioniBroj().equals(tablicaPostojecegVozila)
						&& rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
					voziloImaRezervacija = true;
				}
			}
			if(!voziloImaRezervacija) {
				for(Vozila vozilo : vozila) {
					if(vozilo.getRegistracioniBroj().equals(tablicaPostojecegVozila)) {
						slobodnaVozila.add(vozilo);
					}
				}
			}
		}
		if(slobodnaVozila.isEmpty()) {
			System.out.println("Nema slobodnih vozila.");
			ex.sendResponseHeaders(200, mapper.writeValueAsString("Nema slobodnih vozila.").getBytes().length);
			os.write(mapper.writeValueAsString("Nema slobodnih vozila."));
		} else {
			System.out.println("Ima slobodnih vozila.");
			ex.sendResponseHeaders(200, mapper.writeValueAsString(slobodnaVozila).getBytes().length);
			os.write(mapper.writeValueAsString(slobodnaVozila));
		}
	}
	
	private void putBrisanjeVozila(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {
		
		String registracionaTablica = bodyStr;
	
		
		System.out.println(registracionaTablica);
		
		for(Vozila vozilo : vozila) {
			if(vozilo.getRegistracioniBroj().equals(registracionaTablica)) {
				vozilo.setLogickiObrisanoVozilo(true);
			}
		}
		
		VozilaLista listaVozila = new VozilaLista(new ArrayList<Vozila>(vozila));
		ObjectMapper omVozila = new ObjectMapper();
		omVozila.enable(SerializationFeature.INDENT_OUTPUT);
		File fajlVozila = new File("vozila.json");
		omVozila.writeValue(fajlVozila, listaVozila	);
		
		System.out.println("Vozilo je obrisano.");
		ex.sendResponseHeaders(200, mapper.writeValueAsString("Vozilo je obrisano.").getBytes().length);
		os.write(mapper.writeValueAsString("Vozilo je obrisano."));
	
	}
	
	private void postBrisanjeVozila(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {
	
	}
}
	