/**
 * 
 */
package server;

import java.io.File;
import java.io.IOException;
import java.net.InetSocketAddress;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.sun.net.httpserver.HttpServer;

import liste.GorivaLista;
import liste.KorisniciLista;
import liste.RezervacijeLista;
import liste.ServisiranjaLista;
import liste.VozilaLista;

public class App {

	/**
	 * @param args
	 * @throws IOException 
	 * @throws ClassNotFoundException 
	 */
	public static void main(String[] args) throws IOException, ClassNotFoundException {
			
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);
		
		KorisniciLista korisnici = mapper.readValue(new File("korisnici.json"), KorisniciLista.class);
		VozilaLista vozila = mapper.readValue(new File("vozila.json"), VozilaLista.class);
		RezervacijeLista rezervacije = mapper.readValue(new File("rezervacije.json"), RezervacijeLista.class);
		ServisiranjaLista servisiranja = mapper.readValue(new File("servisiranja.json"), ServisiranjaLista.class);
		GorivaLista goriva = mapper.readValue(new File("goriva.json"), GorivaLista.class);
		
		// Kreiranje servera
		HttpServer server = HttpServer.create(new InetSocketAddress(8080), 0);
		
		server.createContext("/korisnici", new KorisniciHandler(korisnici.getKorisnici()));
		server.createContext("/vozila", new VozilaHandler(vozila.getVozila()));
		server.createContext("/rezervacije", new RezervacijeHandler(rezervacije.getRezervacije()));
		server.createContext("/slobodnaVozila", new SlobodnaVozilaHandler(rezervacije.getRezervacije(), vozila.getVozila()));
		server.createContext("/racunanjeTroskova", new RacunanjeTroskovaHandler(vozila.getVozila()));
		server.createContext("/vracanjeVozila", new VracanjeVozilaHandler(rezervacije.getRezervacije(), vozila.getVozila(), servisiranja.getServisiranja()));
		server.createContext("/goriva", new GorivaHandler(goriva.getGoriva()));
		server.createContext("/brisanjeVozila", new BrisanjeVozilaHandler(rezervacije.getRezervacije(), vozila.getVozila()));
		
		server.setExecutor(null); // podrazumevani executor
		server.start();
		System.out.println("Server slusa na portu 8080!");
	}
}
