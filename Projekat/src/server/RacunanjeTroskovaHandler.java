package server;

import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import model.Bicikli;
import model.PutnickoVozilo;
import model.TeretnoVozilo;
import model.Vozila;
import utils.URIUtil;

public class RacunanjeTroskovaHandler implements HttpHandler {
	private List<Vozila> vozila;

	private ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * @param vozila
	 */
	public RacunanjeTroskovaHandler(List<Vozila> vozila) {
		super();
		this.vozila = vozila;
	}
	
	/* (non-Javadoc)
	 * @see com.sun.net.httpserver.HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
	 */
	@Override
	public void handle(HttpExchange ex) throws IOException {
		// Dobavljanje podatka o metodu zahteva
		String reqMethod = ex.getRequestMethod();
		// Bez obzira koji je zahtev primljen moraju se podesiti zagavlja odgovora na zahtev
		Headers resHeaders = ex.getResponseHeaders();
		resHeaders.set("Content-Type", "application/json; charset=UTF-8");
		resHeaders.set("Access-Control-Allow-Origin", "*");
		resHeaders.set("Access-Control-Allow-Methods", "GET, POST");
		// URI nam je potreban da bismo izvadili parametre (ako postoje)
		URI reqURI = ex.getRequestURI();
		HashMap<String, String> params = URIUtil.queryToMap(reqURI); // mapa moze biti prazna
		Writer os = new OutputStreamWriter(ex.getResponseBody(), "UTF-8"); // priprema odgovora
		// Iscitavanje podataka iz tela zahteva ukoliko ga ima
		InputStream in = ex.getRequestBody();
		byte[] body = in.readAllBytes();
		String bodyStr = new String(body, "UTF-8");
		// U zavisnosti od tipa upita se obradjuju zahtevi
		switch (reqMethod) {
		case "GET":
			System.out.println("Obrada GET zahteva!");
			
			try {
				getRacunanjeTroskova(ex, os, params);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("GET zahtev obradjen!");
			break;
		case "POST":
			System.out.println("Obrada POST zahteva!");
			try {
				postRacunanjeTroskova(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("POST zahtev obradjen!");
			break;
		default:
			System.out.println("Neodgovarajuci metod zahteva!");
		}
		os.close(); // nakon zatvaranja stream-a se salje odgovor
	}
	
	private void getRacunanjeTroskova(HttpExchange ex, Writer os, HashMap<String, String> params) throws JsonProcessingException, IOException {
		if (params.isEmpty()) {
			ex.sendResponseHeaders(200, mapper.writeValueAsString("Nisu prosledjeni odredjeni parametri.").getBytes().length);
			os.write(mapper.writeValueAsString("Nisu prosledjeni odredjeni parametri."));
		} else {  // Dobavi registracione tablice vozila cije se rezervacije poklapaju sa prosledjenim datumima nove rezervacije
			if (params.containsKey("planiraniKilometri") && params.containsKey("registracioneTablice")
					&& params.containsKey("brojDanaRezervacije")) {
				
				System.out.println("Broj dana rezervacije je: " + params.get("brojDanaRezervacije"));
				
				String planiraniKilometri = params.get("planiraniKilometri");
				double kilometri = Double.parseDouble(planiraniKilometri);
				
				int brojDana = Integer.parseInt(params.get("brojDanaRezervacije"));
				
				String listaTablica = params.get("registracioneTablice");
				String[] parsiranaListaTablica = listaTablica.split(",");
				
				ArrayList<String> listaZaVracanjeTrazenihInformacija = new ArrayList<String>();
				
				if (kilometri > 60 * brojDana) {
					System.out.println("Planirane kilometre nije moguce preci, zbog broja dana rezervacije.");
				} else {
					for (Vozila vozilo : vozila) {
						if(vozilo.getClass().getName().equals("model.Bicikli")) {
						
							ArrayList<String> listaBicikl = new ArrayList<String>();
							double cenaNocenja = 0;
							double cenaDana = 0;
							if (!(brojDana > 0)) {
								brojDana = 1;
							}
							double ukupnaCenaIznajmljivanja = 0;
							
							for (String parsiranaTablica : parsiranaListaTablica) {
								if(vozilo.getRegistracioniBroj().equals(parsiranaTablica)) {
									listaBicikl.add(vozilo.getRegistracioniBroj());
									
									System.out.println("Podudareni bicikl je: " + vozilo.getRegistracioniBroj());
									System.out.println("Planiran broj kilometara je: " + kilometri);
									
									cenaDana = brojDana * vozilo.getCenaIznajmljivanjaPoDanu();
									System.out.println("Broj dana je: " + brojDana);
									System.out.println("Ukupna cena dana je: " + cenaDana);
													
									double troskoviZaPredjeniPut = vozilo.cenaPrelaskaRazdaljine(kilometri);
									System.out.println("Troskovi za predjeni put: " + troskoviZaPredjeniPut);
									
									boolean servisJePotreban = vozilo.sledeciServis(kilometri);
									
									if (servisJePotreban) {
										ukupnaCenaIznajmljivanja = cenaDana + cenaNocenja + troskoviZaPredjeniPut + vozilo.getCenaServisiranja();
										float ukupnaCenaIznajmljivanjaRound = Math.round(ukupnaCenaIznajmljivanja * 100)/100.0f;
										System.out.println("Bice potreban servis. Ukupna cena iznajmljivanja sa servisiranjem je: " + ukupnaCenaIznajmljivanjaRound);
										String cenaDanaString = String.valueOf(cenaDana);
										String cenaNocenjaString = String.valueOf(cenaNocenja);
										String ukupnaCenaIznajmljivanjaString = String.valueOf(ukupnaCenaIznajmljivanjaRound);
										listaBicikl.add(cenaDanaString);
										listaBicikl.add("servis je potreban");
										listaBicikl.add(ukupnaCenaIznajmljivanjaString);
										listaBicikl.add("|");
									} else {
										ukupnaCenaIznajmljivanja = cenaDana + cenaNocenja + troskoviZaPredjeniPut;
										float ukupnaCenaIznajmljivanjaRound = Math.round(ukupnaCenaIznajmljivanja * 100)/100.0f;
										System.out.println("Nece biti potreban servis. Ukupna cena iznajmljivanja bez servisiranja je: " + ukupnaCenaIznajmljivanjaRound);
										String cenaDanaString = String.valueOf(cenaDana);
										String cenaNocenjaString = String.valueOf(cenaNocenja);
										String ukupnaCenaIznajmljivanjaString = String.valueOf(ukupnaCenaIznajmljivanjaRound);
										listaBicikl.add(cenaDanaString);
										listaBicikl.add("servis nije potreban");
										listaBicikl.add(ukupnaCenaIznajmljivanjaString);
										listaBicikl.add("|");
									}
									listaZaVracanjeTrazenihInformacija.addAll(listaBicikl);
								}
							}
						}
					}
				}
				for (Vozila vozilo : vozila) {
					if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
					
						ArrayList<String> listaPutnickoVozilo = new ArrayList<String>();
						double cenaNocenja = 0;
						double cenaDana = 0;
						if (!(brojDana > 0)) {
							brojDana = 1;
						}
						double ukupnaCenaIznajmljivanja = 0;
						
						for (String parsiranaTablica : parsiranaListaTablica) {
							if(vozilo.getRegistracioniBroj().equals(parsiranaTablica)) {
								listaPutnickoVozilo.add(vozilo.getRegistracioniBroj());
								
								System.out.println("Podudareno putnicko vozilo je: " + vozilo.getRegistracioniBroj());
								System.out.println("Planiran broj kilometara je: " + kilometri);
								
								cenaDana = brojDana * vozilo.getCenaIznajmljivanjaPoDanu();
								System.out.println("Broj dana je: " + brojDana);
								System.out.println("Ukupna cena dana je: " + cenaDana);								
								
								double cenaPrelaskaRazdaljineGorivo = vozilo.cenaPrelaskaRazdaljine(kilometri);
								System.out.println("Cena za gorivo: " + cenaPrelaskaRazdaljineGorivo);
								
								boolean servisJePotreban = vozilo.sledeciServis(kilometri);
								
								if (servisJePotreban) {
									ukupnaCenaIznajmljivanja = cenaDana + cenaNocenja + cenaPrelaskaRazdaljineGorivo + vozilo.getCenaServisiranja();
									float ukupnaCenaIznajmljivanjaRound = Math.round(ukupnaCenaIznajmljivanja * 100)/100.0f;
									System.out.println("Bice potreban servis. Ukupna cena iznajmljivanja sa servisiranjem je: " + ukupnaCenaIznajmljivanjaRound);
									String cenaDanaString = String.valueOf(cenaDana);
									String cenaNocenjaString = String.valueOf(cenaNocenja);
									String ukupnaCenaIznajmljivanjaString = String.valueOf(ukupnaCenaIznajmljivanjaRound);
									listaPutnickoVozilo.add(cenaDanaString);
									listaPutnickoVozilo.add("servis je potreban");
									listaPutnickoVozilo.add(ukupnaCenaIznajmljivanjaString);
									listaPutnickoVozilo.add("|");
								} else {
									ukupnaCenaIznajmljivanja = cenaDana + cenaNocenja + cenaPrelaskaRazdaljineGorivo;
									float ukupnaCenaIznajmljivanjaRound = Math.round(ukupnaCenaIznajmljivanja * 100)/100.0f;
									System.out.println("Nece biti potreban servis. Ukupna cena iznajmljivanja bez servisiranja je: " + ukupnaCenaIznajmljivanjaRound);
									String cenaDanaString = String.valueOf(cenaDana);
									String cenaNocenjaString = String.valueOf(cenaNocenja);
									String ukupnaCenaIznajmljivanjaString = String.valueOf(ukupnaCenaIznajmljivanjaRound);
									listaPutnickoVozilo.add(cenaDanaString);
									listaPutnickoVozilo.add("servis nije potreban");
									listaPutnickoVozilo.add(ukupnaCenaIznajmljivanjaString);
									listaPutnickoVozilo.add("|");
								}
								listaZaVracanjeTrazenihInformacija.addAll(listaPutnickoVozilo);
							}
						}
				}
			}
				for (Vozila vozilo : vozila) {
					if(vozilo.getClass().getName().equals("model.TeretnoVozilo")) {
					
						ArrayList<String> listaTeretnoVozilo = new ArrayList<String>();
						double cenaNocenja = 0;
						double cenaDana = 0;
						if (!(brojDana > 0)) {
							brojDana = 1;
						}
						double ukupnaCenaIznajmljivanja = 0;
						
						for (String parsiranaTablica : parsiranaListaTablica) {
							if(vozilo.getRegistracioniBroj().equals(parsiranaTablica)) {
								listaTeretnoVozilo.add(vozilo.getRegistracioniBroj());
								
								System.out.println("Podudareno teretno vozilo je: " + vozilo.getRegistracioniBroj());
								System.out.println("Planiran broj kilometara je: " + kilometri);
								
								cenaDana = brojDana * vozilo.getCenaIznajmljivanjaPoDanu();
								System.out.println("Broj dana je: " + brojDana);
								System.out.println("Ukupna cena dana je: " + cenaDana);
								
								double cenaPrelaskaRazdaljineGorivo = vozilo.cenaPrelaskaRazdaljine(kilometri);
								System.out.println("Cena za gorivo: " + cenaPrelaskaRazdaljineGorivo);
								
								boolean servisJePotreban = vozilo.sledeciServis(kilometri);
								
								if (servisJePotreban) {
									ukupnaCenaIznajmljivanja = cenaDana + cenaNocenja + cenaPrelaskaRazdaljineGorivo + vozilo.getCenaServisiranja();
									float ukupnaCenaIznajmljivanjaRound = Math.round(ukupnaCenaIznajmljivanja * 100)/100.0f;
									System.out.println("Bice potreban servis. Ukupna cena iznajmljivanja sa servisiranjem je: " + ukupnaCenaIznajmljivanjaRound);
									String cenaDanaString = String.valueOf(cenaDana);
									String cenaNocenjaString = String.valueOf(cenaNocenja);
									String ukupnaCenaIznajmljivanjaString = String.valueOf(ukupnaCenaIznajmljivanjaRound);
									listaTeretnoVozilo.add(cenaDanaString);
									listaTeretnoVozilo.add("servis je potreban");
									listaTeretnoVozilo.add(ukupnaCenaIznajmljivanjaString);
									listaTeretnoVozilo.add("|");
								} else {
									ukupnaCenaIznajmljivanja = cenaDana + cenaNocenja + cenaPrelaskaRazdaljineGorivo;
									float ukupnaCenaIznajmljivanjaRound = Math.round(ukupnaCenaIznajmljivanja * 100)/100.0f;
									System.out.println("Nece biti potreban servis. Ukupna cena iznajmljivanja bez servisiranja je: " + ukupnaCenaIznajmljivanjaRound);
									String cenaDanaString = String.valueOf(cenaDana);
									String cenaNocenjaString = String.valueOf(cenaNocenja);
									String ukupnaCenaIznajmljivanjaString = String.valueOf(ukupnaCenaIznajmljivanjaRound);
									listaTeretnoVozilo.add(cenaDanaString);
									listaTeretnoVozilo.add("servis nije potreban");
									listaTeretnoVozilo.add(ukupnaCenaIznajmljivanjaString);
									listaTeretnoVozilo.add("|");
								}
								listaZaVracanjeTrazenihInformacija.addAll(listaTeretnoVozilo);
							}
						}
					}
				}
				
		
			ex.sendResponseHeaders(200, mapper.writeValueAsString(listaZaVracanjeTrazenihInformacija).getBytes().length);
			os.write(mapper.writeValueAsString(listaZaVracanjeTrazenihInformacija));
			} else {
				ex.sendResponseHeaders(200, mapper.writeValueAsString("pogresni kljucevi su uneti").getBytes().length);
				os.write(mapper.writeValueAsString("pogresni kljucevi su uneti"));
			}
		}
	}
	
	private void postRacunanjeTroskova(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {
	
	}
}
	