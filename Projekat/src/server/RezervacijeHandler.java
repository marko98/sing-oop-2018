/**
 * 
 */
package server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import liste.RezervacijeLista;
import model.Rezervacije;
import utils.URIUtil;

public class RezervacijeHandler implements HttpHandler {
	private List<Rezervacije> rezervacije;
	private ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * @param rezervacije
	 */
	public RezervacijeHandler(List<Rezervacije> rezervacije) {
		super();
		this.rezervacije = rezervacije;
	}
	/* (non-Javadoc)
	 * @see com.sun.net.httpserver.HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
	 */
	@Override
	public void handle(HttpExchange ex) throws IOException {
		// Dobavljanje podatka o metodu zahteva
		String reqMethod = ex.getRequestMethod();
		// Bez obzira koji je zahtev primljen moraju se podesiti zagavlja odgovora na zahtev
		Headers resHeaders = ex.getResponseHeaders();
		resHeaders.set("Content-Type", "application/json; charset=UTF-8");
		resHeaders.set("Access-Control-Allow-Origin", "*");
		resHeaders.set("Access-Control-Allow-Methods", "GET, POST, PUT");
		// URI nam je potreban da bismo izvadili parametre (npr. ukoliko zelimo da obrisemo neki kurs ili
		// ukoliko zelimo da dobavimo podatke o samo jednom kursu)
		URI reqURI = ex.getRequestURI();
		HashMap<String, String> params = URIUtil.queryToMap(reqURI); // mapa moze biti prazna
		Writer os = new OutputStreamWriter(ex.getResponseBody(), "UTF-8"); // priprema odgovora
		// Iscitavanje podataka iz tela zahteva ukoliko ga ima
		InputStream in = ex.getRequestBody();
		byte[] body = in.readAllBytes();
		String bodyStr = new String(body, "UTF-8");
		// U zavisnosti od tipa upita se obradjuju zahtevi
		switch (reqMethod) {
		case "GET":
			System.out.println("Obrada GET zahteva!");
			try {
				getRezervacije(ex, os, params);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("GET zahtev obradjen!");
			break;
		case "POST":
			System.out.println("Obrada POST zahteva!");
			try {
				postRezervacije(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("POST zahtev obradjen!");
			break;
		case "PUT":
			System.out.println("Obrada PUT zahteva!");
			try {
				putRezervacije(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case "OPTIONS":
			System.out.println("Obrada OPITONS zahteva.");
			ex.sendResponseHeaders(200, 0);
			os.write("");
			System.out.println("OPTIONS zahtev obradjen!");
			break;
		default:
			System.out.println("Neodgovarajuci metod zahteva!");
			ex.sendResponseHeaders(400, 0);
			os.write("");
			System.out.println("Obradjen neodgovarajuci metod zahteva");
		}
		os.close(); // nakon zatvaranja stream-a se salje odgovor
	}
	
	private void getRezervacije(HttpExchange ex, Writer os, HashMap<String, String> params) throws JsonProcessingException, IOException {
		if (params.isEmpty()) {
			ArrayList<Rezervacije> listaRezervacija = new ArrayList<Rezervacije>();
			
			for(Rezervacije rezervacija : rezervacije) {
				if(rezervacija.getLogickiObrisanaRezervacija() == false) {
					listaRezervacija.add(rezervacija);
				}
			}
			if(listaRezervacija.isEmpty()) {
				System.out.println("Nema aktivnih rezervacija.");
				ex.sendResponseHeaders(200, mapper.writeValueAsString("Nema aktivnih rezervacija.").getBytes().length);
				os.write(mapper.writeValueAsString("Nema aktivnih rezervacija."));
			} else {
				System.out.println("Ima aktivnih rezervacija.");
				ex.sendResponseHeaders(200, mapper.writeValueAsString(listaRezervacija).getBytes().length);
				os.write(mapper.writeValueAsString(listaRezervacija));
			}

		} else {
			if(params.containsKey("korisnickoIme")) {
				System.out.println("U parametrima se nalazi korisnicko ime.");
				
				String korisnickoIme = params.get("korisnickoIme");
				ArrayList<Rezervacije> listaRezervacijaIznajmljivaca = new ArrayList<Rezervacije>();
				
				for(Rezervacije rezervacija : rezervacije) {
					if(rezervacija.getIznajmljivac().getKorisnickoIme().equals(korisnickoIme) &&
							rezervacija.getLogickiObrisanaRezervacija() == false) {
						listaRezervacijaIznajmljivaca.add(rezervacija);
					}
				}
				if(listaRezervacijaIznajmljivaca.isEmpty()) {
					System.out.println("Iznajmljivac nema aktivnih rezervacija ili ih nije ni pravio.");
					ex.sendResponseHeaders(200, mapper.writeValueAsString("Iznajmljivac nema aktivnih rezervacija ili ih nije ni pravio.").getBytes().length);
					os.write(mapper.writeValueAsString("Iznajmljivac nema aktivnih rezervacija ili ih nije ni pravio."));
				} else {
					System.out.println("Iznajmljivac poseduje rezervacije.");
					ex.sendResponseHeaders(200, mapper.writeValueAsString(listaRezervacijaIznajmljivaca).getBytes().length);
					os.write(mapper.writeValueAsString(listaRezervacijaIznajmljivaca));
				}				
			} else {
				ex.sendResponseHeaders(400, 0);
				os.write("");
			}	
		}
	}
	
	private void postRezervacije(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {
		Rezervacije reqRezervacije = mapper.readValue(bodyStr, Rezervacije.class);
		
		rezervacije.add(reqRezervacije);
		RezervacijeLista listaRezervacije = new RezervacijeLista(new ArrayList<Rezervacije>(rezervacije));
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);	
		mapper.writeValue(new File("rezervacije.json"), listaRezervacije);
		
		System.out.println("Upisana je nova rezervacija.");
		ex.sendResponseHeaders(200, "Server: primljeni su podaci i dodati u listu rezervacija".getBytes().length);
		os.write("Server: primljeni su podaci i dodati u listu rezervacija");
		os.close();		
	}
	
	private void putRezervacije(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {		
		if (params.isEmpty()) {
			ex.sendResponseHeaders(200, mapper.writeValueAsString("Nisu prosledjeni parametri.").getBytes().length);
			os.write(mapper.writeValueAsString("Nisu prosledjeni parametri."));
		} else {
			if(params.containsKey("obrisiRegistracioneTablice")) {
				String reqRezervacijeBrisanje = bodyStr;
				String[] brisanjePodaci = reqRezervacijeBrisanje.split(",");
				String registracioneTablice = brisanjePodaci[1];
				String datumPocetkaRezervacije = brisanjePodaci[2];
				String datumKrajaRezervacije = brisanjePodaci[3];
				
				for (Rezervacije rezervacija : rezervacije)  {
					if(rezervacija.getDatumPocetkaRezervacije().equals(datumPocetkaRezervacije) &&
							rezervacija.getDatumKrajaRezervacije().equals(datumKrajaRezervacije) &&
							rezervacija.getRezervisanoVozilo().getRegistracioniBroj().equals(registracioneTablice)) {
						rezervacija.obrisiRezervaciju(rezervacija);
						ex.sendResponseHeaders(200, mapper.writeValueAsString("Rezervacija je obrisana.").getBytes().length);
						os.write(mapper.writeValueAsString("Rezervacija je obrisana."));
					}
				}
				RezervacijeLista listaRezervacije = new RezervacijeLista(new ArrayList<Rezervacije>(rezervacije));
				ObjectMapper mapper = new ObjectMapper();
				mapper.enable(SerializationFeature.INDENT_OUTPUT);	
				mapper.writeValue(new File("rezervacije.json"), listaRezervacije);
				
				System.out.println("Rezervacija je obrisana.");	
			}
		}
	}
}
