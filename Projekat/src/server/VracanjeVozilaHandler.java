/**
 * 
 */
package server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import liste.RezervacijeLista;
import liste.ServisiranjaLista;
import liste.VozilaLista;
import model.Rezervacije;
import model.Servisiranja;
import model.Vozila;
import utils.URIUtil;

public class VracanjeVozilaHandler implements HttpHandler {
	private List<Rezervacije> rezervacije;
	private List<Vozila> vozila;
	private List<Servisiranja> servisiranja;
	private ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * @param rezervacije
	 * @param vozila
	 * @param servisiranja
	 */
	public VracanjeVozilaHandler(List<Rezervacije> rezervacije, List<Vozila> vozila, List<Servisiranja> servisiranja) {
		super();
		this.rezervacije = rezervacije;
		this.vozila = vozila;
		this.servisiranja = servisiranja;
	}
	/* (non-Javadoc)
	 * @see com.sun.net.httpserver.HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
	 */
	@Override
	public void handle(HttpExchange ex) throws IOException {
		// Dobavljanje podatka o metodu zahteva
		String reqMethod = ex.getRequestMethod();
		// Bez obzira koji je zahtev primljen moraju se podesiti zagavlja odgovora na zahtev
		Headers resHeaders = ex.getResponseHeaders();
		resHeaders.set("Content-Type", "application/json; charset=UTF-8");
		resHeaders.set("Access-Control-Allow-Origin", "*");
		resHeaders.set("Access-Control-Allow-Methods", "GET, POST, PUT");
		// URI nam je potreban da bismo izvadili parametre (npr. ukoliko zelimo da obrisemo neki kurs ili
		// ukoliko zelimo da dobavimo podatke o samo jednom kursu)
		URI reqURI = ex.getRequestURI();
		HashMap<String, String> params = URIUtil.queryToMap(reqURI); // mapa moze biti prazna
		Writer os = new OutputStreamWriter(ex.getResponseBody(), "UTF-8"); // priprema odgovora
		// Iscitavanje podataka iz tela zahteva ukoliko ga ima
		InputStream in = ex.getRequestBody();
		byte[] body = in.readAllBytes();
		String bodyStr = new String(body, "UTF-8");
		// U zavisnosti od tipa upita se obradjuju zahtevi
		switch (reqMethod) {
		case "GET":
			System.out.println("Obrada GET zahteva!");
			try {
				getVracanjeVozila(ex, os, params);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("GET zahtev obradjen!");
			break;
		case "POST":
			System.out.println("Obrada POST zahteva!");
			try {
				postVracanjeVozila(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("POST zahtev obradjen!");
			break;
		case "PUT":
			System.out.println("Obrada PUT zahteva!");
			try {
				putVracanjeVozila(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case "OPTIONS":
			System.out.println("Obrada OPITONS zahteva.");
			ex.sendResponseHeaders(200, 0);
			os.write("");
			System.out.println("OPTIONS zahtev obradjen!");
			break;
		default:
			System.out.println("Neodgovarajuci metod zahteva!");
			ex.sendResponseHeaders(400, 0);
			os.write("");
			System.out.println("Obradjen neodgovarajuci metod zahteva");
		}
		os.close(); // nakon zatvaranja stream-a se salje odgovor
	}

	private void getVracanjeVozila(HttpExchange ex, Writer os, HashMap<String, String> params) throws JsonProcessingException, IOException {

	}

	private void postVracanjeVozila(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {
		
	}
	
	private void putVracanjeVozila(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {		
		String reqVracanjeVozila = bodyStr;
		String[] reqVracanjeVozilaNiz = reqVracanjeVozila.split(",");
		
		String registracioniBroj = reqVracanjeVozilaNiz[1];
		String datumPocetkaRezervacije = reqVracanjeVozilaNiz[5];
		String datumKrajaRezervacije = reqVracanjeVozilaNiz[6];
		
		String cenaString = reqVracanjeVozilaNiz[7];
		Double cena = Double.parseDouble(cenaString);
		
		String predjeniBrojKilometaraString = reqVracanjeVozilaNiz[2];
		Double predjeniBrojKilometara = Double.parseDouble(predjeniBrojKilometaraString);
		
		String datumServisiranja = reqVracanjeVozilaNiz[3];
		boolean pronadjenaRezervacija = false;
		
		for(Rezervacije rezervacija : rezervacije) {
			if(rezervacija.getDatumPocetkaRezervacije().equals(datumPocetkaRezervacije) &&
					rezervacija.getDatumKrajaRezervacije().equals(datumKrajaRezervacije)) {
				if(rezervacija.getRezervisanoVozilo().getRegistracioniBroj().equals(registracioniBroj)) {
					rezervacija.setLogickiObrisanaRezervacija(true);
					rezervacija.setCenaRezervacije(cena);
					pronadjenaRezervacija = true;
				}
			}
		}
		if(pronadjenaRezervacija) {
			System.out.println("Rezervacija je pronadjena!");
		} else {
			System.out.println("Nije pronadjena rezervacija!");
		}
		RezervacijeLista listaRezervacije = new RezervacijeLista(new ArrayList<Rezervacije>(rezervacije));
		ObjectMapper mapper = new ObjectMapper();
		mapper.enable(SerializationFeature.INDENT_OUTPUT);	
		mapper.writeValue(new File("rezervacije.json"), listaRezervacije);
		
		for(Vozila vozilo : vozila) {
			if(vozilo.getRegistracioniBroj().equals(registracioniBroj)) {				
				if(vozilo.sledeciServis(predjeniBrojKilometara)) {
					Servisiranja noviServis= new Servisiranja(registracioniBroj, datumServisiranja, (vozilo.getPredjeniKm() + predjeniBrojKilometara));
					ArrayList<Servisiranja> servis = new ArrayList<Servisiranja>();
					
					for(Servisiranja servisVozila : vozilo.getServisiranje()) {
						servis.add(servisVozila);
					}
					
					servis.add(noviServis);
					vozilo.setServisiranje(servis);
					
					servisiranja.add(noviServis);
					ServisiranjaLista listaServisiranja = new ServisiranjaLista(new ArrayList<Servisiranja>(servisiranja));
					ObjectMapper mapper2 = new ObjectMapper();
					mapper2.enable(SerializationFeature.INDENT_OUTPUT);	
					mapper2.writeValue(new File("servisiranja.json"), listaServisiranja);
				}
				
				vozilo.setPredjeniKm(vozilo.getPredjeniKm() + predjeniBrojKilometara);
				VozilaLista listaVozila = new VozilaLista(new ArrayList<Vozila>(vozila));
				ObjectMapper mapper3 = new ObjectMapper();
				mapper3.enable(SerializationFeature.INDENT_OUTPUT);	
				mapper3.writeValue(new File("vozila.json"), listaVozila);
				
				ex.sendResponseHeaders(200, mapper.writeValueAsString("Izvrsene su izmene.").getBytes().length);
				os.write(mapper.writeValueAsString("Izvrsene su izmene."));
			}
		}
	}
}


