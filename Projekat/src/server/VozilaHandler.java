package server;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStreamWriter;
import java.io.Writer;
import java.net.URI;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;
import com.sun.net.httpserver.Headers;
import com.sun.net.httpserver.HttpExchange;
import com.sun.net.httpserver.HttpHandler;

import liste.VozilaLista;
import model.Vozila;
import utils.URIUtil;

public class VozilaHandler implements HttpHandler {
	private List<Vozila> vozila;
	private ObjectMapper mapper = new ObjectMapper();
	
	/**
	 * @param vozila
	 */
	public VozilaHandler(List<Vozila> vozila) {
		super();
		this.vozila = vozila;
	}
	
	/* (non-Javadoc)
	 * @see com.sun.net.httpserver.HttpHandler#handle(com.sun.net.httpserver.HttpExchange)
	 */
	@Override
	public void handle(HttpExchange ex) throws IOException {
		// Dobavljanje podatka o metodu zahteva
		String reqMethod = ex.getRequestMethod();
		// Bez obzira koji je zahtev primljen moraju se podesiti zagavlja odgovora na zahtev
		Headers resHeaders = ex.getResponseHeaders();
		resHeaders.set("Content-Type", "application/json; charset=UTF-8");
		resHeaders.set("Access-Control-Allow-Origin", "*");
		resHeaders.set("Access-Control-Allow-Methods", "GET, POST, PUT");
		// URI nam je potreban da bismo izvadili parametre (ako postoje)
		URI reqURI = ex.getRequestURI();
		HashMap<String, String> params = URIUtil.queryToMap(reqURI); // mapa moze biti prazna
		Writer os = new OutputStreamWriter(ex.getResponseBody(), "UTF-8"); // priprema odgovora
		// Iscitavanje podataka iz tela zahteva ukoliko ga ima
		InputStream in = ex.getRequestBody();
		byte[] body = in.readAllBytes();
		String bodyStr = new String(body, "UTF-8");
		// U zavisnosti od tipa upita se obradjuju zahtevi
		switch (reqMethod) {
		case "GET":
			System.out.println("Obrada GET zahteva!");
			try {
				getVozila(ex, os, params);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("GET zahtev obradjen!");
			break;
		case "POST":
			System.out.println("Obrada POST zahteva!");
			try {
				postVozila(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			System.out.println("POST zahtev obradjen!");
			break;
		case "PUT":
			System.out.println("Obrada PUT zahteva!");
			try {
				putVozila(ex, os, params, bodyStr);
			} catch (Exception e) {
				e.printStackTrace();
			}
			break;
		case "OPTIONS":
			System.out.println("Obrada OPITONS zahteva.");
			ex.sendResponseHeaders(200, 0);
			os.write("");
			System.out.println("OPTIONS zahtev obradjen!");
			break;
		default:
			System.out.println("Neodgovarajuci metod zahteva!");
			ex.sendResponseHeaders(400, 0);
			os.write("");
			System.out.println("Obradjen neodgovarajuci metod zahteva");
			}
		os.close(); // nakon zatvaranja stream-a se salje odgovor
	}
	
	private void getVozila(HttpExchange ex, Writer os, HashMap<String, String> params) throws JsonProcessingException, IOException {
		if (params.isEmpty()) {
			ex.sendResponseHeaders(200, mapper.writeValueAsString(vozila).getBytes().length);
			os.write(mapper.writeValueAsString(vozila));
		} else {
			if (params.containsKey("registracionaTablica")) {
				
				String registracionaTablica = params.get("registracionaTablica");
				Vozila pronadjenoVozilo = null;
				
				for(Vozila vozilo : vozila) {
					if(vozilo.getRegistracioniBroj().equals(registracionaTablica)) {
						pronadjenoVozilo = vozilo;
					}
				}
				
				if(!(pronadjenoVozilo == null)) {
					ex.sendResponseHeaders(200, mapper.writeValueAsString(pronadjenoVozilo).getBytes().length);
					os.write(mapper.writeValueAsString(pronadjenoVozilo));
				} else {
					ex.sendResponseHeaders(200, mapper.writeValueAsString(null).getBytes().length);
					os.write(mapper.writeValueAsString(null));
				}
			} else if (params.containsKey("obrisanaVozila")) {
				ArrayList<Vozila> obrisanaVozila = new ArrayList<Vozila>();
				
				for(Vozila vozilo : vozila) {
					if(vozilo.getLogickiObrisanoVozilo().equals(true)) {
						obrisanaVozila.add(vozilo);
					}
				}
				if(obrisanaVozila.isEmpty()) {
					ex.sendResponseHeaders(200, mapper.writeValueAsString("Nema obrisanih vozila.").getBytes().length);
					os.write(mapper.writeValueAsString("Nema obrisanih vozila."));
				} else {
					ex.sendResponseHeaders(200, mapper.writeValueAsString(obrisanaVozila).getBytes().length);
					os.write(mapper.writeValueAsString(obrisanaVozila));
				}
			} else {
				ex.sendResponseHeaders(200, mapper.writeValueAsString(vozila).getBytes().length);
				os.write(mapper.writeValueAsString(vozila));
			}
		}
	}
	
	private void postVozila(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {
		Vozila reqVozila = mapper.readValue(bodyStr, Vozila.class);
		
		boolean duplicate = false;
		for (Vozila vozilo : vozila) {
			if (vozilo.getRegistracioniBroj().equals(reqVozila.getRegistracioniBroj())) {
				duplicate = true;
				break;
			}
		}
		if (!duplicate) {
			vozila.add(reqVozila);
			VozilaLista listaVozila = new VozilaLista(new ArrayList<Vozila>(vozila));
			ObjectMapper omVozila = new ObjectMapper();
			omVozila.enable(SerializationFeature.INDENT_OUTPUT);
			
			File fajlVozila = new File("vozila.json");
			
			omVozila.writeValue(fajlVozila, listaVozila	);
			
			ex.sendResponseHeaders(200, "Server: primljeni su podaci i dodati u listu vozila".getBytes().length);
			os.write("Server: primljeni su podaci i dodati u listu vozila");
			}
		else {
			System.out.println("Duplikat - vozilo");  
			
			ex.sendResponseHeaders(200, "Server: duplikat - vec postoji vozilo sa istim registracionim brojem".getBytes().length);
			os.write("Server: duplikat - vec postoji vozilo sa istim registracionim brojem");
	   }
		os.close();		
	}
	private void putVozila(HttpExchange ex, Writer os, HashMap<String, String> params, String bodyStr) throws JsonParseException, JsonMappingException, IOException {
		if (params.isEmpty()) {
		ex.sendResponseHeaders(200, mapper.writeValueAsString("Nisu prosledjeni parametri.").getBytes().length);
		os.write(mapper.writeValueAsString("Nisu prosledjeni parametri."));
	} else {
		if(params.containsKey("aktiviranjeVozila")) {
			String reqAktiviranjeVozilaTablica = bodyStr;
	
			
			for (Vozila vozilo : vozila)  {
				if(vozilo.getRegistracioniBroj().equals(reqAktiviranjeVozilaTablica)) {
					vozilo.setLogickiObrisanoVozilo(false);
					ex.sendResponseHeaders(200, mapper.writeValueAsString("Vozilo je aktivirano.").getBytes().length);
					os.write(mapper.writeValueAsString("Vozilo je aktivirano."));
				}
			}
			VozilaLista listaVozila = new VozilaLista(new ArrayList<Vozila>(vozila));
			ObjectMapper mapper = new ObjectMapper();
			mapper.enable(SerializationFeature.INDENT_OUTPUT);	
			mapper.writeValue(new File("vozila.json"), listaVozila);
			
			System.out.println("Vozilo tablica: " + reqAktiviranjeVozilaTablica + " je aktivirano.");	
		}
	}
	}
}
	