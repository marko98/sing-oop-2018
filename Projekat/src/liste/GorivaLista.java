package liste;

import java.util.ArrayList;

import model.Goriva;

public class GorivaLista {
	private ArrayList<Goriva> goriva;

	public ArrayList<Goriva> getGoriva() {
		return goriva;
	}

	public void setGoriva(ArrayList<Goriva> goriva) {
		this.goriva = goriva;
	}

	public void add(Goriva g) {
		this.goriva.add(g);
	}

	public GorivaLista() {
		
	}

	@Override
	public String toString() {
		return goriva.toString();
	}

	public GorivaLista(ArrayList<Goriva> goriva) {
		super();
		this.goriva = goriva;
	}

}
