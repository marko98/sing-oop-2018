package liste;

import java.util.ArrayList;

import model.Vozila;

public class VozilaLista {
	private ArrayList<Vozila> vozila;

	public ArrayList<Vozila> getVozila() {
		return vozila;
	}

	public void setVozila(ArrayList<Vozila> vozila) {
		this.vozila = vozila;
	}

	public void add(Vozila v) {
		this.vozila.add(v);
	}

	public VozilaLista() {
		
	}

	@Override
	public String toString() {
		return vozila.toString();
	}

	public VozilaLista(ArrayList<Vozila> vozila) {
		super();
		this.vozila = vozila;
	}

}
