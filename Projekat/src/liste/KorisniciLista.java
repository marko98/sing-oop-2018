package liste;

import java.util.ArrayList;

import model.Korisnici;

public class KorisniciLista {
	private ArrayList<Korisnici> korisnici;
	
	public ArrayList<Korisnici> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(ArrayList<Korisnici> korisnici) {
		this.korisnici = korisnici;
	}

	public void add(Korisnici k) {
		this.korisnici.add(k);
	}

	public KorisniciLista() {
		
	}

	@Override
	public String toString() {
		return  korisnici.toString();
	}

	public KorisniciLista(ArrayList<Korisnici> korisnici) {
		super();
		this.korisnici = korisnici;
	}

}
