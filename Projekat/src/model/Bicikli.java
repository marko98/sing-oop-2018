package model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;

@JsonIgnoreProperties({"brojVrata", "gorivo", "potrosnjaGoriva"})
public class Bicikli extends Vozila {
	
	public Bicikli() {
		super();
	}
	
	public Bicikli(String registracioniBroj, Goriva gorivo, List<Servisiranja> servisiranje, double potrosnjaGoriva,
			double predjeniKm, double brKmPreServisiranja, double cenaServisiranja, double cenaIznajmljivanjaPoDanu,
			Integer brojSedista, Integer brojVrata, Boolean logickiObrisanoVozilo) {
		super(registracioniBroj, gorivo, servisiranje, potrosnjaGoriva, predjeniKm, brKmPreServisiranja, cenaServisiranja,
				cenaIznajmljivanjaPoDanu, brojSedista, brojVrata, logickiObrisanoVozilo);
	}

	@Override
	public String toString() {
		return "Bicikli []";
	}
	
	@Override
	public double cenaPrelaskaRazdaljine(double kilometri) {
		if(kilometri < 60) {
			int brojNocenja = 1;
			double troskoviZaPredjeniPut = brojNocenja * 1180;
			return troskoviZaPredjeniPut;
		} else {
			double brojNoci = kilometri / 60;
			int brojNocenja = (int) brojNoci;
			double troskoviZaPredjeniPut = brojNocenja * 1180;
			return troskoviZaPredjeniPut;
		}
	}
	
	@Override
	public boolean sledeciServis(double kilometri) {
		if (!this.getServisiranje().isEmpty()) {
			System.out.println("Postoje servisiranja na vozilu.");
			double brojPredjenihKm = this.getPredjeniKm();
			System.out.println("Predjeni km bicikla su: " + brojPredjenihKm);
			
			List<Servisiranja> servisiranje = this.getServisiranje();
			int velicinaListe = servisiranje.size();
			double kmUTrenutkuPoslednjegServisa = servisiranje.get(velicinaListe-1).getBrPredjenihKmUTrenutkuServisiranja();
			System.out.println("Broj predjenih kilometara u trenutku poslednjeg servisiranja su: " + kmUTrenutkuPoslednjegServisa); 
			
			double brKmDoPrvogSledecegServisiranja = this.getBrKmPreServisiranja() - (brojPredjenihKm - kmUTrenutkuPoslednjegServisa);
			System.out.println("Broj km do sledeceg servisiranja je: " + brKmDoPrvogSledecegServisiranja);
			
			if (kilometri >= brKmDoPrvogSledecegServisiranja) {
				System.out.println("Vozilo ce morati da ide na servis.");
				boolean servisJePotreban = true;
				return servisJePotreban;
			} else {
				System.out.println("Nece biti potrebe za servisiranjem.");
				boolean servisJePotreban = false;
				return servisJePotreban;
			}
			
		} else {
			System.out.println("Ne postoje servisiranja na vozilu.");
			double brojPredjenihKm = this.getPredjeniKm();
			System.out.println("Predjeni km bicikla su: " + brojPredjenihKm);
			
			if((kilometri + brojPredjenihKm) >= this.getBrKmPreServisiranja()) {
				System.out.println("Vozilo ce morati da ide na servis.");
				boolean servisJePotreban = true;
				return servisJePotreban;
			} else {
				System.out.println("Nece biti potrebe za servisiranjem.");
				boolean servisJePotreban = false;
				return servisJePotreban;
			}
		}
	}
}
