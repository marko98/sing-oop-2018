package model;

import java.util.List;

public class TeretnoVozilo extends Vozila {
	private double maksimalnaMasa, maksimalnaVisina;
	
	public TeretnoVozilo() {
		super();
	}
	
	public TeretnoVozilo(String registracioniBroj, Goriva gorivo, List<Servisiranja> servisiranje, double potrosnjaGoriva,
			double predjeniKm, double brKmPreServisiranja, double cenaServisiranja, double cenaIznajmljivanjaPoDanu,
			Integer brojSedista, Integer brojVrata, Boolean logickiObrisanoVozilo, double maksimalnaMasa, double maksimalnaVisina) {
		super(registracioniBroj, gorivo, servisiranje, potrosnjaGoriva, predjeniKm, brKmPreServisiranja, cenaServisiranja,
				cenaIznajmljivanjaPoDanu, brojSedista, brojVrata, logickiObrisanoVozilo);
		this.maksimalnaMasa = maksimalnaMasa;
		this.maksimalnaVisina = maksimalnaVisina;
	}

	@Override
	public String toString() {
		return "TeretnoVozilo [maksimalnaMasa=" + maksimalnaMasa + ", maksimalnaVisina=" + maksimalnaVisina + "]";
	}
	
	@Override
	public double cenaPrelaskaRazdaljine(double kilometri) {
		double potrosnjaGorivaPoJednomKm = this.getPotrosnjaGoriva() / 100;
		double potrosnjaLitaraGoriva = potrosnjaGorivaPoJednomKm * kilometri;
		double cenaGorivaPoLitri = this.getGorivo().getCenaGoriva();
		double cenaPrelaskaRazdaljineGorivo = potrosnjaLitaraGoriva * cenaGorivaPoLitri;
		return cenaPrelaskaRazdaljineGorivo;
	}
	
	@Override
	public boolean sledeciServis(double kilometri) {
		if (!this.getServisiranje().isEmpty()) {
			System.out.println("Postoje servisiranja na vozilu.");
			double brojPredjenihKm = this.getPredjeniKm();
			System.out.println("Predjeni km teretnog vozila su: " + brojPredjenihKm);
			
			List<Servisiranja> servisiranje = this.getServisiranje();
			int velicinaListe = servisiranje.size();
			double kmUTrenutkuPoslednjegServisa = servisiranje.get(velicinaListe-1).getBrPredjenihKmUTrenutkuServisiranja();
			System.out.println("Broj predjenih kilometara u trenutku poslednjeg servisiranja su: " + kmUTrenutkuPoslednjegServisa); 
			
			double brKmDoPrvogSledecegServisiranja = this.getBrKmPreServisiranja() - (brojPredjenihKm - kmUTrenutkuPoslednjegServisa);
			System.out.println("Broj km do sledeceg servisiranja je: " + brKmDoPrvogSledecegServisiranja);
			
			if (kilometri >= brKmDoPrvogSledecegServisiranja) {
				System.out.println("Vozilo ce morati da ide na servis.");
				boolean servisJePotreban = true;
				return servisJePotreban;
			} else {
				System.out.println("Nece biti potrebe za servisiranjem.");
				boolean servisJePotreban = false;
				return servisJePotreban;
			}
			
		} else {
			System.out.println("Ne postoje servisiranja na vozilu.");
			double brojPredjenihKm = this.getPredjeniKm();
			System.out.println("Predjeni km teretnog vozila su: " + brojPredjenihKm);
			
			if((kilometri + brojPredjenihKm) >= this.getBrKmPreServisiranja()) {
				System.out.println("Vozilo ce morati da ide na servis.");
				boolean servisJePotreban = true;
				return servisJePotreban;
			} else {
				System.out.println("Nece biti potrebe za servisiranjem.");
				boolean servisJePotreban = false;
				return servisJePotreban;
			}
		}
	}

	public double getMaksimalnaMasa() {
		return maksimalnaMasa;
	}

	public void setMaksimalnaMasa(double maksimalnaMasa) {
		this.maksimalnaMasa = maksimalnaMasa;
	}

	public double getMaksimalnaVisina() {
		return maksimalnaVisina;
	}

	public void setMaksimalnaVisina(double maksimalnaVisina) {
		this.maksimalnaVisina = maksimalnaVisina;
	}
	
}
