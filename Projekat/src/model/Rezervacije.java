package model;

public class Rezervacije {
	private IznajmljivaciVozila iznajmljivac;
	private Vozila rezervisanoVozilo;
	private String datumPocetkaRezervacije;
	private String datumKrajaRezervacije;
	private double cenaRezervacije;
	private Boolean logickiObrisanaRezervacija;
	
	public Rezervacije() {
		super();
	}
	
	public Rezervacije(IznajmljivaciVozila iznajmljivac, Vozila rezervisanoVozilo, String datumPocetkaRezervacije, String datumKrajaRezervacije, 
			double cenaRezervacije, Boolean logickiObrisanaRezervacija) {
		super();
		this.iznajmljivac = iznajmljivac;
		this.rezervisanoVozilo = rezervisanoVozilo;
		this.datumPocetkaRezervacije = datumPocetkaRezervacije;
		this.datumKrajaRezervacije = datumKrajaRezervacije;
		this.cenaRezervacije = cenaRezervacije;
		this.logickiObrisanaRezervacija = logickiObrisanaRezervacija;
	}
	
	public void obrisiRezervaciju(Rezervacije rezervacija) {
		rezervacija.setLogickiObrisanaRezervacija(true);
		// u slucaju kada otkazujemo rezervaciju cena je 0 dinara
		rezervacija.setCenaRezervacije(0);
	} 
	
	@Override
	public String toString() {
		return "Rezervacije [iznajmljivac=" + iznajmljivac + ", rezervisanoVozilo=" + rezervisanoVozilo
				 + ", datumPocetkaRezervacije=" + datumPocetkaRezervacije + ", datumKrajaRezervacije="
				+ datumKrajaRezervacije + ", cenaRezervacije=" + cenaRezervacije + ", logickiObrisanaRezervacija="
				+ logickiObrisanaRezervacija + "]";
	}

	public IznajmljivaciVozila getIznajmljivac() {
		return iznajmljivac;
	}

	public void setIznajmljivac(IznajmljivaciVozila iznajmljivac) {
		this.iznajmljivac = iznajmljivac;
	}

	public Vozila getRezervisanoVozilo() {
		return rezervisanoVozilo;
	}

	public void setRezervisanoVozilo(Vozila rezervisanoVozilo) {
		this.rezervisanoVozilo = rezervisanoVozilo;
	}

	public String getDatumPocetkaRezervacije() {
		return datumPocetkaRezervacije;
	}

	public void setDatumPocetkaRezervacije(String datumPocetkaRezervacije) {
		this.datumPocetkaRezervacije = datumPocetkaRezervacije;
	}

	public String getDatumKrajaRezervacije() {
		return datumKrajaRezervacije;
	}

	public void setDatumKrajaRezervacije(String datumKrajaRezervacije) {
		this.datumKrajaRezervacije = datumKrajaRezervacije;
	}

	public double getCenaRezervacije() {
		return cenaRezervacije;
	}

	public void setCenaRezervacije(double cenaRezervacije) {
		this.cenaRezervacije = cenaRezervacije;
	}

	public Boolean getLogickiObrisanaRezervacija() {
		return logickiObrisanaRezervacija;
	}

	public void setLogickiObrisanaRezervacija(Boolean logickiObrisanaRezervacija) {
		this.logickiObrisanaRezervacija = logickiObrisanaRezervacija;
	}
	
}
