package model;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;
import com.fasterxml.jackson.annotation.JsonTypeInfo;


@JsonTypeInfo(
	    use = JsonTypeInfo.Id.NAME, 
		include = JsonTypeInfo.As.PROPERTY, 
	    property = "type")
	@JsonSubTypes({ 
		@Type(value = Bicikli.class, name = "bicikl"),
		@Type(value = PutnickoVozilo.class, name = "putnicko vozilo"),
		@Type(value = TeretnoVozilo.class, name = "teretno vozilo")
	})

public abstract class Vozila {
	private String registracioniBroj;
	private List<Servisiranja> servisiranje;
	private Goriva gorivo;
	private double potrosnjaGoriva, predjeniKm, brKmPreServisiranja,
	 cenaServisiranja, cenaIznajmljivanjaPoDanu;
	private Integer brojSedista, brojVrata;
	private Boolean logickiObrisanoVozilo;
	
	public Vozila() {
		super();
	}
	
	public Vozila(String registracioniBroj, Goriva gorivo, List<Servisiranja> servisiranje, 
			double potrosnjaGoriva, double predjeniKm, double brKmPreServisiranja, double cenaServisiranja, 
			double cenaIznajmljivanjaPoDanu, Integer brojSedista, Integer brojVrata,
			Boolean logickiObrisanoVozilo) {
		super();
		this.registracioniBroj = registracioniBroj;
		this.gorivo = gorivo;
		this.servisiranje = servisiranje;
		this.potrosnjaGoriva = potrosnjaGoriva;
		this.predjeniKm = predjeniKm;
		this.brKmPreServisiranja = brKmPreServisiranja;
		this.cenaServisiranja = cenaServisiranja;
		this.cenaIznajmljivanjaPoDanu = cenaIznajmljivanjaPoDanu;
		this.brojSedista = brojSedista;
		this.brojVrata = brojVrata;
		this.logickiObrisanoVozilo = logickiObrisanoVozilo;
	}
	
	@Override
	public String toString() {
		return "Vozila [registracioniBroj=" + registracioniBroj + ", servisiranje=" + servisiranje + ", gorivo="
				+ gorivo + ", potrosnjaGoriva=" + potrosnjaGoriva + ", predjeniKm=" + predjeniKm
				+ ", brKmPreServisiranja=" + brKmPreServisiranja + ", cenaServisiranja=" + cenaServisiranja
				+ ", cenaIznajmljivanjaPoDanu=" + cenaIznajmljivanjaPoDanu + ", brojSedista=" + brojSedista
				+ ", brojVrata=" + brojVrata + ", logickiObrisanoVozilo=" + logickiObrisanoVozilo + "]";
	}
	
	public double cenaPrelaskaRazdaljine(double kilometri) {
		double troskoviZaPredjeniPut = 0;
		return troskoviZaPredjeniPut;
	}
	
	public boolean sledeciServis(double kilometri) {
		boolean servisJePotreban = false;
		return servisJePotreban;
	}

	public String getRegistracioniBroj() {
		return registracioniBroj;
	}

	public void setRegistracioniBroj(String registracioniBroj) {
		this.registracioniBroj = registracioniBroj;
	}

	public List<Servisiranja> getServisiranje() {
		return servisiranje;
	}

	public void setServisiranje(List<Servisiranja> servisiranje) {
		this.servisiranje = servisiranje;
	}

	public Goriva getGorivo() {
		return gorivo;
	}

	public void setGorivo(Goriva gorivo) {
		this.gorivo = gorivo;
	}

	public double getPotrosnjaGoriva() {
		return potrosnjaGoriva;
	}

	public void setPotrosnjaGoriva(double potrosnjaGoriva) {
		this.potrosnjaGoriva = potrosnjaGoriva;
	}

	public double getPredjeniKm() {
		return predjeniKm;
	}

	public void setPredjeniKm(double predjeniKm) {
		this.predjeniKm = predjeniKm;
	}

	public double getBrKmPreServisiranja() {
		return brKmPreServisiranja;
	}

	public void setBrKmPreServisiranja(double brKmPreServisiranja) {
		this.brKmPreServisiranja = brKmPreServisiranja;
	}

	public double getCenaServisiranja() {
		return cenaServisiranja;
	}

	public void setCenaServisiranja(double cenaServisiranja) {
		this.cenaServisiranja = cenaServisiranja;
	}

	public double getCenaIznajmljivanjaPoDanu() {
		return cenaIznajmljivanjaPoDanu;
	}

	public void setCenaIznajmljivanjaPoDanu(double cenaIznajmljivanjaPoDanu) {
		this.cenaIznajmljivanjaPoDanu = cenaIznajmljivanjaPoDanu;
	}

	public Integer getBrojSedista() {
		return brojSedista;
	}

	public void setBrojSedista(Integer brojSedista) {
		this.brojSedista = brojSedista;
	}

	public Integer getBrojVrata() {
		return brojVrata;
	}

	public void setBrojVrata(Integer brojVrata) {
		this.brojVrata = brojVrata;
	}

	public Boolean getLogickiObrisanoVozilo() {
		return logickiObrisanoVozilo;
	}

	public void setLogickiObrisanoVozilo(Boolean logickiObrisanoVozilo) {
		this.logickiObrisanoVozilo = logickiObrisanoVozilo;
	}
	
}
