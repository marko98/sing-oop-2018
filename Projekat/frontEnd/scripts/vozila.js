function izbrisiVozilo() {
	var slobodnaVozila = {};

  	var xhttp = new XMLHttpRequest();		  	

  	xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	        content = JSON.parse(xhttp.responseText);
	        if (content != "Nema slobodnih vozila.") {
	        	prikaziSlobodnaVozila(content);
	    	} else {
	    		window.alert("Nema slobodnih vozila.");
	    	}
	    }
	}

	xhttp.open("GET", "http://localhost:8080/brisanjeVozila", true);
    xhttp.send();
}

var isPrikaziSlobodnaVozilaClicked = false;
function prikaziSlobodnaVozila(slobodnaVozila) {
	if (!isPrikaziSlobodnaVozilaClicked) {

		isPrikaziSlobodnaVozilaClicked = true;
		var tabelaVozilaZaBrisanje = document.getElementById("tabelaVozilaZaBrisanje");
		tabelaVozilaZaBrisanje.setAttribute("style", "display:block");
		var tabelaVozilaZaBrisanjeTBody = document.getElementById("tabelaVozilaZaBrisanjeTBody");


		for (let i = 0; i < slobodnaVozila.length; i++) {				
		  	var registracioniBroj = slobodnaVozila[i]["registracioniBroj"];
		  	var cenaIznajmljivanjaPoDanu = slobodnaVozila[i]["cenaIznajmljivanjaPoDanu"];
		  	var predjeniKm = slobodnaVozila[i]["predjeniKm"];
		  	var brKmPreServisiranja = slobodnaVozila[i]["brKmPreServisiranja"];

		  	var row = document.createElement("tr");
		  	tabelaVozilaZaBrisanjeTBody.appendChild(row);

		  	var registracioniBrojRow = document.createElement("td");
		  	registracioniBrojRow.setAttribute("class", registracioniBroj + " registracioniBroj");
		  	var registracioniBrojRowTekst = document.createTextNode(registracioniBroj);
		  	registracioniBrojRow.appendChild(registracioniBrojRowTekst);
		  	row.appendChild(registracioniBrojRow);

		  	var cenaIznajmljivanjaPoDanuRow = document.createElement("td");
		  	cenaIznajmljivanjaPoDanuRow.setAttribute("id", registracioniBroj + " cenaIznajmljivanjaPoDanu");
		  	var cenaIznajmljivanjaPoDanuRowTekst = document.createTextNode(cenaIznajmljivanjaPoDanu);
		  	cenaIznajmljivanjaPoDanuRow.appendChild(cenaIznajmljivanjaPoDanuRowTekst);
		  	row.appendChild(cenaIznajmljivanjaPoDanuRow);

		  	var predjeniKmRow = document.createElement("td");
		  	predjeniKmRow.setAttribute("id", registracioniBroj + " predjeniKm");
		  	var predjeniKmRowTekst = document.createTextNode(predjeniKm);
		  	predjeniKmRow.appendChild(predjeniKmRowTekst);
		  	row.appendChild(predjeniKmRow);

		  	var brKmPreServisiranjaRow = document.createElement("td");
		  	brKmPreServisiranjaRow.setAttribute("id", registracioniBroj + " brKmPreServisiranja");
		  	var brKmPreServisiranjaRowTekst = document.createTextNode(brKmPreServisiranja);
		  	brKmPreServisiranjaRow.appendChild(brKmPreServisiranjaRowTekst);
		  	row.appendChild(brKmPreServisiranjaRow);

		  	var obrisiteVoziloRow = document.createElement("td");
	      	obrisiteVoziloRow.setAttribute("id", registracioniBroj + " obrisiteVozilo");
	      	obrisiteVoziloRow.setAttribute("style", "cursor:pointer;");
	      	obrisiteVoziloRow.setAttribute("onclick", "obrisiVozilo(" + '/' + registracioniBroj + '/' + ")");
	      	var obrisiteVoziloRowTekst = document.createTextNode("Obrišite vozilo");
	      	obrisiteVoziloRow.appendChild(obrisiteVoziloRowTekst);
	      	row.appendChild(obrisiteVoziloRow);
		}
	}
}

function obrisiVozilo(registracioniBroj) {
	registracioniBroj = registracioniBroj.toString();
	registracioniBroj = registracioniBroj.replace(/\//g,'');

	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            content = JSON.parse(xhttp.responseText);
        	window.alert(content);
        	odustaniOdBrisanjaVozila();
        }
    };

  	xhttp.open("PUT", "http://localhost:8080/brisanjeVozila", true);
  	xhttp.setRequestHeader("Content-type", "text/plain");
  	xhttp.send(registracioniBroj);
}

function odustaniOdBrisanjaVozila() {
	var tabelaVozilaZaBrisanje = document.getElementById("tabelaVozilaZaBrisanje");
	tabelaVozilaZaBrisanje.setAttribute("style", "display:none");

  	var tabelaVozilaZaBrisanjeTBody = document.getElementById("tabelaVozilaZaBrisanjeTBody");
  	while (tabelaVozilaZaBrisanjeTBody.firstChild) {
    	tabelaVozilaZaBrisanjeTBody.removeChild(tabelaVozilaZaBrisanjeTBody.firstChild);
  	}

  	isPrikaziSlobodnaVozilaClicked = false;
}

var isDodavanjeVozilaClicked = false;
function dodajVozilo() {
	if (!isDodavanjeVozilaClicked) {
		var divVozilaZaDodavanje = document.getElementById("divVozilaZaDodavanje");
		divVozilaZaDodavanje.setAttribute("style", "display:block;");
		var divVrstaVozila = document.getElementById("divVrstaVozila");
		divVrstaVozila.setAttribute("style", "display:block;");
		isDodavanjeVozilaClicked = true;
	}
}

function dobaviPodatkeOGorivu() {
	var xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function () {
   		if (this.readyState == 4 && this.status == 200) {
    		content = JSON.parse(xhttp.responseText);
        	prikaziInputeZaDodavanjeVozila(content);
        }
  	};

  	xhttp.open("GET", "http://localhost:8080/goriva", true);
  	xhttp.send();
}

function prikaziInputeZaDodavanjeVozila(listaGoriva) {
	var biciklInput = document.getElementById("biciklInput");
	var putnickoVoziloInput = document.getElementById("putnickoVoziloInput");
	var teretnoVoziloInput = document.getElementById("teretnoVoziloInput");
	var divVozilo = document.getElementById("divVozilo");
	var divPutnickoVozilo = document.getElementById("divPutnickoVozilo");
	var divTeretnoVozilo = document.getElementById("divTeretnoVozilo");
	var divVrstaVozila = document.getElementById("divVrstaVozila");
	divVrstaVozila.setAttribute("style", "display:none;");
	divVozilo.setAttribute("style", "display:block;");


	if (teretnoVoziloInput.checked) {
		divTeretnoVozilo.setAttribute("style", "display:block;");
	} else if (biciklInput.checked) {
		var nazivICenaGorivaDiv = document.getElementById("nazivICenaGorivaDiv");
		var potrosnjaGorivaDiv = document.getElementById("potrosnjaGorivaDiv");
		var brojVrataDiv = document.getElementById("brojVrataDiv");
		nazivICenaGorivaDiv.setAttribute("style", "display:none;");
		potrosnjaGorivaDiv.setAttribute("style", "display:none;");
		brojVrataDiv.setAttribute("style", "display:none;");
	}

	var select = document.getElementById("nazivICenaGoriva");

	for (let i = 0; i < listaGoriva.length; i++) {
		
		var option = document.createElement("option");
		option.setAttribute("id", listaGoriva[i]["nazivGoriva"] + " " + listaGoriva[i]["cenaGoriva"]);
		var optionText = document.createTextNode(listaGoriva[i]["nazivGoriva"] + ", " + listaGoriva[i]["cenaGoriva"] + " dinara po litri");
		option.appendChild(optionText);
		select.appendChild(option);
	}
}

function pripremiPodatkeVozila() {
	var biciklInput = document.getElementById("biciklInput");
	var putnickoVoziloInput = document.getElementById("putnickoVoziloInput");
	var teretnoVoziloInput = document.getElementById("teretnoVoziloInput");

	if (biciklInput.checked) {
		var vrstaVozila = biciklInput.value;
	} else if (putnickoVoziloInput.checked) {
		var vrstaVozila = putnickoVoziloInput.value;
	} else {
		var vrstaVozila = teretnoVoziloInput.value;
	}

	var registracioniBroj = document.getElementById("registracioniBroj").value;
	registracioniBroj = registracioniBroj.toUpperCase();
	var nazivICenaGoriva = document.getElementById("nazivICenaGoriva").value;
	var potrosnjaGoriva = document.getElementById("potrosnjaGoriva").value;
	var brKmPreServisiranja = document.getElementById("brKmPreServisiranja").value;
	var cenaServisiranja = document.getElementById("cenaServisiranja").value;
	var cenaIznajmljivanjaPoDanu = document.getElementById("cenaIznajmljivanjaPoDanu").value;
	var brojSedista = document.getElementById("brojSedista").value;
	var brojVrata = document.getElementById("brojVrata").value;
	var servisiranje = [];

	nazivICenaGoriva = nazivICenaGoriva.split(",");
	var nazivGoriva = nazivICenaGoriva[0];
	var cenaGoriva = nazivICenaGoriva[1].split(" ");
	cenaGoriva = cenaGoriva[1];
	var gorivo = {
		"nazivGoriva": nazivGoriva, 
		"cenaGoriva": cenaGoriva
	}
	console.log(vrstaVozila);

	if (vrstaVozila == "bicikl") {
		if (registracioniBroj == "" || brKmPreServisiranja == "" || cenaServisiranja == "" || 
		cenaIznajmljivanjaPoDanu == "" || brojSedista == "") {
			window.alert("Popunite sva polja.");
			return;
		}
		var type = "bicikl";
	} else {
		if (registracioniBroj == "" || potrosnjaGoriva == "" || brKmPreServisiranja == "" || cenaServisiranja == "" || 
		cenaIznajmljivanjaPoDanu == "" || brojSedista == "" || brojVrata == "") {
			window.alert("Popunite sva polja.");
			return;
		}
		var type = "putnicko vozilo";
	}

	if (vrstaVozila == "teretnoVozilo") {
		var maksimalnaMasa = document.getElementById("maksimalnaMasa").value;
		var maksimalnaVisina = document.getElementById("maksimalnaVisina").value;
		if (maksimalnaMasa == "" || maksimalnaVisina == "") {
			window.alert("Popunite sva polja.");
			return;
		}
		var type = "teretno vozilo";
	}

	var newVozilo = {
		"type": type,
		"registracioniBroj": registracioniBroj,
		"servisiranje": servisiranje,
		"predjeniKm": 0,
		"brKmPreServisiranja": parseInt(brKmPreServisiranja),
		"cenaServisiranja": parseInt(cenaServisiranja),
		"cenaIznajmljivanjaPoDanu": parseInt(cenaIznajmljivanjaPoDanu),
		"brojSedista": parseInt(brojSedista),
		"logickiObrisanoVozilo": false
	}

	if (vrstaVozila == "putnickoVozilo") {
		newVozilo["gorivo"] = gorivo;
		newVozilo["potrosnjaGoriva"] = parseInt(potrosnjaGoriva);
		newVozilo["brojVrata"] = parseInt(brojVrata);		
	} else if (vrstaVozila == "teretnoVozilo") {
		newVozilo["gorivo"] = gorivo;
		newVozilo["potrosnjaGoriva"] = parseInt(potrosnjaGoriva);
		newVozilo["brojVrata"] = parseInt(brojVrata);
		newVozilo["maksimalnaMasa"] = parseInt(maksimalnaMasa);
		newVozilo["maksimalnaVisina"] = parseInt(maksimalnaVisina);
	} else {

	}

	proveriRegistracionuTablicu(newVozilo, vrstaVozila);
}

function proveriRegistracionuTablicu(newVozilo, vrstaVozila) {
	console.log(newVozilo);
	var registracionaTablica = newVozilo["registracioniBroj"];

	var xhttp = new XMLHttpRequest();
  	xhttp.onreadystatechange = function () {
   		if (this.readyState == 4 && this.status == 200) {
    		content = JSON.parse(xhttp.responseText);
        	if(content == "Registraciona tablica je slobodna."){
        		window.alert("Registraciona tablica je slobodna.");
        		upisiVozilo(newVozilo, vrstaVozila);
        	} else {
        		window.alert("Registraciona tablica nije slobodna.");
        		odustaniOdDodavanjaVozila();
        	}
        }
  	};

  	xhttp.open("GET", "http://localhost:8080/slobodnaVozila?registracionaTablica=" + registracionaTablica, true);
  	xhttp.send();
}

function upisiVozilo(newVozilo, vrstaVozila) {

	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
        if (this.readyState == 4 && this.status == 200) {
        	content = xhttp.responseText;
        	if (content == "Server: primljeni su podaci i dodati u listu vozila") {
        		window.alert("Novo vozilo je dodato.");
        		odustaniOdDodavanjaVozila();
        	} else {
        		window.alert("Uneta registraciona tablica je zauzeta. Novo vozilo nije dodato.");
        		odustaniOdDodavanjaVozila();
        	}
        };
    }
      
    xhttp.open("POST", "http://localhost:8080/vozila", true);
    xhttp.setRequestHeader("Content-type", "text/plain");
    xhttp.send(JSON.stringify(newVozilo));
}

function odustaniOdDodavanjaVozila() {
  	var inputiTypeRadio = document.querySelectorAll('input[type="radio"]');
  	for (let i  = 0, l = inputiTypeRadio.length; i < l; i++) {
 		inputiTypeRadio[i].checked = inputiTypeRadio[i].defaultChecked;
  	}
  	$("input[type=text]").val("");
  	$("input[type=number]").val("");

  	var select = document.getElementById("nazivICenaGoriva");
  	while (select.firstChild) {
    	select.removeChild(select.firstChild);
  	}

	var nazivICenaGorivaDiv = document.getElementById("nazivICenaGorivaDiv");
	var potrosnjaGorivaDiv = document.getElementById("potrosnjaGorivaDiv");
	var brojVrataDiv = document.getElementById("brojVrataDiv");
	nazivICenaGorivaDiv.setAttribute("style", "display:block;");
	potrosnjaGorivaDiv.setAttribute("style", "display:block;");
	brojVrataDiv.setAttribute("style", "display:block;");

	var divVozilaZaDodavanje = document.getElementById("divVozilaZaDodavanje");
	divVozilaZaDodavanje.setAttribute("style", "display:none;");
	var divVrstaVozila = document.getElementById("divVrstaVozila");
	divVrstaVozila.setAttribute("style", "display:none;");

	var divVozilo = document.getElementById("divVozilo");
	var divTeretnoVozilo = document.getElementById("divTeretnoVozilo");
	divVozilo.setAttribute("style", "display:none;");
	divTeretnoVozilo.setAttribute("style", "display:none;");

	isDodavanjeVozilaClicked = false;
}

function pronadjiObrisanaVozila() {
    var xhttp = new XMLHttpRequest();  	

  	xhttp.onreadystatechange = function() {
	    if (this.readyState == 4 && this.status == 200) {
	        content = JSON.parse(xhttp.responseText);
	        if (content != "Nema obrisanih vozila.") {
	        	prikaziObrisanaVozila(content);
	    	} else {
	    		window.alert("Sva vozila su aktivna.");
	    	}
	    }
    }


	xhttp.open("GET", "http://localhost:8080/vozila?obrisanaVozila=obrisanaVozila", true);
    xhttp.send();
}

var isPrikaziObrisanaVozilaClicked = false;
function prikaziObrisanaVozila(obrisanaVozila) {
	if (!isPrikaziObrisanaVozilaClicked) {

	isPrikaziObrisanaVozilaClicked = true;
	var tabelaVozilaZaAktiviranje = document.getElementById("tabelaVozilaZaAktiviranje");
	tabelaVozilaZaAktiviranje.setAttribute("style", "display:block");
	var tabelaVozilaZaAktiviranjeTBody = document.getElementById("tabelaVozilaZaAktiviranjeTBody");


		for (let i = 0; i < obrisanaVozila.length; i++) {				
		  	var registracioniBroj = obrisanaVozila[i]["registracioniBroj"];
		  	var cenaIznajmljivanjaPoDanu = obrisanaVozila[i]["cenaIznajmljivanjaPoDanu"];
		  	var predjeniKm = obrisanaVozila[i]["predjeniKm"];
		  	var brKmPreServisiranja = obrisanaVozila[i]["brKmPreServisiranja"];

		  	var row = document.createElement("tr");
		  	tabelaVozilaZaAktiviranjeTBody.appendChild(row);

		  	var registracioniBrojRow = document.createElement("td");
		  	registracioniBrojRow.setAttribute("class", registracioniBroj + " registracioniBroj");
		  	var registracioniBrojRowTekst = document.createTextNode(registracioniBroj);
		  	registracioniBrojRow.appendChild(registracioniBrojRowTekst);
		  	row.appendChild(registracioniBrojRow);

		  	var cenaIznajmljivanjaPoDanuRow = document.createElement("td");
		  	cenaIznajmljivanjaPoDanuRow.setAttribute("id", registracioniBroj + " cenaIznajmljivanjaPoDanu");
		  	var cenaIznajmljivanjaPoDanuRowTekst = document.createTextNode(cenaIznajmljivanjaPoDanu);
		  	cenaIznajmljivanjaPoDanuRow.appendChild(cenaIznajmljivanjaPoDanuRowTekst);
		  	row.appendChild(cenaIznajmljivanjaPoDanuRow);

		  	var predjeniKmRow = document.createElement("td");
		  	predjeniKmRow.setAttribute("id", registracioniBroj + " predjeniKm");
		  	var predjeniKmRowTekst = document.createTextNode(predjeniKm);
		  	predjeniKmRow.appendChild(predjeniKmRowTekst);
		  	row.appendChild(predjeniKmRow);

		  	var brKmPreServisiranjaRow = document.createElement("td");
		  	brKmPreServisiranjaRow.setAttribute("id", registracioniBroj + " brKmPreServisiranja");
		  	var brKmPreServisiranjaRowTekst = document.createTextNode(brKmPreServisiranja);
		  	brKmPreServisiranjaRow.appendChild(brKmPreServisiranjaRowTekst);
		  	row.appendChild(brKmPreServisiranjaRow);

		  	var aktivirajteVoziloRow = document.createElement("td");
	      	aktivirajteVoziloRow.setAttribute("id", registracioniBroj + " aktivirajteVozilo");
	      	aktivirajteVoziloRow.setAttribute("style", "cursor:pointer;");
	      	aktivirajteVoziloRow.setAttribute("onclick", "aktivirajVozilo(" + '/' + registracioniBroj + '/' + ")");
	      	var aktivirajteVoziloRowTekst = document.createTextNode("Aktivirajte vozilo");
	      	aktivirajteVoziloRow.appendChild(aktivirajteVoziloRowTekst);
	      	row.appendChild(aktivirajteVoziloRow);
		}
	

	}
}

function aktivirajVozilo(registracioniBroj) {
	registracioniBroj = registracioniBroj.toString();
	registracioniBroj = registracioniBroj.replace(/\//g,'');
	console.log(registracioniBroj);


	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
			content = JSON.parse(xhttp.responseText);
        	window.alert(content);
        	odustaniOdAktiviranjaVozila();
        }
    };

  	xhttp.open("PUT", "http://localhost:8080/vozila?aktiviranjeVozila=aktiviranjeVozila", true);
  	xhttp.setRequestHeader("Content-type", "text/plain");
  	xhttp.send(registracioniBroj);
}

function odustaniOdAktiviranjaVozila() {
	var tabelaVozilaZaAktiviranje = document.getElementById("tabelaVozilaZaAktiviranje");
	tabelaVozilaZaAktiviranje.setAttribute("style", "display:none");

  	var tabelaVozilaZaAktiviranjeTBody = document.getElementById("tabelaVozilaZaAktiviranjeTBody");
  	while (tabelaVozilaZaAktiviranjeTBody.firstChild) {
    	tabelaVozilaZaAktiviranjeTBody.removeChild(tabelaVozilaZaAktiviranjeTBody.firstChild);
  	}

  	isPrikaziObrisanaVozilaClicked = false;
}