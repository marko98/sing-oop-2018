function logovanjeSluzbenika() {
  var poljaNisuPopunjena = false;
  var korisnickoIme = document.getElementById("korisnickoImeInput").value;
  var sifra = document.getElementById("sifraInput").value;
  
  var Korisnik = {
                  "korisnickoIme": korisnickoIme, 
                  "sifra" : sifra
                  };

  if (korisnickoIme == "" || sifra == "") {
    window.alert("Popunite sva polja.");
    poljaNisuPopunjena = true;
  }
    
  if (!poljaNisuPopunjena) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              content = JSON.parse(xhttp.responseText);
              if (!(content == "Nije pronadjen korisnik.")) {
                var korisnik = content.split(",");
                var ime = korisnik[0];
                var prezime = korisnik[1];
                var uloga = korisnik[2];
                var korisnickoIme = korisnik[3];
                window.alert("Uspešno ste se ulogovali.");
                localStorage.setItem("aktivniKorisnik", JSON.stringify(ime + " " + prezime + ", " + uloga + ", " + korisnickoIme));
                window.location.replace("rezervacije.html");
              } else {
                window.alert("Prijava nije uspešna, pokušajte ponovo.");
              }  
            }
          }
        }

  xhttp.open("GET", "http://localhost:8080/korisnici?korisnickoIme=" + korisnickoIme + "&sifra=" + sifra, true);
  xhttp.send();
}

function registrovanjeIznajmljivacaVozila() {
  var poljaNisuPopunjena = false;
  var korisnickoIme  = document.getElementById("registrationKorisnickoImeInput").value;
  var sifra  = document.getElementById("registrationSifraInput").value;
  var jmbg  = document.getElementById("jmbgInput").value;
  var ime = document.getElementById("imeInput").value;
  var prezime = document.getElementById("prezimeInput").value;
  var drzavljanstvo = document.getElementById("drzavljanstvoInput").value;
  var brTelefona = document.getElementById("brTelefonaInput").value;

  if (korisnickoIme == "" || sifra == "" || jmbg == "" || ime == "" || prezime == "" || 
    drzavljanstvo == "" || brTelefona == "") {
    window.alert("Popunite sva polja.");
    poljaNisuPopunjena = true;
  }

  if (!poljaNisuPopunjena) {
    var newIznajmljivacVozila = {
              "type": "iznajmljivac vozila",
              "korisnickoIme": korisnickoIme,
              "sifra": sifra,
              "jmbg": jmbg,
              "ime": ime, 
              "prezime" : prezime,
              "drzavljanstvo": drzavljanstvo,
              "brojTelefona": brTelefona
              };
      
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function () {
         if (this.readyState == 4 && this.status == 200) {
          /*document.getElementById("demo3").innerHTML = xhttp.responseText;*/
          if (xhttp.responseText == "Server: duplikat - vec postoji iznajmljivac vozila sa istim korisnickim imenom") {
            window.alert("Korisnicko ime je zauzeto, pokušajte ponovo.");
            isprazniPoljaRegistracija();
          } else {
            window.alert("Registracija je uspešna.");
            isprazniPoljaRegistracija();
          }
          }
        };
      
    xhttp.open("POST", "http://localhost:8080/korisnici", true);
    xhttp.setRequestHeader("Content-type", "text/plain");
    //console.log(xhttp.queryParams);
    xhttp.send(JSON.stringify(newIznajmljivacVozila));
  }
}

function isprazniPoljaRegistracija() {
  document.getElementById("registrationKorisnickoImeInput").value = "";
  document.getElementById("registrationSifraInput").value = "";
  document.getElementById("jmbgInput").value = "";
  document.getElementById("imeInput").value = "";
  document.getElementById("prezimeInput").value = "";
  document.getElementById("drzavljanstvoInput").value = "";
  document.getElementById("brTelefonaInput").value = "";
}

function kontakt() {
  window.alert("Kontakt telefon: 021/458-685");
}

function logout() {
  window.location.replace("loginPage.html");
}

function prikazMenija() {
  var aktivniKorisnik = JSON.parse(localStorage.getItem("aktivniKorisnik"));
  var aktivniKorisnikNiz = aktivniKorisnik.split(",");
  var uloga = aktivniKorisnikNiz[1].replace(" ", "");
  //console.log(uloga);
  var brisanjeVozilaDiv = document.getElementById("brisanjeVozilaDiv");
  var aktiviranjeObrisanihVozilaDiv = document.getElementById("aktiviranjeObrisanihVozilaDiv");
  var dodavanjeVozilaDiv = document.getElementById("dodavanjeVozilaDiv");
  var pravljenjeRezervacijaDiv = document.getElementById("pravljenjeRezervacijaDiv");
  var vracanjeVozilaDiv = document.getElementById("vracanjeVozilaDiv");

  if (uloga == "Iznajmljivac vozila"){
    brisanjeVozilaDiv.setAttribute("style", "display:none");
    aktiviranjeObrisanihVozilaDiv.setAttribute("style", "display:none");
    dodavanjeVozilaDiv.setAttribute("style", "display:none");
  } else {
    pravljenjeRezervacijaDiv.setAttribute("style", "display:none");
    vracanjeVozilaDiv.setAttribute("style", "display:none");
  }
}