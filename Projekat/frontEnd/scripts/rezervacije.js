function pravljenjeRezervacije() {
  var trazenjeOdgovarajucihVozilaDiv = document.getElementById("trazenjeOdgovarajucihVozilaDiv");
  trazenjeOdgovarajucihVozilaDiv.setAttribute("style", "display:block");
  var pretraziDugme = document.getElementById("pretraziVozila");
  pretraziDugme.setAttribute("style", "display:block");
}

function trazenjeOdgovarajucihVozila() {

  var datumPocetkaRezervacije = document.getElementById("datumPocetkaRezervacije").value;
  var datumKrajaRezervacije = document.getElementById("datumKrajaRezervacije").value;

  console.log(datumPocetkaRezervacije);
  console.log(datumKrajaRezervacije);
  if (datumPocetkaRezervacije != "" && datumKrajaRezervacije != "") {
    var datumPocetkaRezervacijeNiz = datumPocetkaRezervacije.split("-");
    var datumKrajaRezervacijeNiz = datumKrajaRezervacije.split("-");

    var datumPocetkaRezervacijeDate = new Date(datumPocetkaRezervacijeNiz[0], (parseInt(datumPocetkaRezervacijeNiz[1])-1).toString(), datumPocetkaRezervacijeNiz[2]);
    var datumKrajaRezervacijeDate = new Date(datumKrajaRezervacijeNiz[0], (parseInt(datumKrajaRezervacijeNiz[1])-1).toString(), datumKrajaRezervacijeNiz[2]);
      
    var danasnjiDatum = new Date();
    console.log(datumKrajaRezervacijeDate);
    console.log(datumPocetkaRezervacijeDate);
    console.log(danasnjiDatum);


    var SvaVozila = document.getElementById("SvaVozila");
    var Bicikl = document.getElementById("Bicikl");
    var PutnickoVozilo = document.getElementById("PutnickoVozilo");
    var TeretnoVozilo = document.getElementById("TeretnoVozilo");

    if (datumPocetkaRezervacijeDate < danasnjiDatum) {
      window.alert("Unesite ispravan datum početka rezervacije.");
      return;
    }

    if (datumKrajaRezervacijeDate < datumPocetkaRezervacijeDate) {
      window.alert("Datum kraja rezervacije nije ispravan.");
    } else {

      var trajanjeDanaUMilisekundama = 1000*60*60*24;    
  	  var datumPocetkaRezervacijeDate_ms = datumPocetkaRezervacijeDate.getTime();   
  		var datumKrajaRezervacijeDate_ms = datumKrajaRezervacijeDate.getTime();   
  		var razlika_ms = datumKrajaRezervacijeDate_ms - datumPocetkaRezervacijeDate_ms;
  		var brojDanaRezervacije = Math.round(razlika_ms/trajanjeDanaUMilisekundama);

  		window.localStorage.setItem("brojDanaRezervacije", JSON.stringify(brojDanaRezervacije));

    	var xhttp = new XMLHttpRequest();
    	xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              content = JSON.parse(xhttp.responseText);
              if (Bicikl.checked) {
                podaciOSlobodnimVozilima(content, "bicikl");
              } else if (PutnickoVozilo.checked) {
                podaciOSlobodnimVozilima(content, "putnicko vozilo");
              } else if (TeretnoVozilo.checked) {
                podaciOSlobodnimVozilima(content, "teretno vozilo");
              }
          }
        }

      if (Bicikl.checked) {
        xhttp.open("GET", "http://localhost:8080/slobodnaVozila?vrstaVozila=Bicikl&datumPocetkaRezervacije=" + datumPocetkaRezervacije + 
          "&datumKrajaRezervacije=" + datumKrajaRezervacije, true);
        xhttp.send();
    	} else if (PutnickoVozilo.checked) {
	    	xhttp.open("GET", "http://localhost:8080/slobodnaVozila?vrstaVozila=PutnickoVozilo&datumPocetkaRezervacije=" + datumPocetkaRezervacije + 
	          "&datumKrajaRezervacije=" + datumKrajaRezervacije, true);
	        xhttp.send();
    	} else if (TeretnoVozilo.checked) {
	    	xhttp.open("GET", "http://localhost:8080/slobodnaVozila?vrstaVozila=TeretnoVozilo&datumPocetkaRezervacije=" + datumPocetkaRezervacije + 
	          "&datumKrajaRezervacije=" + datumKrajaRezervacije, true);
	        xhttp.send();
    	} else {
		    var xhttpBicikl = new XMLHttpRequest();
		  	var xhttpPutnickoVozilo = new XMLHttpRequest();
		  	var xhttpTeretnoVozilo = new XMLHttpRequest();		  	

		  	xhttpBicikl.onreadystatechange = function() {
	        if (this.readyState == 4 && this.status == 200) {
	            contentBicikl = JSON.parse(xhttpBicikl.responseText);
	            podaciOSlobodnimVozilima(contentBicikl, "bicikl");
	            }
	        }

		  	xhttpPutnickoVozilo.onreadystatechange = function() {
  		    if (this.readyState == 4 && this.status == 200) {
  		        contentPutnickoVozilo = JSON.parse(xhttpPutnickoVozilo.responseText);
  		        podaciOSlobodnimVozilima(contentPutnickoVozilo, "putnicko vozilo");
  		        }
  		    }

		  	xhttpTeretnoVozilo.onreadystatechange = function() {
  		    if (this.readyState == 4 && this.status == 200) {
  		        contentTeretnoVozilo = JSON.parse(xhttpTeretnoVozilo.responseText);
  		        podaciOSlobodnimVozilima(contentTeretnoVozilo, "teretno vozilo");
  		        }
  		    }

			 xhttpBicikl.open("GET", "http://localhost:8080/slobodnaVozila?vrstaVozila=Bicikl&datumPocetkaRezervacije=" + datumPocetkaRezervacije + 
		    "&datumKrajaRezervacije=" + datumKrajaRezervacije, true);
		    xhttpBicikl.send();

		    xhttpPutnickoVozilo.open("GET", "http://localhost:8080/slobodnaVozila?vrstaVozila=PutnickoVozilo&datumPocetkaRezervacije=" + datumPocetkaRezervacije + 
		    "&datumKrajaRezervacije=" + datumKrajaRezervacije, true);
		    xhttpPutnickoVozilo.send();

		    xhttpTeretnoVozilo.open("GET", "http://localhost:8080/slobodnaVozila?vrstaVozila=TeretnoVozilo&datumPocetkaRezervacije=" + datumPocetkaRezervacije + 
		    "&datumKrajaRezervacije=" + datumKrajaRezervacije, true);
		    xhttpTeretnoVozilo.send();
     	}
    }
  } else {
    window.alert("Unesite podatke.")
  }
}

var isPretragaVrstaVozilaBiciklClicked = false;
var isPretragaVrstaVozilaPutnickoVoziloClicked = false;
var isPretragaVrstaVozilaTeretnoVoziloClicked = false;

function podaciOSlobodnimVozilima(podaciOSlobodnimVozilima, vrstaVozila) {
  if (podaciOSlobodnimVozilima != "Svi biciklovi su zauzeti." &&
      podaciOSlobodnimVozilima != "Sva putnicka vozila su zauzeta." &&
      podaciOSlobodnimVozilima != "Sva teretna vozila su zauzeta.") {

    if (vrstaVozila == "bicikl") {

      var sviSlobodniBicikli = [];

      for (let i = 0; i < podaciOSlobodnimVozilima.length; i++) {

        var tipVozila = "Bicikl";
        var registracioniBroj = podaciOSlobodnimVozilima[i]["registracioniBroj"];
        var cenaIznajmljivanjaPoDanu = podaciOSlobodnimVozilima[i]["cenaIznajmljivanjaPoDanu"];
        var predjeniKm = podaciOSlobodnimVozilima[i]["predjeniKm"];
        var brKmPreServisiranja = podaciOSlobodnimVozilima[i]["brKmPreServisiranja"];
        var listaServisiranja = podaciOSlobodnimVozilima[i]["servisiranje"];

        if (listaServisiranja.length == 0) {
        	var brKmNaPoslednjemServisiranju = "Na vozilu nije rađen servis.";
        } else {
	        for (let i = 0; i < listaServisiranja.length; i++) {
	        	var brKmNaPoslednjemServisiranju = listaServisiranja[i]["brPredjenihKmUTrenutkuServisiranja"];
	        }
	    }

        var slobodanBicikl = {
          "tipVozila": tipVozila,
          "registracioniBroj": registracioniBroj,
          "cenaIznajmljivanjaPoDanu": cenaIznajmljivanjaPoDanu,
          "predjeniKm": predjeniKm,
          "brKmNaPoslednjemServisiranju": brKmNaPoslednjemServisiranju,
          "brKmPreServisiranja": brKmPreServisiranja
        }

        sviSlobodniBicikli.push(slobodanBicikl);
      }

      if (!isPretragaVrstaVozilaBiciklClicked) {
        prikazSlobodnihVozila(sviSlobodniBicikli);
      }
      isPretragaVrstaVozilaBiciklClicked = true;

    } else if (vrstaVozila == "putnicko vozilo") {

      var svaSlobodnaPutnickaVozila = [];

      for (let i = 0; i < podaciOSlobodnimVozilima.length; i++) {

        var tipVozila = "Putnicko vozilo";
        var registracioniBroj = podaciOSlobodnimVozilima[i]["registracioniBroj"];
        var cenaIznajmljivanjaPoDanu = podaciOSlobodnimVozilima[i]["cenaIznajmljivanjaPoDanu"];
        var predjeniKm = podaciOSlobodnimVozilima[i]["predjeniKm"];
        var brKmPreServisiranja = podaciOSlobodnimVozilima[i]["brKmPreServisiranja"];
        var listaServisiranja = podaciOSlobodnimVozilima[i]["servisiranje"];
        
        if (listaServisiranja.length == 0) {
        	var brKmNaPoslednjemServisiranju = "Na vozilu nije rađen servis.";
        } else {
	        for (let i = 0; i < listaServisiranja.length; i++) {
	        	var brKmNaPoslednjemServisiranju = listaServisiranja[i]["brPredjenihKmUTrenutkuServisiranja"];
	        }
	    }

        var slobodnoPutnickoVozilo = {
          "tipVozila": tipVozila,
          "registracioniBroj": registracioniBroj,
          "cenaIznajmljivanjaPoDanu": cenaIznajmljivanjaPoDanu,
          "predjeniKm": predjeniKm,
          "brKmNaPoslednjemServisiranju": brKmNaPoslednjemServisiranju,
          "brKmPreServisiranja": brKmPreServisiranja
        }

        svaSlobodnaPutnickaVozila.push(slobodnoPutnickoVozilo);
      }

      if (!isPretragaVrstaVozilaPutnickoVoziloClicked) {
        prikazSlobodnihVozila(svaSlobodnaPutnickaVozila);
      }
      isPretragaVrstaVozilaPutnickoVoziloClicked = true;

    } else {

      var svaSlobodnaTeretnaVozila = [];

      for (let i = 0; i < podaciOSlobodnimVozilima.length; i++) {

        var tipVozila = "Teretno vozilo";
        var registracioniBroj = podaciOSlobodnimVozilima[i]["registracioniBroj"];
        var cenaIznajmljivanjaPoDanu = podaciOSlobodnimVozilima[i]["cenaIznajmljivanjaPoDanu"];
        var predjeniKm = podaciOSlobodnimVozilima[i]["predjeniKm"];
        var brKmPreServisiranja = podaciOSlobodnimVozilima[i]["brKmPreServisiranja"];
        var listaServisiranja = podaciOSlobodnimVozilima[i]["servisiranje"];
        
        if (listaServisiranja.length == 0) {
        	var brKmNaPoslednjemServisiranju = "Na vozilu nije rađen servis.";
        } else {
	        for (let i = 0; i < listaServisiranja.length; i++) {
	        	var brKmNaPoslednjemServisiranju = listaServisiranja[i]["brPredjenihKmUTrenutkuServisiranja"];
	        }
	    }

        var slobodnoTeretnoVozilo = {
          "tipVozila": tipVozila,
          "registracioniBroj": registracioniBroj,
          "cenaIznajmljivanjaPoDanu": cenaIznajmljivanjaPoDanu,
          "predjeniKm": predjeniKm,
          "brKmNaPoslednjemServisiranju": brKmNaPoslednjemServisiranju,
          "brKmPreServisiranja": brKmPreServisiranja
        }

        svaSlobodnaTeretnaVozila.push(slobodnoTeretnoVozilo);
      }

      if (!isPretragaVrstaVozilaTeretnoVoziloClicked) {
        prikazSlobodnihVozila(svaSlobodnaTeretnaVozila);
      }

      isPretragaVrstaVozilaTeretnoVoziloClicked = true;
    }
  } else {
    window.alert(podaciOSlobodnimVozilima);
  }
}

function prikazSlobodnihVozila(listaJedneVrsteVozila) {
  var tabelaSlobodnaVozilaDiv = document.getElementById("tabelaSlobodnaVozila");
  var tabelaSlobodnaVozilaTBody = document.getElementById("tabelaSlobodnaVozilaTBody");

  if (listaJedneVrsteVozila.length != 0) {
	  document.getElementById("ponistiPravljenjeRezervacijeH5").setAttribute("style", "display:none");
	  document.getElementById("ponistiPravljenjeRezervacijeDugme").setAttribute("style", "display:none");
  }

  tabelaSlobodnaVozilaDiv.setAttribute("style", "display:block");

  for (let i = 0; i < listaJedneVrsteVozila.length; i++) {
    var tipVozila = listaJedneVrsteVozila[i]["tipVozila"];
    var registracioniBroj = listaJedneVrsteVozila[i]["registracioniBroj"];
    var cenaIznajmljivanjaPoDanu = listaJedneVrsteVozila[i]["cenaIznajmljivanjaPoDanu"];
    var predjeniKm = listaJedneVrsteVozila[i]["predjeniKm"];
    var brKmNaPoslednjemServisiranju = listaJedneVrsteVozila[i]["brKmNaPoslednjemServisiranju"];
    var brKmPreServisiranja = listaJedneVrsteVozila[i]["brKmPreServisiranja"];

    var row = document.createElement("tr");
    tabelaSlobodnaVozilaTBody.appendChild(row);

    var tipVozilaRow = document.createElement("td");
    tipVozilaRow.setAttribute("id", registracioniBroj + " tipVozila");
    var tipVozilaRowTekst = document.createTextNode(tipVozila);
    tipVozilaRow.appendChild(tipVozilaRowTekst);
    row.appendChild(tipVozilaRow);

    var registracioniBrojRow = document.createElement("td");
    registracioniBrojRow.setAttribute("class", registracioniBroj + " registracioniBroj");
    var registracioniBrojRowTekst = document.createTextNode(registracioniBroj);
    registracioniBrojRow.appendChild(registracioniBrojRowTekst);
    row.appendChild(registracioniBrojRow);

    var cenaIznajmljivanjaPoDanuRow = document.createElement("td");
    cenaIznajmljivanjaPoDanuRow.setAttribute("id", registracioniBroj + " cenaIznajmljivanjaPoDanu");
    var cenaIznajmljivanjaPoDanuRowTekst = document.createTextNode(cenaIznajmljivanjaPoDanu);
    cenaIznajmljivanjaPoDanuRow.appendChild(cenaIznajmljivanjaPoDanuRowTekst);
    row.appendChild(cenaIznajmljivanjaPoDanuRow);

    var predjeniKmRow = document.createElement("td");
    predjeniKmRow.setAttribute("id", registracioniBroj + " predjeniKm");
    var predjeniKmRowTekst = document.createTextNode(predjeniKm);
    predjeniKmRow.appendChild(predjeniKmRowTekst);
    row.appendChild(predjeniKmRow);

    var brKmNaPoslednjemServisiranjuRow = document.createElement("td");
    brKmNaPoslednjemServisiranjuRow.setAttribute("id", registracioniBroj + " brKmNaPoslednjemServisiranju");
    var brKmNaPoslednjemServisiranjuRowTekst = document.createTextNode(brKmNaPoslednjemServisiranju);
    brKmNaPoslednjemServisiranjuRow.appendChild(brKmNaPoslednjemServisiranjuRowTekst);
    row.appendChild(brKmNaPoslednjemServisiranjuRow);

    var brKmPreServisiranjaRow = document.createElement("td");
    brKmPreServisiranjaRow.setAttribute("id", registracioniBroj + " brKmPreServisiranja");
    var brKmPreServisiranjaRowTekst = document.createTextNode(brKmPreServisiranja);
    brKmPreServisiranjaRow.appendChild(brKmPreServisiranjaRowTekst);
    row.appendChild(brKmPreServisiranjaRow);

    var cenaIznajmljivanjaRow = document.createElement("td");
    cenaIznajmljivanjaRow.setAttribute("id", registracioniBroj + " cenaIznajmljivanja");
    var cenaIznajmljivanjaRowTekst = document.createTextNode("Unesite broj kilometara.");
    cenaIznajmljivanjaRow.appendChild(cenaIznajmljivanjaRowTekst);
    row.appendChild(cenaIznajmljivanjaRow);
  }
}

function ponistiPravljenjeRezervacije() {
  $("input[type=number]").val("0");
  $("input[type=date]").val("");
  var inputiTypeRadio = document.querySelectorAll('input[type="radio"]');
  for (let i  = 0, l = inputiTypeRadio.length; i < l; i++) {
      inputiTypeRadio[i].checked = inputiTypeRadio[i].defaultChecked;
  }

  document.getElementById("ponistiPravljenjeRezervacijeH5").setAttribute("style", "display:block");
  document.getElementById("ponistiPravljenjeRezervacijeDugme").setAttribute("style", "display:block");

  var tabelaSlobodnaVozilaDiv = document.getElementById("tabelaSlobodnaVozila");
  var tabelaSlobodnaVozilaTBody = document.getElementById("tabelaSlobodnaVozilaTBody");
  var trazenjeOdgovarajucihVozilaDiv = document.getElementById("trazenjeOdgovarajucihVozilaDiv");

  isPretragaVrstaVozilaBiciklClicked = false;
  isPretragaVrstaVozilaPutnickoVoziloClicked = false;
  isPretragaVrstaVozilaTeretnoVoziloClicked = false;

  tabelaSlobodnaVozilaDiv.setAttribute("style", "display:none");
  trazenjeOdgovarajucihVozilaDiv.setAttribute("style", "display:none");

  while (tabelaSlobodnaVozilaTBody.firstChild) {
    tabelaSlobodnaVozilaTBody.removeChild(tabelaSlobodnaVozilaTBody.firstChild);
  }
}

var isPretragaRezervacijaClicked = false;
function pretragaRezervacija(mogucnost) {

  var iznajmljivac = JSON.parse(localStorage.getItem("aktivniKorisnik"));
  var iznajmljivacNiz = iznajmljivac.split(",");
  var korisnickoIme = iznajmljivacNiz[2].replace(" ", "");
  var uloga = iznajmljivacNiz[1].replace(" ", "");

  if (!isPretragaRezervacijaClicked) {
    var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              content = JSON.parse(xhttp.responseText);
              if (content == "Nema aktivnih rezervacija.") {
                window.alert(content);
              } else if (content == "Iznajmljivac nema aktivnih rezervacija ili ih nije ni pravio.") {
                window.alert(content);

              } else {

              isPretragaRezervacijaClicked = true;

              for (let i = 0; i < content.length; i++) {
                var ime = content[i]["iznajmljivac"]["ime"];
                var prezime = content[i]["iznajmljivac"]["prezime"];
                var brTelefona = content[i]["iznajmljivac"]["brojTelefona"];
                var drzavljanstvo = content[i]["iznajmljivac"]["drzavljanstvo"];
                var vozilo = content[i]["rezervisanoVozilo"]["type"];
                var registracioneTablice = content[i]["rezervisanoVozilo"]["registracioniBroj"];
                var datumPocetkaRezervacije = content[i]["datumPocetkaRezervacije"];
                var datumKrajaRezervacije = content[i]["datumKrajaRezervacije"];
                var cenaRezervacije = content[i]["cenaRezervacije"];

                console.log(vozilo);
                console.log(registracioneTablice);

                prikaziRezervacije(ime, prezime, brTelefona, drzavljanstvo, vozilo, registracioneTablice, 
                  datumPocetkaRezervacije, datumKrajaRezervacije, cenaRezervacije, mogucnost);
              }
            }
          }
      }

      if (uloga == "Iznajmljivac vozila"){
        xhttp.open("GET", "http://localhost:8080/rezervacije?korisnickoIme=" + korisnickoIme, true);
        xhttp.send();      
      } else {
        xhttp.open("GET", "http://localhost:8080/rezervacije", true);
        xhttp.send();
      }
  }
}

function prikaziRezervacije(ime, prezime, brTelefona, drzavljanstvo, vozilo, registracioneTablice, 
  datumPocetkaRezervacije, datumKrajaRezervacije, cenaRezervacije, mogucnost) {

  var row = document.createElement("tr");
  if (mogucnost == "vracanjeVozila") {
    var tabelaRezervacijeVracanjeVozila = document.getElementById("tabelaRezervacijeVracanjeVozila");
    tabelaRezervacijeVracanjeVozila.setAttribute("style", "display:block");
    var tabelaRezervacijeVracanjeVozilaTBody = document.getElementById("tabelaRezervacijeVracanjeVozilaTBody"); 
    tabelaRezervacijeVracanjeVozilaTBody.appendChild(row);
  } else {
    var tabelaAktivneRezervacijeDiv = document.getElementById("tabelaAktivneRezervacije");
    var tabelaAktivneRezervacijeTBody = document.getElementById("tabelaAktivneRezervacijeTBody");
    var tabelaAktivneRezervacijeH4 = document.getElementById("tabelaAktivneRezervacijeH4");
    tabelaAktivneRezervacijeDiv.setAttribute("style", "display:block");
    tabelaAktivneRezervacijeH4.setAttribute("style", "display:block");
    tabelaAktivneRezervacijeTBody.appendChild(row);
  }

  var imeRow = document.createElement("td");
  var imeRowTekst = document.createTextNode(ime);
  imeRow.appendChild(imeRowTekst);
  row.appendChild(imeRow);

  var prezimeRow = document.createElement("td");
  var prezimeRowTekst = document.createTextNode(prezime);
  prezimeRow.appendChild(prezimeRowTekst);
  row.appendChild(prezimeRow);

  var brTelefonaRow = document.createElement("td");
  var brTelefonaRowTekst = document.createTextNode(brTelefona);
  brTelefonaRow.appendChild(brTelefonaRowTekst);
  row.appendChild(brTelefonaRow);

  var drzavljanstvoRow = document.createElement("td");
  var drzavljanstvoRowTekst = document.createTextNode(drzavljanstvo);
  drzavljanstvoRow.appendChild(drzavljanstvoRowTekst);
  row.appendChild(drzavljanstvoRow);

  var voziloRow = document.createElement("td");
  var voziloRowTekst = document.createTextNode(vozilo);
  voziloRow.appendChild(voziloRowTekst);
  row.appendChild(voziloRow);

  var registracioneTabliceRow = document.createElement("td");
  var registracioneTabliceRowTekst = document.createTextNode(registracioneTablice);
  registracioneTabliceRow.appendChild(registracioneTabliceRowTekst);
  row.appendChild(registracioneTabliceRow);

  var datumPocetkaRezervacijeRow = document.createElement("td");
  var datumPocetkaRezervacijeRowTekst = document.createTextNode(datumPocetkaRezervacije);
  datumPocetkaRezervacijeRow.appendChild(datumPocetkaRezervacijeRowTekst);
  row.appendChild(datumPocetkaRezervacijeRow);

  var datumKrajaRezervacijeRow = document.createElement("td");
  var datumKrajaRezervacijeRowTekst = document.createTextNode(datumKrajaRezervacije);
  datumKrajaRezervacijeRow.appendChild(datumKrajaRezervacijeRowTekst);
  row.appendChild(datumKrajaRezervacijeRow);

  var cenaRezervacijeRow = document.createElement("td");
  if (mogucnost == "vracanjeVozila") {
    var cenaRezervacijeRowTekst = document.createTextNode("Cena će biti prikazana nakon klika na opciju vratite vozilo");
  } else {
    var cenaRezervacijeRowTekst = document.createTextNode(cenaRezervacije);
  }
  cenaRezervacijeRow.appendChild(cenaRezervacijeRowTekst);
  row.appendChild(cenaRezervacijeRow);

  var obrisiRow = document.createElement("td");
  var obrisiRowP = document.createElement("p");
  obrisiRowP.setAttribute("style", "cursor:pointer");
  if (mogucnost == "vracanjeVozila") {
    obrisiRowP.setAttribute("onclick", "vratiVozilo(" + '/' + registracioneTablice + "," + datumPocetkaRezervacije + 
                                                                      "," + datumKrajaRezervacije + "," + vozilo + '/' + ")");
    var obrisiRowTekst = document.createTextNode("Vratite vozilo");
  } else {
    obrisiRowP.setAttribute("onclick", "ponistiOtkazivanjeRezervacije(" + '/' + registracioneTablice + "," + datumPocetkaRezervacije + 
                                                                      "," + datumKrajaRezervacije + '/' + ")");
    var obrisiRowTekst = document.createTextNode("Obriši");
  }
  obrisiRowP.appendChild(obrisiRowTekst);
  obrisiRow.appendChild(obrisiRowP);
  row.appendChild(obrisiRow);    
}

function ponistiOtkazivanjeRezervacije(podaciZaBrisanjeRezervacije) {
  podaciZaBrisanjeRezervacije = podaciZaBrisanjeRezervacije.toString();
  podaciZaBrisanjeRezervacije = podaciZaBrisanjeRezervacije.replace(/\//g,'');
  podaciZaBrisanjeRezervacije = podaciZaBrisanjeRezervacije.split(",");
  
  var registracioneTablice = podaciZaBrisanjeRezervacije[0];
  var datumPocetkaRezervacije = podaciZaBrisanjeRezervacije[1];
  var datumKrajaRezervacije = podaciZaBrisanjeRezervacije[2];

  obrisiRezervaciju(registracioneTablice, datumPocetkaRezervacije, datumKrajaRezervacije);

  var tabelaAktivneRezervacijeDiv = document.getElementById("tabelaAktivneRezervacije");
  var tabelaAktivneRezervacijeTBody = document.getElementById("tabelaAktivneRezervacijeTBody");
  var tabelaAktivneRezervacijeH4 = document.getElementById("tabelaAktivneRezervacijeH4");

  isPretragaRezervacijaClicked = false;

  tabelaAktivneRezervacijeDiv.setAttribute("style", "display:none");
  tabelaAktivneRezervacijeH4.setAttribute("style", "display:none");

  while (tabelaAktivneRezervacijeTBody.firstChild) {
    tabelaAktivneRezervacijeTBody.removeChild(tabelaAktivneRezervacijeTBody.firstChild);
  }
}

function ponistiOtkazivanjeRezervacijeForma() {
  var tabelaAktivneRezervacijeDiv = document.getElementById("tabelaAktivneRezervacije");
  var tabelaAktivneRezervacijeTBody = document.getElementById("tabelaAktivneRezervacijeTBody");
  var tabelaAktivneRezervacijeH4 = document.getElementById("tabelaAktivneRezervacijeH4");

  isPretragaRezervacijaClicked = false;

  tabelaAktivneRezervacijeDiv.setAttribute("style", "display:none");
  tabelaAktivneRezervacijeH4.setAttribute("style", "display:none");

  while (tabelaAktivneRezervacijeTBody.firstChild) {
    tabelaAktivneRezervacijeTBody.removeChild(tabelaAktivneRezervacijeTBody.firstChild);
  }
}

function obrisiRezervaciju(registracioneTablice, datumPocetkaRezervacije, datumKrajaRezervacije) {

  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
          content = JSON.parse(xhttp.responseText);
          window.alert(content);
        }
    };

  xhttp.open("PUT", "http://localhost:8080/rezervacije?obrisiRegistracioneTablice=obrisiRegistracioneTablice", true);
  xhttp.setRequestHeader("Content-type", "text/plain");
  xhttp.send(JSON.stringify("," + registracioneTablice + "," + datumPocetkaRezervacije + "," + datumKrajaRezervacije  + ","));
}

function azurirajTrazenjeOdgovarajucihVozila() {
	var pretraziDugme = document.getElementById("pretraziVozila");
	pretraziDugme.setAttribute("style", "display:none");

	var tds = document.getElementById("tabelaSlobodnaVozilaTable").getElementsByTagName("td");
	var brojDanaRezervacije = localStorage.getItem("brojDanaRezervacije");

	var listaTablica = [];

	for (let i = 0; i < tds.length; i++) {
		if (tds[i].className.includes("registracioniBroj")) {
			listaTablica.push(tds[i].innerHTML);
		}
	}

	var planiraniKilometri = document.getElementById("brojKilometaraInput").value;

  if (planiraniKilometri == "") {
    window.alert("Unesite broj kilometara koje planirate da pređete.")
    return;
  }	else if (planiraniKilometri < 0) {
		window.alert("Unesite broj kilometara koje planirate da pređete.")
		return;
	}

	var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              content = JSON.parse(xhttp.responseText);
              //console.log(content);
              if (content.length == 0) {
              	window.alert("Planirane kilometre nije moguće preći, zbog broja dana rezervacije.");
              	return;
              }
              azuriranjePrikazaSlobodnihVozila(content);
          }
      }

	xhttp.open("GET", "http://localhost:8080/racunanjeTroskova?planiraniKilometri=" + planiraniKilometri +
		"&registracioneTablice=" + listaTablica + "&brojDanaRezervacije=" + brojDanaRezervacije, true);
	xhttp.send();
}

function azuriranjePrikazaSlobodnihVozila(listaSaPodacimaCena) {
	var listaVozilaSaCenamaIznajmljivanja = [];

	var listaVozilaICena = [];

	for (let i = 0; i < listaSaPodacimaCena.length; i++) {
		if (listaSaPodacimaCena[i] == "|") {
			listaVozilaSaCenamaIznajmljivanja.push(listaVozilaICena);
			listaVozilaICena = [];
		} else {
			listaVozilaICena.push(listaSaPodacimaCena[i]);
		}
	}

	function sortByKey(array) {
	    return array.sort(function(a, b) {
	        var x = parseInt(a[3]); var y = parseInt(b[3]);
	        return ((x < y) ? -1 : ((x > y) ? 1 : 0));
	    });
	}

	sortByKey(listaVozilaSaCenamaIznajmljivanja);

	var tds = document.getElementById("tabelaSlobodnaVozilaTable").getElementsByTagName("td");

	var listaTablica = [];
	var listaTablicaICena = [];


	for (let j = 0; j < listaVozilaSaCenamaIznajmljivanja.length; j++) {
		for (let i = 0; i < tds.length; i++) {
			if (tds[i].id.includes(listaVozilaSaCenamaIznajmljivanja[j][0]) || 
				tds[i].className.includes(listaVozilaSaCenamaIznajmljivanja[j][0])) {
				if (!tds[i].id.includes("rezervacija")) {
					listaTablica.push(tds[i].innerHTML);
				}
			}
		}
		listaTablica.push(listaVozilaSaCenamaIznajmljivanja[j][1]);
		listaTablica.push(listaVozilaSaCenamaIznajmljivanja[j][2]);
		listaTablica.push(listaVozilaSaCenamaIznajmljivanja[j][3]);

		listaTablicaICena.push(listaTablica);
		listaTablica = [];
	}

	var tabelaSlobodnaVozilaTBody = document.getElementById("tabelaSlobodnaVozilaTBody");
	
	while (tabelaSlobodnaVozilaTBody.firstChild) {
  		tabelaSlobodnaVozilaTBody.removeChild(tabelaSlobodnaVozilaTBody.firstChild);
	}

	azurirajPrikazSlobodnihVozila(listaTablicaICena);
}

function azurirajPrikazSlobodnihVozila(listaTablicaICena) {
  var tabelaSlobodnaVozilaTBody = document.getElementById("tabelaSlobodnaVozilaTBody");

  for (let i = 0; i < listaTablicaICena.length; i++) {

    var tipVozila = listaTablicaICena[i][0];
    var registracioniBroj = listaTablicaICena[i][1];
    var cenaIznajmljivanjaPoDanu = listaTablicaICena[i][2];
    var predjeniKm = listaTablicaICena[i][3];
    var brKmNaPoslednjemServisiranju = listaTablicaICena[i][4];
    var brKmPreServisiranja = listaTablicaICena[i][5];
    var cenaIznajmljivanja = listaTablicaICena[i][9];

    var row = document.createElement("tr");
    tabelaSlobodnaVozilaTBody.appendChild(row);

    var tipVozilaRow = document.createElement("td");
    tipVozilaRow.setAttribute("id", registracioniBroj + " tipVozila");
    var tipVozilaRowTekst = document.createTextNode(tipVozila);
    tipVozilaRow.appendChild(tipVozilaRowTekst);
    row.appendChild(tipVozilaRow);

    var registracioniBrojRow = document.createElement("td");
    registracioniBrojRow.setAttribute("class", registracioniBroj + " registracioniBroj");
    var registracioniBrojRowTekst = document.createTextNode(registracioniBroj);
    registracioniBrojRow.appendChild(registracioniBrojRowTekst);
    row.appendChild(registracioniBrojRow);

    var cenaIznajmljivanjaPoDanuRow = document.createElement("td");
    cenaIznajmljivanjaPoDanuRow.setAttribute("id", registracioniBroj + " cenaIznajmljivanjaPoDanu");
    var cenaIznajmljivanjaPoDanuRowTekst = document.createTextNode(cenaIznajmljivanjaPoDanu);
    cenaIznajmljivanjaPoDanuRow.appendChild(cenaIznajmljivanjaPoDanuRowTekst);
    row.appendChild(cenaIznajmljivanjaPoDanuRow);

    var predjeniKmRow = document.createElement("td");
    predjeniKmRow.setAttribute("id", registracioniBroj + " predjeniKm");
    var predjeniKmRowTekst = document.createTextNode(predjeniKm);
    predjeniKmRow.appendChild(predjeniKmRowTekst);
    row.appendChild(predjeniKmRow);

    var brKmNaPoslednjemServisiranjuRow = document.createElement("td");
    brKmNaPoslednjemServisiranjuRow.setAttribute("id", registracioniBroj + " brKmNaPoslednjemServisiranju");
    var brKmNaPoslednjemServisiranjuRowTekst = document.createTextNode(brKmNaPoslednjemServisiranju);
    brKmNaPoslednjemServisiranjuRow.appendChild(brKmNaPoslednjemServisiranjuRowTekst);
    row.appendChild(brKmNaPoslednjemServisiranjuRow);

    var brKmPreServisiranjaRow = document.createElement("td");
    brKmPreServisiranjaRow.setAttribute("id", registracioniBroj + " brKmPreServisiranja");
    var brKmPreServisiranjaRowTekst = document.createTextNode(brKmPreServisiranja);
    brKmPreServisiranjaRow.appendChild(brKmPreServisiranjaRowTekst);
    row.appendChild(brKmPreServisiranjaRow);

    var cenaIznajmljivanjaRow = document.createElement("td");
    cenaIznajmljivanjaRow.setAttribute("id", registracioniBroj + " cenaIznajmljivanja");
    var cenaIznajmljivanjaRowTekst = document.createTextNode(cenaIznajmljivanja);
    cenaIznajmljivanjaRow.appendChild(cenaIznajmljivanjaRowTekst);
    row.appendChild(cenaIznajmljivanjaRow);

    var rezervacijaRow = document.createElement("td");
    rezervacijaRow.setAttribute("id", registracioniBroj + " rezervacija");
    rezervacijaRow.setAttribute("style", "cursor:pointer");
    rezervacijaRow.setAttribute("onclick", "rezervisiVozilo(" + '/' + registracioniBroj + "," + cenaIznajmljivanja + '/' + ")");
    var rezervacijaRowTekst = document.createTextNode("Rezerviši");
    rezervacijaRow.appendChild(rezervacijaRowTekst);
    row.appendChild(rezervacijaRow);
  }
}

function rezervisiVozilo(podaci) {
	podaci = podaci.toString();
	podaci = podaci.replace(/\//g,'');
  podaci = podaci.split(",");
  //console.log(registracioniBroj);
  registracioniBroj = podaci[0];
  var cenaRezervacije = parseInt(podaci[1]);
  window.alert("Zelite da rezervisete vozilo registarskih tablica: " + registracioniBroj);

  var aktivniKorisnik = JSON.parse(localStorage.getItem("aktivniKorisnik"));
  var aktivniKorisnikNiz = aktivniKorisnik.split(",");
  var imeAktivnogKorisnika = aktivniKorisnikNiz[2].replace(" ", "");

  var datumPocetkaRezervacije = document.getElementById("datumPocetkaRezervacije").value;
  var datumKrajaRezervacije = document.getElementById("datumKrajaRezervacije").value;

  var xhttpKorisnik = new XMLHttpRequest();
  xhttpKorisnik.onreadystatechange = function () {
       if (this.readyState == 4 && this.status == 200) {
        contentKorisnik = JSON.parse(xhttpKorisnik.responseText);
        }
      };
  xhttpKorisnik.open("GET", "http://localhost:8080/korisnici?korisnickoIme=" + imeAktivnogKorisnika, true);
  xhttpKorisnik.send();
      
  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
       if (this.readyState == 4 && this.status == 200) {
        content = JSON.parse(xhttp.responseText);
        console.log("Vozilo: " + content);
        console.log(typeof content);
        upisiRezervaciju(contentKorisnik, content,
                   datumPocetkaRezervacije, datumKrajaRezervacije, cenaRezervacije, false);
        }
      };
  xhttp.open("GET", "http://localhost:8080/vozila?registracionaTablica=" + registracioniBroj, true);
  xhttp.send(); 
}

function upisiRezervaciju(iznajmljivac, rezervisanoVozilo,
  datumPocetkaRezervacije, datumKrajaRezervacije, cenaRezervacije, logickiObrisanaRezervacija) {

  ponistiPravljenjeRezervacije();

  var newRezervacija = {
            "iznajmljivac" : iznajmljivac,
            "rezervisanoVozilo" : rezervisanoVozilo,
            "datumPocetkaRezervacije" : datumPocetkaRezervacije,
            "datumKrajaRezervacije": datumKrajaRezervacije,
            "cenaRezervacije": cenaRezervacije,
            "logickiObrisanaRezervacija": false
            };

  var xhttp = new XMLHttpRequest();
  xhttp.onreadystatechange = function () {
      if (this.readyState == 4 && this.status == 200) {
        content = xhttp.responseText;
        console.log(content);
        window.alert("Vozilo je rezervisano.");
      }
    };
    
  xhttp.open("POST", "http://localhost:8080/rezervacije", true);
  xhttp.setRequestHeader("Content-type", "text/plain");
  xhttp.send(JSON.stringify(newRezervacija));
}

function ponistiVracanjeVozila() {
  $("input[type=number]").val("");
  document.getElementById("ukupnoZaUplatu").innerHTML = "Unesite broj kilometara i kliknite ažuriraj";

  var tabelaRezervacijeVracanjeVozilaTable = document.getElementById("tabelaRezervacijeVracanjeVozilaTable");
  var predjeniBrojKilometaraDiv = document.getElementById("predjeniBrojKilometaraDiv");
  var tabelaRezervacijeVracanjeVozilaDiv = document.getElementById("tabelaRezervacijeVracanjeVozila");
  var tabelaRezervacijeVracanjeVozilaTBody = document.getElementById("tabelaRezervacijeVracanjeVozilaTBody");
  var zakljuciRacunDiv = document.getElementById("zakljuciRacunDiv");

  isPretragaRezervacijaClicked = false;

  tabelaRezervacijeVracanjeVozilaDiv.setAttribute("style", "display:none");
  zakljuciRacunDiv.setAttribute("style", "display:none");
  tabelaRezervacijeVracanjeVozilaTable.setAttribute("style", "display:block");
  predjeniBrojKilometaraDiv.setAttribute("style", "display:none");

  while (tabelaRezervacijeVracanjeVozilaTBody.firstChild) {
    tabelaRezervacijeVracanjeVozilaTBody.removeChild(tabelaRezervacijeVracanjeVozilaTBody.firstChild);
  }
}

function vratiVozilo(podaciZaVracanjeVozila) {
  var tabelaRezervacijeVracanjeVozilaTable = document.getElementById("tabelaRezervacijeVracanjeVozilaTable");
  var predjeniBrojKilometaraDiv = document.getElementById("predjeniBrojKilometaraDiv");
  tabelaRezervacijeVracanjeVozilaTable.setAttribute("style", "display:none");
  predjeniBrojKilometaraDiv.setAttribute("style", "display:block");

  podaciZaVracanjeVozila = podaciZaVracanjeVozila.toString();
  podaciZaVracanjeVozila = podaciZaVracanjeVozila.replace(/\//g,'');
  podaciZaVracanjeVozila = podaciZaVracanjeVozila.split(",");
  
  var registracioneTablice = podaciZaVracanjeVozila[0];
  var datumPocetkaRezervacije = podaciZaVracanjeVozila[1];
  var datumKrajaRezervacije = podaciZaVracanjeVozila[2];
  var tipVozila = podaciZaVracanjeVozila[3];

  var datumPocetkaRezervacijeNiz = datumPocetkaRezervacije.split("-");
  var datumKrajaRezervacijeNiz = datumKrajaRezervacije.split("-");

  var datumPocetkaRezervacijeDate = new Date(datumPocetkaRezervacijeNiz[0], (parseInt(datumPocetkaRezervacijeNiz[1])-1).toString(), datumPocetkaRezervacijeNiz[2]);
  var datumKrajaRezervacijeDate = new Date(datumKrajaRezervacijeNiz[0], (parseInt(datumKrajaRezervacijeNiz[1])-1).toString(), datumKrajaRezervacijeNiz[2]);

  var trajanjeDanaUMilisekundama = 1000*60*60*24;  
  var datumPocetkaRezervacijeDate_ms = datumPocetkaRezervacijeDate.getTime();   
  var datumKrajaRezervacijeDate_ms = datumKrajaRezervacijeDate.getTime(); 
  var razlika_ms = datumKrajaRezervacijeDate_ms - datumPocetkaRezervacijeDate_ms;
  var brojDanaRezervacijeVracanjeVozila = Math.round(razlika_ms/trajanjeDanaUMilisekundama);

  var voziloZaVracanje = {
    "registracioneTablice": registracioneTablice,
    "brojDanaRezervacijeVracanjeVozila": brojDanaRezervacijeVracanjeVozila,
    "datumPocetkaRezervacije": datumPocetkaRezervacije,
    "datumKrajaRezervacije": datumKrajaRezervacije
  }
  localStorage.setItem("Vozilo koje se vraca", JSON.stringify(voziloZaVracanje));
  localStorage.setItem("Tip vozila koje se vraca", JSON.stringify(tipVozila));
}

function sracunajUkupnuCenu() {
  var predjeniBrojKilometara = document.getElementById("predjeniBrojKilometara").value;
  var zakljuciRacunDiv = document.getElementById("zakljuciRacunDiv");

  if (predjeniBrojKilometara <= 0) {
    window.alert("Pređeni broj kilometara mora biti veći od 0.");
    return;
  }


  var voziloZaVracanje = JSON.parse(localStorage.getItem("Vozilo koje se vraca"));
  var brojDanaRezervacijeVracanjeVozila = voziloZaVracanje["brojDanaRezervacijeVracanjeVozila"];
  var tablica = voziloZaVracanje["registracioneTablice"];
  var listaTablica = [];
  listaTablica.push(tablica);

  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
          if (this.readyState == 4 && this.status == 200) {
              content = JSON.parse(xhttp.responseText);
              //console.log(content);
              if (content.length == 0) {
                window.alert("Planirane kilometre nije moguće preći, zbog broja dana rezervacije.");
                zakljuciRacunDiv.setAttribute("style", "display:none");
                return;
              }
              zakljuciRacunDiv.setAttribute("style", "display:block");
              azurirajPodatke(content);
          }
      }

  xhttp.open("GET", "http://localhost:8080/racunanjeTroskova?planiraniKilometri=" + predjeniBrojKilometara +
  "&registracioneTablice=" + listaTablica + "&brojDanaRezervacije=" + brojDanaRezervacijeVracanjeVozila, true);
  xhttp.send();

}

function azurirajPodatke(podaciZaZakljucivanjeRacuna) {
  var ukupnaCena = podaciZaZakljucivanjeRacuna[3];
  
  localStorage.setItem("Podaci za zakljucivanje racuna", JSON.stringify(podaciZaZakljucivanjeRacuna));
  document.getElementById("ukupnoZaUplatu").innerHTML = ukupnaCena + " dinara";
}

function zakljuciRacun() {
  var podaciZaZakljucivanjeRacuna = JSON.parse(localStorage.getItem("Podaci za zakljucivanje racuna"));
  var ukupnaCena = podaciZaZakljucivanjeRacuna[3];
  var predjeniBrojKilometara = document.getElementById("predjeniBrojKilometara").value;
  var podaciZaZakljucivanjeRacuna = JSON.parse(localStorage.getItem("Podaci za zakljucivanje racuna"));
  var registracioniBroj = podaciZaZakljucivanjeRacuna[0];
  var tipVozila = JSON.parse(localStorage.getItem("Tip vozila koje se vraca"));
  var voziloZaVracanje = JSON.parse(localStorage.getItem("Vozilo koje se vraca"));

  var datumPocetkaRezervacije = voziloZaVracanje["datumPocetkaRezervacije"];
  var datumKrajaRezervacije = voziloZaVracanje["datumKrajaRezervacije"];

  if (podaciZaZakljucivanjeRacuna[2] == "servis je potreban") {
    window.alert("U ukupnu cenu uračunat je servis.");
    window.alert("Vaša ukuna cena iznosi: " + podaciZaZakljucivanjeRacuna[3] + " dinara.");
  } else {
    window.alert("Vaša ukuna cena iznosi: " + podaciZaZakljucivanjeRacuna[3] + " dinara.");
  }

  var today = new Date();
  var godina = today.getFullYear();
  var mesec = today.getMonth() + 1;
  var dan = today.getDate();
  if (mesec < 10) {
    mesec = "0" + mesec.toString();
  }

  if (dan < 10) {
    dan = "0" + dan.toString();
  }

  var datumServisiranja = godina.toString() + "-" + mesec.toString() + "-" + dan.toString();
  console.log(datumServisiranja);


  var xhttp = new XMLHttpRequest();
    xhttp.onreadystatechange = function() {
        if (this.readyState == 4 && this.status == 200) {
            content = JSON.parse(xhttp.responseText);
            if (content == "Izvrsene su izmene.") {
              window.alert("Izvršene su izmene.");
            }
            ponistiVracanjeVozila();
        }
    };

  xhttp.open("PUT", "http://localhost:8080/vracanjeVozila", true);

  xhttp.setRequestHeader("Content-type", "text/plain");
  xhttp.send(JSON.stringify("," + registracioniBroj + "," + predjeniBrojKilometara + "," + datumServisiranja + "," +
    tipVozila + "," + datumPocetkaRezervacije + "," + datumKrajaRezervacije + "," + ukupnaCena + ","));
}