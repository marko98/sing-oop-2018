package visak;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import funkcionalnosti.ProveraBroj;
import liste.RezervacijeLista;
import liste.ServisiranjaLista;
import liste.VozilaLista;
import model.Korisnici;
import model.Rezervacije;
import model.Servisiranja;
import model.Vozila;

public class VracanjeVozila {

	public static void vracanjeVozila(Korisnici korisnik) throws JsonParseException, JsonMappingException, IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		VozilaLista listaVozila = mapper.readValue(new File("vozila.json"), VozilaLista.class);
		ArrayList<Vozila> rezervisanaVozila = new ArrayList<Vozila>();
		
		RezervacijeLista listaRezervacije = mapper.readValue(new File("rezervacije.json"), RezervacijeLista.class);
		ServisiranjaLista listaServisiranja = mapper.readValue(new File("servisiranja.json"), ServisiranjaLista.class);
		boolean trazenjeRezervacijaUToku = true;
		boolean trazenjeRezervacijaUToku2 = true;
		
		for(Vozila vozilo : listaVozila.getVozila()) {
			boolean dozvola = true;
			for(Rezervacije rezervacija : vozilo.getRezervacije()) {
				if(korisnik.getKorisnickoIme().equals(rezervacija.getKorisnickoImeIznajmljivaca()) &&
						rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
					for(Vozila rezervisanoVozilo : rezervisanaVozila) {
						if(vozilo.getRegistracioniBroj().equals(rezervisanoVozilo.getRegistracioniBroj())) {
							dozvola = false;
						}
					}
					if(dozvola) {
						rezervisanaVozila.add(vozilo);
					}
				}
			}
		}
		

		
		if(rezervisanaVozila.isEmpty()) {
			System.out.println("Nemate aktivnih rezervacija");
		} else {
			HashMap<Integer, Vozila> map = new HashMap<Integer, Vozila>();
			
			System.out.println("Aktivne rezervacije su: ");
			int brojacRezervisanihVozila = 0;
			
			for(Vozila vozilo : rezervisanaVozila) {
				boolean rezervacijePostoje = false;
				for(Rezervacije rezervacija : vozilo.getRezervacije()) {
					if(rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
						if(!rezervacijePostoje) {
							brojacRezervisanihVozila++;
							if(vozilo.getClass().getName().equals("model.Bicikli")) {
								System.out.println(brojacRezervisanihVozila + "." + "Bicikl, registarskih tablica: " +
													vozilo.getRegistracioniBroj() + ".");
							} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
								System.out.println(brojacRezervisanihVozila + "." + "Putničko vozilo, registarskih tablica: " +
										vozilo.getRegistracioniBroj() + ".");
							} else {
								System.out.println(brojacRezervisanihVozila + "." + "Teretno vozilo, registarskih tablica: " +
										vozilo.getRegistracioniBroj() + ".");
							}
							map.put(brojacRezervisanihVozila, vozilo);
							rezervacijePostoje = true;
						}
					}
				}		
			}
			int odustaniOpcija = brojacRezervisanihVozila + 1;
			System.out.println(odustaniOpcija + ". Odustani");
			
			while(trazenjeRezervacijaUToku) {
				int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred vozila koje želite da vratite: ");
				if(!(opcija <= 0 || opcija > odustaniOpcija)) {
					if(opcija == odustaniOpcija) {
						trazenjeRezervacijaUToku = false;
					} else {
						Vozila vozilo = map.get(opcija);
						
						Integer brojacRezervisanihVozila2 = 0;
						HashMap<Integer, Rezervacije> map2 = new HashMap<Integer, Rezervacije>();
						
						for(Rezervacije rezervacija : vozilo.getRezervacije()) {
							if(korisnik.getKorisnickoIme().equals(rezervacija.getKorisnickoImeIznajmljivaca()) &&
									rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
								brojacRezervisanihVozila2++;
								if(vozilo.getClass().getName().equals("model.Bicikli")) {
									System.out.println(brojacRezervisanihVozila2 + "." + "Bicikl, registarskih tablica: " +
														vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
														rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
														rezervacija.getDatumKrajaRezervacije() + ".");
								} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
									System.out.println(brojacRezervisanihVozila2 + "." + "Putničko vozilo, registarskih tablica: " +
											vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
											rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
											rezervacija.getDatumKrajaRezervacije() + ".");
								} else {
									System.out.println(brojacRezervisanihVozila2 + "." + "Teretno vozilo, registarskih tablica: " +
											vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
											rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
											rezervacija.getDatumKrajaRezervacije() + ".");
								}
								map2.put(brojacRezervisanihVozila2, rezervacija);
							}
						}
						
						int odustaniOpcija2 = brojacRezervisanihVozila2 + 1;
						System.out.println(odustaniOpcija2 + ". Odustani");
						
						while(trazenjeRezervacijaUToku2) {
							int opcija2 = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije koju želite da zaključite: ");
							if(!(opcija2 <= 0 || opcija2 > odustaniOpcija2)) {
								if(opcija2 == odustaniOpcija2) {
									trazenjeRezervacijaUToku = false;
									trazenjeRezervacijaUToku2 = false;
								} else {
									Rezervacije rezervacija = map2.get(opcija2);
									
									double brPredjenihKm = ProveraBroj.proveraBrojDouble("Unesite broj pređenih km: ");
									
									String StringDatumPocetkaRezervacije = rezervacija.getDatumPocetkaRezervacije();
									String[] parsiranDatumPocetkaRezervacije = StringDatumPocetkaRezervacije.split("-");
									LocalDate DatumPocetkaRezervacije = LocalDate.of(Integer.parseInt(parsiranDatumPocetkaRezervacije[0]), 
																	Integer.parseInt(parsiranDatumPocetkaRezervacije[1]),
																	Integer.parseInt(parsiranDatumPocetkaRezervacije[2]));
									
									String StringDatumKrajaRezervacije = rezervacija.getDatumKrajaRezervacije();
									String[] parsiranDatumKrajaRezervacije = StringDatumKrajaRezervacije.split("-");
									LocalDate DatumKrajaRezervacije = LocalDate.of(Integer.parseInt(parsiranDatumKrajaRezervacije[0]), 
																	Integer.parseInt(parsiranDatumKrajaRezervacije[1]),
																	Integer.parseInt(parsiranDatumKrajaRezervacije[2]));
								
									long brojDanaRezervacije = Duration.between(DatumPocetkaRezervacije.atStartOfDay(), DatumKrajaRezervacije.atStartOfDay()).toDays();
									
									if(vozilo.getClass().getName().equals("model.Bicikli")) {
										if(brPredjenihKm > 60*brojDanaRezervacije) {
											System.out.println("Na biciklu nije bilo moguće preći " + brPredjenihKm + " km.\n" + 
																"Obratite nam se povodom ovog problema na broj 021/452-698.");
											trazenjeRezervacijaUToku = false;
											break;
										}
									}
									
									double cena = vozilo.cenaPrelaskaRazdaljine(brPredjenihKm);
									
									if(vozilo.sledeciServis(brPredjenihKm)) {
										System.out.println("Servis je uračunat u cenu.");
										cena = cena + vozilo.getCenaServisiranja();
									}
									
									System.out.println("Ukupno za uplatu: " +  Math.round(cena) + " dinara." + "\n" + 
														"Zakljucite račun: " + "\n" +
														"1. Da" + "\n" + "2. Ne - poništi račun");
									
									while(true) {
										int zakljuciRacunOpcija = ProveraBroj.proveraBrojInt("Odaberite opciju: ");
										
										if(!(zakljuciRacunOpcija < 0 || zakljuciRacunOpcija > 2)) {
											if(zakljuciRacunOpcija == 1) {
												for(Rezervacije rezervacija2 : vozilo.getRezervacije()) {
													if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija2.getKorisnickoImeIznajmljivaca()) &&
															rezervacija.getDatumPocetkaRezervacije().equals(rezervacija2.getDatumPocetkaRezervacije()) &&
															rezervacija.getDatumKrajaRezervacije().equals(rezervacija2.getDatumKrajaRezervacije())) {
														rezervacija2.setLogickiObrisanaRezervacija(true);
													}
												}
												
												for(Rezervacije rezervacija3 : listaRezervacije.getRezervacije()) {
													if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija3.getKorisnickoImeIznajmljivaca()) &&
															rezervacija.getDatumPocetkaRezervacije().equals(rezervacija3.getDatumPocetkaRezervacije()) &&
															rezervacija.getDatumKrajaRezervacije().equals(rezervacija3.getDatumKrajaRezervacije())) {
														rezervacija3.setLogickiObrisanaRezervacija(true);
													}								
												}
												vozilo.setPredjeniKm(vozilo.getPredjeniKm() + brPredjenihKm);
												
												ArrayList<Servisiranja> listaServisa = new ArrayList<Servisiranja>();
												for (Servisiranja servis : vozilo.getServisiranje()) {
													listaServisa.add(servis);
												}
												LocalDate danasnjiDatum = LocalDate.now();
												Servisiranja noviServis = new Servisiranja(vozilo.getRegistracioniBroj(), danasnjiDatum.toString(), vozilo.getPredjeniKm());
												listaServisa.add(noviServis);
												vozilo.setServisiranje(listaServisa);
												
												listaServisiranja.getServisiranja().add(noviServis);
												
												ObjectMapper obrisiRezervaciju = new ObjectMapper();
												obrisiRezervaciju.enable(SerializationFeature.INDENT_OUTPUT);
												
												obrisiRezervaciju.writeValue(new File("vozila.json"), listaVozila);
												obrisiRezervaciju.writeValue(new File("rezervacije.json"), listaRezervacije);
												obrisiRezervaciju.writeValue(new File("servisiranja.json"), listaServisiranja);
												
												System.out.println("Račun je zaključen. Vozilo je vraćeno.");
												trazenjeRezervacijaUToku = false;
												trazenjeRezervacijaUToku2 = false;
												break;
											} else if (zakljuciRacunOpcija == 2) {
												System.out.println("Izdavanje računa je poništeno.");
												trazenjeRezervacijaUToku = false;
												trazenjeRezervacijaUToku2 = false;
												break;
											}
										}
									}
								}
								
							}
						}
					}
					
				}
			}
		}
		


	}
}
