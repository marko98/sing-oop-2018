package visak;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import funkcionalnosti.ProveraBroj;
import liste.VozilaLista;
import model.Rezervacije;
import model.Vozila;

public class BrisanjeVozila {

	public static void brisanjeVozila() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		om.enable(SerializationFeature.INDENT_OUTPUT);
		VozilaLista listaVozila = om.readValue(new File("vozila.json"), VozilaLista.class);
		
		System.out.println("Aktivna slobodna vozila: ");
		HashMap<Integer, Vozila> map = new HashMap<Integer, Vozila>();
		boolean postojeZauzetaVozila = false;
		boolean postojeAktivnaVozila = false;
		Integer brojacAktivnihVozila = 0;
		for(Vozila vozilo : listaVozila.getVozila()) {
			boolean prikaziVozilo = true;
			for(Rezervacije rezervacija : vozilo.getRezervacije()) {
				if(rezervacija.getLogickiObrisanaRezervacija().equals(false) || vozilo.getLogickiObrisanoVozilo().equals(true)) {
					postojeZauzetaVozila = true;
					prikaziVozilo = false;
				} 
			}
			if(prikaziVozilo) {
				if(vozilo.getLogickiObrisanoVozilo().equals(false)) {
					brojacAktivnihVozila++;
					if(vozilo.getClass().getName().equals("model.Bicikli")) {
						System.out.println(brojacAktivnihVozila + ". Bicikli, registarskih tablica: " + vozilo.getRegistracioniBroj());
					} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
						System.out.println(brojacAktivnihVozila + ". Putničko vozilo, registarskih tablica: " + vozilo.getRegistracioniBroj());
					} else {
						System.out.println(brojacAktivnihVozila + ". Teretno vozilo, registarskih tablica: " + vozilo.getRegistracioniBroj());
					}
					map.put(brojacAktivnihVozila, vozilo);
					postojeAktivnaVozila = true;
				}
			}
		}
		int odustaniOpcija = brojacAktivnihVozila + 1;
		
		if(!postojeAktivnaVozila) {
			System.out.println("Sva vozila su obrisana ili rezervisana, te njihovo 'brisanje' nije dozvoljeno");
		} else {
			System.out.println(odustaniOpcija + ". Odustani");
			if(postojeZauzetaVozila) {
				System.out.println("Neka od vozila nisu prikazana jer su rezervisana");
			}
		}
		
		
		while(postojeAktivnaVozila) {
			int opcijaVozilo = ProveraBroj.proveraBrojInt("Unesite broj ispred vozila koje želite da 'obrišete' ili poslednju opciju za odustajanje: ");
			if(!(opcijaVozilo <= 0 || opcijaVozilo > odustaniOpcija)) {
				if(opcijaVozilo == odustaniOpcija) {
					break;
				} else {
					Vozila vozilo = map.get(opcijaVozilo);
					vozilo.setLogickiObrisanoVozilo(true);
					om.writeValue(new File("vozila.json"), listaVozila);
					System.out.println("Vozilo je 'obrisano'.");
					break;
				}	
			}
		}
	}
}
