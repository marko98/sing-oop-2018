package visak;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import funkcionalnosti.ProveraBroj;
import liste.VozilaLista;
import model.Vozila;

public class AktiviranjeVozila {

	public static void aktiviranjeVozila() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		om.enable(SerializationFeature.INDENT_OUTPUT);
		VozilaLista listaVozila = om.readValue(new File("vozila.json"), VozilaLista.class);
		
		System.out.println("Obrisana vozila: ");
		HashMap<Integer, Vozila> map = new HashMap<Integer, Vozila>();
		boolean postojeObrisanaVozila = false;
		Integer brojacObrisanihVozila = 0;
		for(Vozila vozilo : listaVozila.getVozila()) {
			if(vozilo.getLogickiObrisanoVozilo().equals(true)) {
				brojacObrisanihVozila++;
				if(vozilo.getClass().getName().equals("model.Bicikli")) {
					System.out.println(brojacObrisanihVozila + ". Bicikli, registarskih tablica: " + vozilo.getRegistracioniBroj());
				} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
					System.out.println(brojacObrisanihVozila + ". Putničko vozilo, registarskih tablica: " + vozilo.getRegistracioniBroj());
				} else {
					System.out.println(brojacObrisanihVozila + ". Teretno vozilo, registarskih tablica: " + vozilo.getRegistracioniBroj());
				}
				map.put(brojacObrisanihVozila, vozilo);
				postojeObrisanaVozila = true;
			}
		}
		int odustaniOpcija = brojacObrisanihVozila + 1;
		
		if(!postojeObrisanaVozila) {
			System.out.println("Nema obrisanih vozila");
		} else {
			System.out.println(odustaniOpcija + ". Odustani");
		}
		
		while(postojeObrisanaVozila) {
			int opcijaVozilo = ProveraBroj.proveraBrojInt("Unesite broj ispred vozila koje želite da 'aktivirate' ili poslednju opciju za odustajanje: ");
			if(!(opcijaVozilo <= 0 || opcijaVozilo > odustaniOpcija)) {
				if(opcijaVozilo == odustaniOpcija) {
					break;
				} else {
					Vozila vozilo = map.get(opcijaVozilo);
					vozilo.setLogickiObrisanoVozilo(false);
					om.writeValue(new File("vozila.json"), listaVozila);
					System.out.println("Vozilo je 'aktivirano'.");
					break;
				}
			}
		}
	}
}
