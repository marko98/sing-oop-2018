package visak;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import funkcionalnosti.ProveraBroj;
import liste.GorivaLista;
import liste.VozilaLista;
import model.Bicikli;
import model.Goriva;
import model.PutnickoVozilo;
import model.Rezervacije;
import model.Servisiranja;
import model.TeretnoVozilo;
import model.Vozila;

public class DodavanjeVozila {
	
	public static PutnickoVozilo dovrsiPutnickoVozilo(PutnickoVozilo nedovrsenoVozilo) throws JsonParseException, JsonMappingException, IOException {	
		boolean trazenjeGoriva = true;
		while(trazenjeGoriva) {
			System.out.println("Vrste goriva: ");
			ObjectMapper om = new ObjectMapper();
			GorivaLista listaGoriva = om.readValue(new File("goriva.json"), GorivaLista.class);
			
			Integer brojacPostojecihGoriva = 0;
			HashMap<Integer, Goriva> map = new HashMap<Integer, Goriva>();
			
			for(Goriva moguceGorivo : listaGoriva.getGoriva()) {
				brojacPostojecihGoriva++;
				System.out.println(brojacPostojecihGoriva + ". " + "Naziv goriva: " + moguceGorivo.getNazivGoriva() +
						", cena: "+ Math.round(moguceGorivo.getCenaGoriva()));
				map.put(brojacPostojecihGoriva, moguceGorivo);
			}
			while(true) {
				int odabirGoriva = ProveraBroj.proveraBrojInt("Odaberite tip goriva za novo vozilo: ");
				
				if(!(odabirGoriva <= 0 || odabirGoriva > brojacPostojecihGoriva)) {
					nedovrsenoVozilo.setGorivo(map.get(odabirGoriva));
				
					trazenjeGoriva = false;
					break;
				}
			}
		}
		double potrosnjaGoriva = ProveraBroj.proveraBrojDouble("Potrošnja goriva: ");
		int brojVrata = ProveraBroj.proveraBrojInt("Unesite broj vrata: ");

		nedovrsenoVozilo.setPotrosnjaGoriva(potrosnjaGoriva);
		nedovrsenoVozilo.setBrojVrata(brojVrata);
		

		return nedovrsenoVozilo;		
	}
	
	public static TeretnoVozilo dovrsiTeretnoVozilo(TeretnoVozilo nedovrsenoVozilo) throws JsonParseException, JsonMappingException, IOException {
		boolean trazenjeGoriva = true;
		while(trazenjeGoriva) {
			System.out.println("Vrste goriva: ");
			ObjectMapper om = new ObjectMapper();
			GorivaLista listaGoriva = om.readValue(new File("goriva.json"), GorivaLista.class);
			
			Integer brojacPostojecihGoriva = 0;
			HashMap<Integer, Goriva> map = new HashMap<Integer, Goriva>();
			
			for(Goriva moguceGorivo : listaGoriva.getGoriva()) {
				brojacPostojecihGoriva++;
				System.out.println(brojacPostojecihGoriva + ". " + "Naziv goriva: " + moguceGorivo.getNazivGoriva() +
						", cena: "+ Math.round(moguceGorivo.getCenaGoriva()));
				map.put(brojacPostojecihGoriva, moguceGorivo);
			}
			while(true) {
				int odabirGoriva = ProveraBroj.proveraBrojInt("Odaberite tip goriva za novo vozilo: ");
				
				if(!(odabirGoriva <= 0 || odabirGoriva > brojacPostojecihGoriva)) {
					nedovrsenoVozilo.setGorivo(map.get(odabirGoriva));
				
					trazenjeGoriva = false;
					break;
				}
			}
		}
		double potrosnjaGoriva = ProveraBroj.proveraBrojDouble("Potrošnja goriva: ");
		int brojVrata = ProveraBroj.proveraBrojInt("Unesite broj vrata: ");
		double maksimalnaMasa = ProveraBroj.proveraBrojDouble("Maksimalna masa: ");
		double maksimalnaVisina = ProveraBroj.proveraBrojDouble("Maksimalna visina: ");

		nedovrsenoVozilo.setPotrosnjaGoriva(potrosnjaGoriva);
		nedovrsenoVozilo.setBrojVrata(brojVrata);		
		nedovrsenoVozilo.setMaksimalnaMasa(maksimalnaMasa);
		nedovrsenoVozilo.setMaksimalnaVisina(maksimalnaVisina);

		return nedovrsenoVozilo;		
	}

	public static void dodavanjeVozila() throws JsonParseException, JsonMappingException, IOException {
		System.out.println("Tipovi vozila: \n" + "1 - Bicikli \n" + "2 - Putničko vozilo \n" + "3 - Teretno vozilo \n" + "4 - Odustani \n");
		ObjectMapper saveNovoVozilo = new ObjectMapper();
		saveNovoVozilo.enable(SerializationFeature.INDENT_OUTPUT);
		
		VozilaLista listaVozila = saveNovoVozilo.readValue(new File("vozila.json"), VozilaLista.class);
		
		while(true) {
			int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred ponuđene opcije: ");
			if(!(opcija <= 0 || opcija > 4)) {
				
				if (opcija != 4) {
					Scanner tastatura = new Scanner(System.in);
					
					String registracioniBroj;
					while(true) {
						boolean postojiTablica = false;
						System.out.println("Unesite registracionu tablicu novog vozila: ");
						registracioniBroj = tastatura.nextLine();
						for(Vozila vozilo : listaVozila.getVozila()) {
							if(registracioniBroj.equals(vozilo.getRegistracioniBroj())) {
								postojiTablica = true;
							}
						}
						if(!postojiTablica) {
							break;
						} else {
							System.out.println("Uneta registraciona tablica je zauzeta.");
						}
					}
					
					ArrayList<Servisiranja> servisiranje = new ArrayList<Servisiranja>();
					ArrayList<Rezervacije> rezervacije = new ArrayList<Rezervacije>();
					double brKmPreServisiranja = ProveraBroj.proveraBrojDouble("Unesite broj km od jednog do drugog servisa: ");
					double cenaServisiranja = ProveraBroj.proveraBrojDouble("Cena servisiranja: ");
					double cenaIznajmljivanjaPoDanu = ProveraBroj.proveraBrojDouble("Cena iznajmljivanja po danu: ");
					int brojSedista = ProveraBroj.proveraBrojInt("Unesite broj sedišta: ");
					
					if(opcija == 1) {
						Bicikli noviBicikl = new Bicikli(registracioniBroj, null, servisiranje, rezervacije, 0, 0, brKmPreServisiranja, cenaServisiranja, cenaIznajmljivanjaPoDanu, brojSedista, 0, false);
			
						listaVozila.getVozila().add(noviBicikl);
						saveNovoVozilo.writeValue(new File("vozila.json"), listaVozila);
						System.out.println("Dodat je novi bicikl.");
						break;					
					} else if(opcija == 2) {
						PutnickoVozilo nedovrsenoVozilo = new PutnickoVozilo(registracioniBroj, null, servisiranje, rezervacije, 0, 0, brKmPreServisiranja, cenaServisiranja, cenaIznajmljivanjaPoDanu, brojSedista, 0, false);
						
						listaVozila.getVozila().add(dovrsiPutnickoVozilo(nedovrsenoVozilo));
						saveNovoVozilo.writeValue(new File("vozila.json"), listaVozila);
						System.out.println("Dodato je novo putničko vozilo.");
						break;
					} else if(opcija == 3) {
						TeretnoVozilo nedovrsenoVozilo = new TeretnoVozilo(registracioniBroj, null, servisiranje, rezervacije, 0, 0, brKmPreServisiranja, cenaServisiranja, cenaIznajmljivanjaPoDanu, brojSedista, 0, false, 0, 0);
						
						listaVozila.getVozila().add(dovrsiTeretnoVozilo(nedovrsenoVozilo));
						saveNovoVozilo.writeValue(new File("vozila.json"), listaVozila);
						System.out.println("Dodato je novo teretno vozilo.");
						break;
					} 
					
				} else {
					break;
				}
			}	
		}
	}
}
