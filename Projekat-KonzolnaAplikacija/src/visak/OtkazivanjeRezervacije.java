package visak;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import funkcionalnosti.ProveraBroj;
import liste.RezervacijeLista;
import liste.VozilaLista;
import model.Korisnici;
import model.Rezervacije;
import model.Vozila;

public class OtkazivanjeRezervacije {

	public static void otkazivanjeRezervacije(Korisnici korisnik) throws JsonParseException, JsonMappingException, IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		VozilaLista listaVozila = mapper.readValue(new File("vozila.json"), VozilaLista.class);
		ArrayList<Vozila> rezervisanaVozila = new ArrayList<Vozila>();
		
		RezervacijeLista listaRezervacije = mapper.readValue(new File("rezervacije.json"), RezervacijeLista.class);
		boolean trazenjeRezervacijaUToku = true;
		
		if(korisnik.getClass().getName().equals("model.IznajmljivaciVozila")) {
		
			for(Vozila vozilo : listaVozila.getVozila()) {
				boolean dozvola = true;
				for(Rezervacije rezervacija : vozilo.getRezervacije()) {
					if(korisnik.getKorisnickoIme().equals(rezervacija.getKorisnickoImeIznajmljivaca()) &&
							rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
						for(Vozila rezervisanoVozilo : rezervisanaVozila) {
							if(vozilo.getRegistracioniBroj().equals(rezervisanoVozilo.getRegistracioniBroj())) {
								dozvola = false;
							}
						}
						if(dozvola) {
							rezervisanaVozila.add(vozilo);
						}
					}
				}
			}
			
		} else {
			
			for(Vozila vozilo : listaVozila.getVozila()) {
				boolean dozvola = true;
				for(Rezervacije rezervacija : vozilo.getRezervacije()) {
					if(rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
						for(Vozila rezervisanoVozilo : rezervisanaVozila) {
							if(vozilo.getRegistracioniBroj().equals(rezervisanoVozilo.getRegistracioniBroj())) {
								dozvola = false;
							}
						}
						if(dozvola) {
							rezervisanaVozila.add(vozilo);
						}
					}
				}
			}
			
		}
		
		if(rezervisanaVozila.isEmpty()) {
			System.out.println("Nema aktivnih rezervacija. ");
		} else {
			HashMap<Integer, Vozila> map = new HashMap<Integer, Vozila>();
			
			System.out.println("Aktivne rezervacije su: ");
			int brojacRezervisanihVozila = 0;
			
			for(Vozila vozilo : rezervisanaVozila) {
				boolean rezervacijePostoje = false;
				for(Rezervacije rezervacija : vozilo.getRezervacije()) {	
					if(rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
						if(!rezervacijePostoje) {
							brojacRezervisanihVozila++;
							if(vozilo.getClass().getName().equals("model.Bicikli")) {
								System.out.println(brojacRezervisanihVozila + ". Bicikl, registarskih tablica: " +
													vozilo.getRegistracioniBroj());
							} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
								System.out.println(brojacRezervisanihVozila + ". Putničko vozilo, registarskih tablica: " +
										vozilo.getRegistracioniBroj());
							} else {
								System.out.println(brojacRezervisanihVozila + ". Teretno vozilo, registarskih tablica: " +
										vozilo.getRegistracioniBroj());
							}
							map.put(brojacRezervisanihVozila, vozilo);
							rezervacijePostoje = true;
						}
					}
				}
			}
			
			int odustaniOpcija = brojacRezervisanihVozila + 1;
			System.out.println(odustaniOpcija + ". Odustani");
			
			if(korisnik.getClass().getName().equals("model.IznajmljivaciVozila")) {
			
				while(trazenjeRezervacijaUToku) {
					int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije: ");
					if(!(opcija <= 0 || opcija > odustaniOpcija)) {
						
						if(opcija == odustaniOpcija) {
							trazenjeRezervacijaUToku = false;
						} else {
							Vozila vozilo = map.get(opcija);
							
							Integer brojacRezervisanihVozila2 = 0;
							HashMap<Integer, Rezervacije> map2 = new HashMap<Integer, Rezervacije>();
							
							for(Rezervacije rezervacija : vozilo.getRezervacije()) {
								if(korisnik.getKorisnickoIme().equals(rezervacija.getKorisnickoImeIznajmljivaca()) &&
										rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
									brojacRezervisanihVozila2++;
									if(vozilo.getClass().getName().equals("model.Bicikli")) {
										System.out.println(brojacRezervisanihVozila2 + ". Bicikl, registarskih tablica: " +
															vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
															rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
															rezervacija.getDatumKrajaRezervacije());
									} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
										System.out.println(brojacRezervisanihVozila2 + ". Putničko vozilo, registarskih tablica: " +
												vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
												rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
												rezervacija.getDatumKrajaRezervacije());
									} else {
										System.out.println(brojacRezervisanihVozila2 + ". Teretno vozilo, registarskih tablica: " +
												vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
												rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
												rezervacija.getDatumKrajaRezervacije());
									}
									map2.put(brojacRezervisanihVozila2, rezervacija);
								}
							}
							
							int odustaniOpcija2 = brojacRezervisanihVozila2 + 1;
							System.out.println(odustaniOpcija2 + ". Odustani");
							
							while(true) {
								int opcija2 = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije koju želite da otkažete: ");
								if(!(opcija2 <= 0 || opcija2 > odustaniOpcija2)) {
									if(opcija2 == odustaniOpcija2) {
										trazenjeRezervacijaUToku = false;
										break;
									} else {
										Rezervacije rezervacija = map2.get(opcija2);
									
										for(Rezervacije rezervacija2 : vozilo.getRezervacije()) {
											if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija2.getKorisnickoImeIznajmljivaca()) &&
													rezervacija.getDatumPocetkaRezervacije().equals(rezervacija2.getDatumPocetkaRezervacije()) &&
													rezervacija.getDatumKrajaRezervacije().equals(rezervacija2.getDatumKrajaRezervacije())) {
												rezervacija2.setLogickiObrisanaRezervacija(true);
											}
										}
										
										for(Rezervacije rezervacija3 : listaRezervacije.getRezervacije()) {
											if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija3.getKorisnickoImeIznajmljivaca()) &&
													rezervacija.getDatumPocetkaRezervacije().equals(rezervacija3.getDatumPocetkaRezervacije()) &&
													rezervacija.getDatumKrajaRezervacije().equals(rezervacija3.getDatumKrajaRezervacije())) {
												rezervacija3.setLogickiObrisanaRezervacija(true);
											}								
										}
										
										ObjectMapper obrisiRezervaciju = new ObjectMapper();
										obrisiRezervaciju.enable(SerializationFeature.INDENT_OUTPUT);
										
										obrisiRezervaciju.writeValue(new File("vozila.json"), listaVozila);
										obrisiRezervaciju.writeValue(new File("rezervacije.json"), listaRezervacije);
										
										System.out.println("Rezervacija je obrisana.");
										trazenjeRezervacijaUToku = false;
										break;
									}
								}
							}
						}
						
					}
				}	
			} else {
				while(trazenjeRezervacijaUToku) {
					int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije: ");
					if(!(opcija <= 0 || opcija > odustaniOpcija)) {
						
						if(opcija == odustaniOpcija) {
							trazenjeRezervacijaUToku = false;
						} else {
							Vozila vozilo = map.get(opcija);
							
							Integer brojacRezervisanihVozila2 = 0;
							HashMap<Integer, Rezervacije> map2 = new HashMap<Integer, Rezervacije>();
							
							for(Rezervacije rezervacija : vozilo.getRezervacije()) {
								if(rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
									brojacRezervisanihVozila2++;
									if(vozilo.getClass().getName().equals("model.Bicikli")) {
										System.out.println(brojacRezervisanihVozila2 + ". Bicikl, registarskih tablica: " +
															vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
															rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
															rezervacija.getDatumKrajaRezervacije() + ". Korisničko ime iznajmljivača: " + rezervacija.getKorisnickoImeIznajmljivaca());
									} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
										System.out.println(brojacRezervisanihVozila2 + ". Putničko vozilo, registarskih tablica: " +
												vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
												rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
												rezervacija.getDatumKrajaRezervacije() + ". Korisničko ime iznajmljivača: " + rezervacija.getKorisnickoImeIznajmljivaca());
									} else {
										System.out.println(brojacRezervisanihVozila2 + ". Teretno vozilo, registarskih tablica: " +
												vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
												rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
												rezervacija.getDatumKrajaRezervacije() + ". Korisničko ime iznajmljivača: " + rezervacija.getKorisnickoImeIznajmljivaca());
									}
									map2.put(brojacRezervisanihVozila2, rezervacija);
								}
							}
							int odustaniOpcija2 = brojacRezervisanihVozila2 + 1;
							System.out.println(odustaniOpcija2 + ". Odustani");
							
							while(true) {
								int opcija2 = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije koju želite da otkažete: ");
								if(!(opcija2 <= 0 || opcija2 > odustaniOpcija2)) {
									if(opcija2 == odustaniOpcija2) {
										trazenjeRezervacijaUToku = false;
										break;
									} else {
										Rezervacije rezervacija = map2.get(opcija2);
										
										for(Rezervacije rezervacija2 : vozilo.getRezervacije()) {
											if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija2.getKorisnickoImeIznajmljivaca()) &&
													rezervacija.getDatumPocetkaRezervacije().equals(rezervacija2.getDatumPocetkaRezervacije()) &&
													rezervacija.getDatumKrajaRezervacije().equals(rezervacija2.getDatumKrajaRezervacije())) {
												rezervacija2.setLogickiObrisanaRezervacija(true);
											}
										}
										
										for(Rezervacije rezervacija3 : listaRezervacije.getRezervacije()) {
											if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija3.getKorisnickoImeIznajmljivaca()) &&
													rezervacija.getDatumPocetkaRezervacije().equals(rezervacija3.getDatumPocetkaRezervacije()) &&
													rezervacija.getDatumKrajaRezervacije().equals(rezervacija3.getDatumKrajaRezervacije())) {
												rezervacija3.setLogickiObrisanaRezervacija(true);
											}								
										}
										
										ObjectMapper obrisiRezervaciju = new ObjectMapper();
										obrisiRezervaciju.enable(SerializationFeature.INDENT_OUTPUT);
										
										obrisiRezervaciju.writeValue(new File("vozila.json"), listaVozila);
										obrisiRezervaciju.writeValue(new File("rezervacije.json"), listaRezervacije);
										
										System.out.println("Rezervacija je obrisana.");
										trazenjeRezervacijaUToku = false;
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
}
