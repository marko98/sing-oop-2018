package liste;

import java.util.ArrayList;

import model.Rezervacije;

public class RezervacijeLista {
	private ArrayList<Rezervacije> rezervacije;
	
	public RezervacijeLista() {
		super();
	}

	public RezervacijeLista(ArrayList<Rezervacije> rezervacije) {
		super();
		this.rezervacije = rezervacije;
	}
	
	public ArrayList<Rezervacije> getRezervacije() {
		return rezervacije;
	}

	public void setRezervacije(ArrayList<Rezervacije> rezervacije) {
		this.rezervacije = rezervacije;
	}

	public void add(Rezervacije r) {
		this.rezervacije.add(r);
	}

	@Override
	public String toString() {
		return rezervacije.toString();
	}



}
