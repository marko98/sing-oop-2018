package liste;

import java.util.ArrayList;

import model.Korisnici;

public class KorisniciLista {
	private ArrayList<Korisnici> korisnici;
	
	public KorisniciLista() {
		super();
	}
	
	public KorisniciLista(ArrayList<Korisnici> korisnici) {
		super();
		this.korisnici = korisnici;
	}
	
	public ArrayList<Korisnici> getKorisnici() {
		return korisnici;
	}

	public void setKorisnici(ArrayList<Korisnici> korisnici) {
		this.korisnici = korisnici;
	}

	public void add(Korisnici k) {
		this.korisnici.add(k);
	}

	@Override
	public String toString() {
		return  korisnici.toString();
	}



}
