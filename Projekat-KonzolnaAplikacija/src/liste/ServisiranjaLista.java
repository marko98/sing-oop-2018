package liste;

import java.util.ArrayList;

import model.Servisiranja;

public class ServisiranjaLista {
	private ArrayList<Servisiranja> servisiranja;
	
	public ServisiranjaLista() {
		super();
	}
	
	public ServisiranjaLista(ArrayList<Servisiranja> servisiranja) {
		super();
		this.servisiranja = servisiranja;
	}

	public ArrayList<Servisiranja> getServisiranja() {
		return servisiranja;
	}

	public void setServisiranja(ArrayList<Servisiranja> servisiranja) {
		this.servisiranja = servisiranja;
	}

	public void add(Servisiranja s) {
		this.servisiranja.add(s);
	}

	@Override
	public String toString() {
		return servisiranja.toString();
	}



}
