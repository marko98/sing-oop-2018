package funkcionalnosti;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import model.Korisnici;


public class Meni {

	public static boolean meni(Korisnici korisnik) throws JsonParseException, JsonMappingException, IOException {
		boolean prikaziOpcije = true;
		boolean ponovnoLogovanje = false;
		if(korisnik.getClass().getName().equals("model.Sluzbenici")) {
			System.out.println("\n" + "Vaše opcije su: " + "\n"
								+ "1 - Otkazivanje rezervacije" + "\n"
								+ "2 - Dodavanje vozila" + "\n"
								+ "3 - 'Brisanje' vozila" + "\n"
								+ "4 - 'Aktiviranje' vozila" + "\n"
								+ "5 - Logout" + "\n");
			
			while(prikaziOpcije) {
				int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred jedne od ponuđenih opcija: ");
				
				if(!(opcija <= 0 || opcija > 5)) {
					prikaziOpcije = false;
				}
				
				if(opcija == 1) {
					System.out.println("Otkazivanje rezervacije: \n");
					OperacijeNadRezervacijama.otkazivanjeRezervacije(korisnik);
					break;
				} else if(opcija == 2) {
					System.out.println("Dodavanje vozila: \n");
					OperacijeNadVozilima.dodavanjeVozila();
					break;
				} else if(opcija == 3) {
					System.out.println("Brisanje vozila: \n");
					OperacijeNadVozilima.brisanjeVozila();
					break;
				} else if(opcija == 4) {
					System.out.println("Aktiviranje vozila: \n");
					OperacijeNadVozilima.aktiviranjeVozila();
					break;
				} else if(opcija == 5) {
					ponovnoLogovanje = true;
					break;
				}
			}				

		} else {
			System.out.println("\n" + "Vaše opcije su: " + "\n"
								+ "1 - Zakazivanje rezervacije" + "\n"
								+ "2 - Otkazivanje rezervacije" + "\n"
								+ "3 - Vraćanje vozila" + "\n"
								+ "4 - Logout" + "\n");
		
			while(prikaziOpcije) {
				int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred jedne od ponuđenih opcija: ");
				
				if(!(opcija <= 0 || opcija > 4)) {
					prikaziOpcije = false;
				}
				
				if(opcija == 1) {
					System.out.println("Zakazivanje rezervacije: \n");
					OperacijeNadRezervacijama.zakazivanjeRezervacije(korisnik);
					break;
				} else if(opcija == 2) {
					System.out.println("Otkazivanje rezervacije: \n");
					OperacijeNadRezervacijama.otkazivanjeRezervacije(korisnik);
					break;
				} else if(opcija == 3) {
					System.out.println("Vraćanje vozila: \n");
					OperacijeNadVozilima.vracanjeVozila(korisnik);
					break;
				} else if(opcija == 4) {
					ponovnoLogovanje = true;
					break;
				} 
			}
		}
		
		if(ponovnoLogovanje) {
			return false;
		} else {
			return true;
		}
	}
}
