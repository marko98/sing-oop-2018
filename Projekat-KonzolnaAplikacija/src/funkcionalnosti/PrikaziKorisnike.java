package funkcionalnosti;

import java.io.File;
import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import liste.KorisniciLista;
import model.Korisnici;

public class PrikaziKorisnike {

	public static void prikaziKorisnike(String tipKorisnika) throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		KorisniciLista listaKorisnika = om.readValue(new File("korisnici.json"), KorisniciLista.class);
		
		if(tipKorisnika.equals("Sluzbenici")) {
			System.out.println("Slu�benici : \n");
		} else {
			System.out.println("Iznajmljiva�i vozila : \n");
		}

		int brojSluzbenika = 0;
		int brojIznajmljivaca = 0;
		for(Korisnici korisnik : listaKorisnika.getKorisnici()) {
			if(tipKorisnika.equals("Sluzbenici")) {
				if(korisnik.getClass().getName().equals("model.Sluzbenici")) {
					brojSluzbenika ++;
					System.out.println(brojSluzbenika + ". Korisni�ko ime: " + korisnik.getKorisnickoIme() + ", �ifra: " + korisnik.getSifra());
				}
			} else {
				brojIznajmljivaca ++;
				System.out.println(brojIznajmljivaca + ". Korisni�ko ime: " + korisnik.getKorisnickoIme() + ", �ifra: " + korisnik.getSifra());
			}
		} System.out.println("\n");
	}
	
}
