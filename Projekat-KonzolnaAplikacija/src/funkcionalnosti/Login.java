package funkcionalnosti;

import java.io.File;
import java.io.IOException;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import liste.KorisniciLista;
import model.Korisnici;

public class Login {

	public static Korisnici logovanje() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		KorisniciLista listaKorisnika = om.readValue(new File("korisnici.json"), KorisniciLista.class);

		boolean pronadjenKorisnik = false;
		Korisnici ulogovanaOsoba = null;
		
		while(!pronadjenKorisnik) {			
			Scanner tastatura = new Scanner(System.in);
			System.out.println("Vaše korisničko ime: ");
			String korisnickoIme = tastatura.nextLine();
			System.out.println("Vaša šifra: ");
			String sifra = tastatura.nextLine();
			
			for(Korisnici korisnik : listaKorisnika.getKorisnici()) {
				if(korisnik.getKorisnickoIme().equals(korisnickoIme) &&
						korisnik.getSifra().equals(sifra)) {
					pronadjenKorisnik = true;
					ulogovanaOsoba = korisnik;
					if(korisnik.getClass().getName().equals("model.Sluzbenici")) {
						System.out.println("Uspešno ste se ulogovali kao: " + korisnik.getIme() + " " + korisnik.getPrezime() + ".\n" +
								"Vaša uloga je: službenik.");
					} else {
						System.out.println("Uspešno ste se ulogovali kao: " + korisnik.getIme() + " " + korisnik.getPrezime() + ".\n" +
								"Vaša uloga je: iznajmljivač vozila.");
					}
				} 
			}
			
			if(!pronadjenKorisnik) {
				System.out.println("Logovanje nije uspešno, pokušajte ponovo.");	
			} 
		}
		return ulogovanaOsoba;
	}

}
