package funkcionalnosti;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Locale;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import liste.RezervacijeLista;
import liste.VozilaLista;
import model.Korisnici;
import model.Rezervacije;
import model.Vozila;
import model.VozilaICene;

public class OperacijeNadRezervacijama {

	public OperacijeNadRezervacijama() {
		super();
	}
	
	public static void zakazivanjeRezervacije(Korisnici korisnik) throws JsonParseException, JsonMappingException, IOException {
		Scanner tastatura = new Scanner(System.in);
		DateTimeFormatter format = DateTimeFormatter.ofPattern("yyyy-MM-dd", Locale.ENGLISH);
		LocalDate danasnjiDatum = LocalDate.now();
		LocalDate pocetakRecervacije, krajRecervacije;
		
		while(true) {
			try {
				System.out.println("Unesite datum početka rezervacije: ");
				String datum = tastatura.nextLine();
			    pocetakRecervacije = LocalDate.parse(datum, format);
			    
			    if(pocetakRecervacije.isAfter(danasnjiDatum)) {
			    	break;
			    } else {
			    	System.out.println("Datum početka rezervacije ne možе biti današnji datum ili istekao datum.");	
			    }
			} catch (DateTimeParseException e){
				System.out.println("Unesite datum u željenom formatu: yyyy-MM-dd!");
			}
		}
		
		while(true) {
			try {
				System.out.println("Unesite datum kraja rezervacije: ");
				String datum = tastatura.nextLine();
			    krajRecervacije = LocalDate.parse(datum, format);
			    
			    if(krajRecervacije.isAfter(pocetakRecervacije)) {
			    	break;
			    } else {
			    	System.out.println("Datum kraja rezervacije ne možе biti pre datuma početka rezervacije.");	
			    }
			} catch (DateTimeParseException e){
				System.out.println("Unesite datum u željenom formatu: yyyy-MM-dd!");
			}
		}
		
		ObjectMapper om = new ObjectMapper();
		VozilaLista listaVozila = om.readValue(new File("vozila.json"), VozilaLista.class);
		RezervacijeLista listaRezervacija = om.readValue(new File("rezervacije.json"), RezervacijeLista.class);
		
		ArrayList<Vozila> slobodnaVozila = new ArrayList<Vozila>();
		for(Vozila vozilo : listaVozila.getVozila()) {
			boolean postojiPreklapanje = false;		
			for(Rezervacije rezervacija : vozilo.getRezervacije()) {
				
				String StringDatumPocetkaRezervacije = rezervacija.getDatumPocetkaRezervacije();
				String[] parsiranDatumPocetkaRezervacije = StringDatumPocetkaRezervacije.split("-");
				LocalDate DatumPocetkaRezervacije = LocalDate.of(Integer.parseInt(parsiranDatumPocetkaRezervacije[0]), 
												Integer.parseInt(parsiranDatumPocetkaRezervacije[1]),
												Integer.parseInt(parsiranDatumPocetkaRezervacije[2]));
				
				String StringDatumKrajaRezervacije = rezervacija.getDatumKrajaRezervacije();
				String[] parsiranDatumKrajaRezervacije = StringDatumKrajaRezervacije.split("-");
				LocalDate DatumKrajaRezervacije = LocalDate.of(Integer.parseInt(parsiranDatumKrajaRezervacije[0]), 
												Integer.parseInt(parsiranDatumKrajaRezervacije[1]),
												Integer.parseInt(parsiranDatumKrajaRezervacije[2]));
				
				if (pocetakRecervacije.isBefore(DatumPocetkaRezervacije) && 
						(krajRecervacije.isAfter(DatumPocetkaRezervacije) || 
								krajRecervacije.equals(DatumPocetkaRezervacije))) {
						postojiPreklapanje = true;
					} else if ((pocetakRecervacije.isBefore(DatumKrajaRezervacije) ||
							pocetakRecervacije.equals(DatumKrajaRezervacije)) && 
							krajRecervacije.isAfter(DatumKrajaRezervacije)) {
						postojiPreklapanje = true;
					} else if ((pocetakRecervacije.isAfter(DatumPocetkaRezervacije) ||
							pocetakRecervacije.equals(DatumPocetkaRezervacije)) && 
								(krajRecervacije.isBefore(DatumKrajaRezervacije) ||
										krajRecervacije.equals(DatumKrajaRezervacije))) {
						postojiPreklapanje = true;
				}
			}
			if(!postojiPreklapanje) {
				slobodnaVozila.add(vozilo);
			}
		}
		
		System.out.println("Slobodna vozila: ");
		
		int brojacSlobodnihKola = 0;
		for(Vozila vozilo : slobodnaVozila) {
			if(vozilo.getLogickiObrisanoVozilo().equals(false)) {
				brojacSlobodnihKola++;
				if(vozilo.getClass().getName().equals("model.Bicikli")) {
					System.out.println(brojacSlobodnihKola + ". " + "Bicikli, registarske tablice: " + vozilo.getRegistracioniBroj());
				} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
					System.out.println(brojacSlobodnihKola + ". " + "Putničko vozilo, registarske tablice: " + vozilo.getRegistracioniBroj());
				} else {
					System.out.println(brojacSlobodnihKola + ". " + "Teretno vozilo, registarske tablice: " + vozilo.getRegistracioniBroj());
				}
			}
		}
		
		long brojDanaRezervacije = Duration.between(pocetakRecervacije.atStartOfDay(), krajRecervacije.atStartOfDay()).toDays();
		
		double brojPlaniranihKm = ProveraBroj.proveraBrojDouble("\nUnesite broj kilometara koje planirate da pređete: \n");

		
		ArrayList<VozilaICene> listaVozilaICena = new ArrayList<VozilaICene>();
		for(Vozila vozilo : slobodnaVozila) {
			double cena = vozilo.cenaPrelaskaRazdaljine(brojPlaniranihKm);
			
			if(vozilo.sledeciServis(brojPlaniranihKm)) {
				cena = vozilo.cenaPrelaskaRazdaljine(brojPlaniranihKm) + vozilo.getCenaServisiranja();
			}

			VozilaICene voziloICena = new VozilaICene(cena, vozilo);
			listaVozilaICena.add(voziloICena);
		}
	    
		listaVozilaICena.sort(model.VozilaICene.comparator);
		
		boolean dozvolaZaBicikl = true;
		Integer brojacSlobodnihKola2 = 0;
		HashMap<Integer, VozilaICene> map = new HashMap<Integer, VozilaICene>();
		
		System.out.println("Slobodna vozila i njihove cene: ");
		for(VozilaICene voziloICena : listaVozilaICena) {
			if(voziloICena.getVozilo().getLogickiObrisanoVozilo().equals(false)) {
				if(brojDanaRezervacije*60 < brojPlaniranihKm && 
						voziloICena.getVozilo().getClass().getName().equals("model.Bicikli")) {
					dozvolaZaBicikl = false;
				} else {	
					if(voziloICena.getVozilo().getClass().getName().equals("model.Bicikli")) {
						brojacSlobodnihKola2++;
						System.out.println(brojacSlobodnihKola2 + ". " + "Bicikli, registarske tablice: " + voziloICena.getVozilo().getRegistracioniBroj() +
								", cena: "+ Math.round(voziloICena.getCena()) + " dinara");
					} else if(voziloICena.getVozilo().getClass().getName().equals("model.PutnickoVozilo")) {
						brojacSlobodnihKola2++;
						System.out.println(brojacSlobodnihKola2 + ". " + "Putničko vozilo, registarske tablice: " + voziloICena.getVozilo().getRegistracioniBroj() + 
								", cena: "+ Math.round(voziloICena.getCena()) + " dinara");
					} else {
						brojacSlobodnihKola2++;
						System.out.println(brojacSlobodnihKola2 + ". " + "Teretno vozilo, registarske tablice: " + voziloICena.getVozilo().getRegistracioniBroj() +
								", cena: "+ Math.round(voziloICena.getCena()) + " dinara");
					}	
					map.put(brojacSlobodnihKola2, voziloICena);
				}
			}
		}
		
		int odustaniOpcija = brojacSlobodnihKola2 + 1;
		System.out.println(odustaniOpcija + ". Odustani");
		
		if(!dozvolaZaBicikl) {
			System.out.println("Planirane km nije moguće preći biciklom za " + brojDanaRezervacije + " dana.");
		}
		
		while(true) {
			int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred željenog vozila: ");
			if(!(opcija <= 0 || opcija > odustaniOpcija)) {
				if(opcija == odustaniOpcija) {
					break;
				} else {
					VozilaICene vozilo = map.get(opcija);
					ArrayList<Rezervacije> rezervacije = new ArrayList<Rezervacije>();

					for(Rezervacije rezervacija : vozilo.getVozilo().getRezervacije()) {
						rezervacije.add(rezervacija);
					}
					Rezervacije rezervacija = new Rezervacije(korisnik.getKorisnickoIme(), pocetakRecervacije.toString(), krajRecervacije.toString(), vozilo.getCena(), false);
					rezervacije.add(rezervacija);
					listaRezervacija.add(rezervacija);
					
					vozilo.getVozilo().setRezervacije(rezervacije);
					ObjectMapper zapisiRezervaciju = new ObjectMapper();
					zapisiRezervaciju.enable(SerializationFeature.INDENT_OUTPUT);
					
					zapisiRezervaciju.writeValue(new File("vozila.json"), listaVozila);
					zapisiRezervaciju.writeValue(new File("rezervacije.json"), listaRezervacija);
					
					System.out.println("Rezervacija je zakazana.");
					break;
				}	
			}
		}
	}

	public static void otkazivanjeRezervacije(Korisnici korisnik) throws JsonParseException, JsonMappingException, IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		VozilaLista listaVozila = mapper.readValue(new File("vozila.json"), VozilaLista.class);
		ArrayList<Vozila> rezervisanaVozila = new ArrayList<Vozila>();
		
		RezervacijeLista listaRezervacije = mapper.readValue(new File("rezervacije.json"), RezervacijeLista.class);
		boolean trazenjeRezervacijaUToku = true;
		
		if(korisnik.getClass().getName().equals("model.IznajmljivaciVozila")) {
		
			for(Vozila vozilo : listaVozila.getVozila()) {
				boolean dozvola = true;
				for(Rezervacije rezervacija : vozilo.getRezervacije()) {
					if(korisnik.getKorisnickoIme().equals(rezervacija.getKorisnickoImeIznajmljivaca()) &&
							rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
						for(Vozila rezervisanoVozilo : rezervisanaVozila) {
							if(vozilo.getRegistracioniBroj().equals(rezervisanoVozilo.getRegistracioniBroj())) {
								dozvola = false;
							}
						}
						if(dozvola) {
							rezervisanaVozila.add(vozilo);
						}
					}
				}
			}
			
		} else {
			
			for(Vozila vozilo : listaVozila.getVozila()) {
				boolean dozvola = true;
				for(Rezervacije rezervacija : vozilo.getRezervacije()) {
					if(rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
						for(Vozila rezervisanoVozilo : rezervisanaVozila) {
							if(vozilo.getRegistracioniBroj().equals(rezervisanoVozilo.getRegistracioniBroj())) {
								dozvola = false;
							}
						}
						if(dozvola) {
							rezervisanaVozila.add(vozilo);
						}
					}
				}
			}
			
		}
		
		if(rezervisanaVozila.isEmpty()) {
			System.out.println("Nemate aktivnih rezervacija");
		} else {
			HashMap<Integer, Vozila> map = new HashMap<Integer, Vozila>();
			
			System.out.println("Aktivne rezervacije su: ");
			int brojacRezervisanihVozila = 0;
			
			for(Vozila vozilo : rezervisanaVozila) {
				boolean rezervacijePostoje = false;
				for(Rezervacije rezervacija : vozilo.getRezervacije()) {	
					if(rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
						if(!rezervacijePostoje) {
							brojacRezervisanihVozila++;
							if(vozilo.getClass().getName().equals("model.Bicikli")) {
								System.out.println(brojacRezervisanihVozila + ". Bicikl, registarskih tablica: " +
													vozilo.getRegistracioniBroj());
							} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
								System.out.println(brojacRezervisanihVozila + ". Putničko vozilo, registarskih tablica: " +
										vozilo.getRegistracioniBroj());
							} else {
								System.out.println(brojacRezervisanihVozila + ". Teretno vozilo, registarskih tablica: " +
										vozilo.getRegistracioniBroj());
							}
							map.put(brojacRezervisanihVozila, vozilo);
							rezervacijePostoje = true;
						}
					}
				}
			}
			
			int odustaniOpcija = brojacRezervisanihVozila + 1;
			System.out.println(odustaniOpcija + ". Odustani");
			
			if(korisnik.getClass().getName().equals("model.IznajmljivaciVozila")) {
			
				while(trazenjeRezervacijaUToku) {
					int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije: ");
					if(!(opcija <= 0 || opcija > odustaniOpcija)) {
						
						if(opcija == odustaniOpcija) {
							trazenjeRezervacijaUToku = false;
						} else {
							Vozila vozilo = map.get(opcija);
							
							Integer brojacRezervisanihVozila2 = 0;
							HashMap<Integer, Rezervacije> map2 = new HashMap<Integer, Rezervacije>();
							
							for(Rezervacije rezervacija : vozilo.getRezervacije()) {
								if(korisnik.getKorisnickoIme().equals(rezervacija.getKorisnickoImeIznajmljivaca()) &&
										rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
									brojacRezervisanihVozila2++;
									if(vozilo.getClass().getName().equals("model.Bicikli")) {
										System.out.println(brojacRezervisanihVozila2 + ". Bicikl, registarskih tablica: " +
															vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
															rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
															rezervacija.getDatumKrajaRezervacije());
									} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
										System.out.println(brojacRezervisanihVozila2 + ". Putničko vozilo, registarskih tablica: " +
												vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
												rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
												rezervacija.getDatumKrajaRezervacije());
									} else {
										System.out.println(brojacRezervisanihVozila2 + ". Teretno vozilo, registarskih tablica: " +
												vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
												rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
												rezervacija.getDatumKrajaRezervacije());
									}
									map2.put(brojacRezervisanihVozila2, rezervacija);
								}
							}
							
							int odustaniOpcija2 = brojacRezervisanihVozila2 + 1;
							System.out.println(odustaniOpcija2 + ". Odustani");
							
							while(true) {
								int opcija2 = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije koju želite da otkažete: ");
								if(!(opcija2 <= 0 || opcija2 > odustaniOpcija2)) {
									if(opcija2 == odustaniOpcija2) {
										trazenjeRezervacijaUToku = false;
										break;
									} else {
										Rezervacije rezervacija = map2.get(opcija2);
									
										for(Rezervacije rezervacija2 : vozilo.getRezervacije()) {
											if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija2.getKorisnickoImeIznajmljivaca()) &&
													rezervacija.getDatumPocetkaRezervacije().equals(rezervacija2.getDatumPocetkaRezervacije()) &&
													rezervacija.getDatumKrajaRezervacije().equals(rezervacija2.getDatumKrajaRezervacije())) {
												rezervacija2.setLogickiObrisanaRezervacija(true);
											}
										}
										
										for(Rezervacije rezervacija3 : listaRezervacije.getRezervacije()) {
											if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija3.getKorisnickoImeIznajmljivaca()) &&
													rezervacija.getDatumPocetkaRezervacije().equals(rezervacija3.getDatumPocetkaRezervacije()) &&
													rezervacija.getDatumKrajaRezervacije().equals(rezervacija3.getDatumKrajaRezervacije())) {
												rezervacija3.setLogickiObrisanaRezervacija(true);
											}								
										}
										
										ObjectMapper obrisiRezervaciju = new ObjectMapper();
										obrisiRezervaciju.enable(SerializationFeature.INDENT_OUTPUT);
										
										obrisiRezervaciju.writeValue(new File("vozila.json"), listaVozila);
										obrisiRezervaciju.writeValue(new File("rezervacije.json"), listaRezervacije);
										
										System.out.println("Rezervacija je obrisana.");
										trazenjeRezervacijaUToku = false;
										break;
									}
								}
							}
						}
						
					}
				}	
			} else {
				while(trazenjeRezervacijaUToku) {
					int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije: ");
					if(!(opcija <= 0 || opcija > odustaniOpcija)) {
						
						if(opcija == odustaniOpcija) {
							trazenjeRezervacijaUToku = false;
						} else {
							Vozila vozilo = map.get(opcija);
							
							Integer brojacRezervisanihVozila2 = 0;
							HashMap<Integer, Rezervacije> map2 = new HashMap<Integer, Rezervacije>();
							
							for(Rezervacije rezervacija : vozilo.getRezervacije()) {
								if(rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
									brojacRezervisanihVozila2++;
									if(vozilo.getClass().getName().equals("model.Bicikli")) {
										System.out.println(brojacRezervisanihVozila2 + ". Bicikl, registarskih tablica: " +
															vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
															rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
															rezervacija.getDatumKrajaRezervacije() + ". Korisničko ime iznajmljivača: " + rezervacija.getKorisnickoImeIznajmljivaca());
									} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
										System.out.println(brojacRezervisanihVozila2 + ". Putničko vozilo, registarskih tablica: " +
												vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
												rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
												rezervacija.getDatumKrajaRezervacije() + ". Korisničko ime iznajmljivača: " + rezervacija.getKorisnickoImeIznajmljivaca());
									} else {
										System.out.println(brojacRezervisanihVozila2 + ". Teretno vozilo, registarskih tablica: " +
												vozilo.getRegistracioniBroj() + ", datum početka rezervacije: " + 
												rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
												rezervacija.getDatumKrajaRezervacije() + ". Korisničko ime iznajmljivača: " + rezervacija.getKorisnickoImeIznajmljivaca());
									}
									map2.put(brojacRezervisanihVozila2, rezervacija);
								}
							}
							int odustaniOpcija2 = brojacRezervisanihVozila2 + 1;
							System.out.println(odustaniOpcija2 + ". Odustani");
							
							while(true) {
								int opcija2 = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije koju želite da otkažete: ");
								if(!(opcija2 <= 0 || opcija2 > odustaniOpcija2)) {
									if(opcija2 == odustaniOpcija2) {
										trazenjeRezervacijaUToku = false;
										break;
									} else {
										Rezervacije rezervacija = map2.get(opcija2);
										
										for(Rezervacije rezervacija2 : vozilo.getRezervacije()) {
											if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija2.getKorisnickoImeIznajmljivaca()) &&
													rezervacija.getDatumPocetkaRezervacije().equals(rezervacija2.getDatumPocetkaRezervacije()) &&
													rezervacija.getDatumKrajaRezervacije().equals(rezervacija2.getDatumKrajaRezervacije())) {
												rezervacija2.setLogickiObrisanaRezervacija(true);
											}
										}
										
										for(Rezervacije rezervacija3 : listaRezervacije.getRezervacije()) {
											if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija3.getKorisnickoImeIznajmljivaca()) &&
													rezervacija.getDatumPocetkaRezervacije().equals(rezervacija3.getDatumPocetkaRezervacije()) &&
													rezervacija.getDatumKrajaRezervacije().equals(rezervacija3.getDatumKrajaRezervacije())) {
												rezervacija3.setLogickiObrisanaRezervacija(true);
											}								
										}
										
										ObjectMapper obrisiRezervaciju = new ObjectMapper();
										obrisiRezervaciju.enable(SerializationFeature.INDENT_OUTPUT);
										
										obrisiRezervaciju.writeValue(new File("vozila.json"), listaVozila);
										obrisiRezervaciju.writeValue(new File("rezervacije.json"), listaRezervacije);
										
										System.out.println("Rezervacija je obrisana.");
										trazenjeRezervacijaUToku = false;
										break;
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
}
