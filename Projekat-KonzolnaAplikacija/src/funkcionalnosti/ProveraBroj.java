package funkcionalnosti;

import java.util.InputMismatchException;
import java.util.Scanner;

public class ProveraBroj {

	public static int proveraBrojInt(String tekst) {
		System.out.println(tekst);
		int opcija;
		while(true) {
			try {
				Scanner tastatura = new Scanner(System.in);
				opcija = tastatura.nextInt();
				
				break;
			} catch (InputMismatchException e){
				System.out.println(tekst);
			}
		}
		return opcija;
	}
	
	public static double proveraBrojDouble(String tekst) {
		System.out.println(tekst);
		double opcija;
		while(true) {
			try {
				Scanner tastatura = new Scanner(System.in);
				opcija = tastatura.nextDouble();
				
				break;
			} catch (InputMismatchException e){
				System.out.println(tekst);
			}
		}
		return opcija;
	}

}
