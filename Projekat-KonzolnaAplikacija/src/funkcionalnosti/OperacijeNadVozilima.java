package funkcionalnosti;

import java.io.File;
import java.io.IOException;
import java.time.Duration;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import liste.GorivaLista;
import liste.RezervacijeLista;
import liste.ServisiranjaLista;
import liste.VozilaLista;
import model.Bicikli;
import model.Goriva;
import model.Korisnici;
import model.PutnickoVozilo;
import model.Rezervacije;
import model.Servisiranja;
import model.TeretnoVozilo;
import model.Vozila;

public class OperacijeNadVozilima {

	public OperacijeNadVozilima() {
		super();
	}
	
	public static PutnickoVozilo dovrsiPutnickoVozilo(PutnickoVozilo nedovrsenoVozilo) throws JsonParseException, JsonMappingException, IOException {	
		boolean trazenjeGoriva = true;
		while(trazenjeGoriva) {
			System.out.println("Vrste goriva: ");
			ObjectMapper om = new ObjectMapper();
			GorivaLista listaGoriva = om.readValue(new File("goriva.json"), GorivaLista.class);
			
			Integer brojacPostojecihGoriva = 0;
			HashMap<Integer, Goriva> map = new HashMap<Integer, Goriva>();
			
			for(Goriva moguceGorivo : listaGoriva.getGoriva()) {
				brojacPostojecihGoriva++;
				System.out.println(brojacPostojecihGoriva + ". " + "Naziv goriva: " + moguceGorivo.getNazivGoriva() +
						", cena: "+ Math.round(moguceGorivo.getCenaGoriva()));
				map.put(brojacPostojecihGoriva, moguceGorivo);
			}
			while(true) {
				int odabirGoriva = ProveraBroj.proveraBrojInt("Odaberite tip goriva za novo vozilo: ");
				
				if(!(odabirGoriva <= 0 || odabirGoriva > brojacPostojecihGoriva)) {
					nedovrsenoVozilo.setGorivo(map.get(odabirGoriva));
				
					trazenjeGoriva = false;
					break;
				}
			}
		}
		double potrosnjaGoriva = ProveraBroj.proveraBrojDouble("Potro�nja goriva: ");
		int brojVrata = ProveraBroj.proveraBrojInt("Unesite broj vrata: ");

		nedovrsenoVozilo.setPotrosnjaGoriva(potrosnjaGoriva);
		nedovrsenoVozilo.setBrojVrata(brojVrata);
		

		return nedovrsenoVozilo;		
	}
	
	public static TeretnoVozilo dovrsiTeretnoVozilo(TeretnoVozilo nedovrsenoVozilo) throws JsonParseException, JsonMappingException, IOException {
		boolean trazenjeGoriva = true;
		while(trazenjeGoriva) {
			System.out.println("Vrste goriva: ");
			ObjectMapper om = new ObjectMapper();
			GorivaLista listaGoriva = om.readValue(new File("goriva.json"), GorivaLista.class);
			
			Integer brojacPostojecihGoriva = 0;
			HashMap<Integer, Goriva> map = new HashMap<Integer, Goriva>();
			
			for(Goriva moguceGorivo : listaGoriva.getGoriva()) {
				brojacPostojecihGoriva++;
				System.out.println(brojacPostojecihGoriva + ". " + "Naziv goriva: " + moguceGorivo.getNazivGoriva() +
						", cena: "+ Math.round(moguceGorivo.getCenaGoriva()));
				map.put(brojacPostojecihGoriva, moguceGorivo);
			}
			while(true) {
				int odabirGoriva = ProveraBroj.proveraBrojInt("Odaberite tip goriva za novo vozilo: ");
				
				if(!(odabirGoriva <= 0 || odabirGoriva > brojacPostojecihGoriva)) {
					nedovrsenoVozilo.setGorivo(map.get(odabirGoriva));
				
					trazenjeGoriva = false;
					break;
				}
			}
		}
		double potrosnjaGoriva = ProveraBroj.proveraBrojDouble("Potro�nja goriva: ");
		int brojVrata = ProveraBroj.proveraBrojInt("Unesite broj vrata: ");
		double maksimalnaMasa = ProveraBroj.proveraBrojDouble("Maksimalna masa: ");
		double maksimalnaVisina = ProveraBroj.proveraBrojDouble("Maksimalna visina: ");

		nedovrsenoVozilo.setPotrosnjaGoriva(potrosnjaGoriva);
		nedovrsenoVozilo.setBrojVrata(brojVrata);		
		nedovrsenoVozilo.setMaksimalnaMasa(maksimalnaMasa);
		nedovrsenoVozilo.setMaksimalnaVisina(maksimalnaVisina);

		return nedovrsenoVozilo;		
	}

	public static void dodavanjeVozila() throws JsonParseException, JsonMappingException, IOException {
		System.out.println("Tipovi vozila: \n" + "1 - Bicikli \n" + "2 - Putni�ko vozilo \n" + "3 - Teretno vozilo \n" + "4 - Odustani \n");
		ObjectMapper saveNovoVozilo = new ObjectMapper();
		saveNovoVozilo.enable(SerializationFeature.INDENT_OUTPUT);
		
		VozilaLista listaVozila = saveNovoVozilo.readValue(new File("vozila.json"), VozilaLista.class);
		
		while(true) {
			int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred ponu�ene opcije: ");
			if(!(opcija <= 0 || opcija > 4)) {
				
				if (opcija != 4) {
					Scanner tastatura = new Scanner(System.in);
					
					String registracioniBroj;
					while(true) {
						boolean postojiTablica = false;
						System.out.println("Unesite registracionu tablicu novog vozila: ");
						registracioniBroj = tastatura.nextLine();
						for(Vozila vozilo : listaVozila.getVozila()) {
							if(registracioniBroj.equals(vozilo.getRegistracioniBroj())) {
								postojiTablica = true;
							}
						}
						if(!postojiTablica) {
							break;
						} else {
							System.out.println("Uneta registraciona tablica je zauzeta.");
						}
					}
					
					ArrayList<Servisiranja> servisiranje = new ArrayList<Servisiranja>();
					ArrayList<Rezervacije> rezervacije = new ArrayList<Rezervacije>();
					double brKmPreServisiranja = ProveraBroj.proveraBrojDouble("Unesite broj km od jednog do drugog servisa: ");
					double cenaServisiranja = ProveraBroj.proveraBrojDouble("Cena servisiranja: ");
					double cenaIznajmljivanjaPoDanu = ProveraBroj.proveraBrojDouble("Cena iznajmljivanja po danu: ");
					int brojSedista = ProveraBroj.proveraBrojInt("Unesite broj sedi�ta: ");
					
					if(opcija == 1) {
						Bicikli noviBicikl = new Bicikli(registracioniBroj, null, servisiranje, rezervacije, 0, 0, brKmPreServisiranja, cenaServisiranja, cenaIznajmljivanjaPoDanu, brojSedista, 0, false);
			
						listaVozila.getVozila().add(noviBicikl);
						saveNovoVozilo.writeValue(new File("vozila.json"), listaVozila);
						System.out.println("Dodat je novi bicikl.");
						break;					
					} else if(opcija == 2) {
						PutnickoVozilo nedovrsenoVozilo = new PutnickoVozilo(registracioniBroj, null, servisiranje, rezervacije, 0, 0, brKmPreServisiranja, cenaServisiranja, cenaIznajmljivanjaPoDanu, brojSedista, 0, false);
						
						listaVozila.getVozila().add(dovrsiPutnickoVozilo(nedovrsenoVozilo));
						saveNovoVozilo.writeValue(new File("vozila.json"), listaVozila);
						System.out.println("Dodato je novo putni�ko vozilo.");
						break;
					} else if(opcija == 3) {
						TeretnoVozilo nedovrsenoVozilo = new TeretnoVozilo(registracioniBroj, null, servisiranje, rezervacije, 0, 0, brKmPreServisiranja, cenaServisiranja, cenaIznajmljivanjaPoDanu, brojSedista, 0, false, 0, 0);
						
						listaVozila.getVozila().add(dovrsiTeretnoVozilo(nedovrsenoVozilo));
						saveNovoVozilo.writeValue(new File("vozila.json"), listaVozila);
						System.out.println("Dodato je novo teretno vozilo.");
						break;
					} 
					
				} else {
					break;
				}
			}	
		}
	}
	
	public static void vracanjeVozila(Korisnici korisnik) throws JsonParseException, JsonMappingException, IOException {
		
		ObjectMapper mapper = new ObjectMapper();
		VozilaLista listaVozila = mapper.readValue(new File("vozila.json"), VozilaLista.class);
		ArrayList<Vozila> rezervisanaVozila = new ArrayList<Vozila>();
		
		RezervacijeLista listaRezervacije = mapper.readValue(new File("rezervacije.json"), RezervacijeLista.class);
		ServisiranjaLista listaServisiranja = mapper.readValue(new File("servisiranja.json"), ServisiranjaLista.class);
		boolean trazenjeRezervacijaUToku = true;
		boolean trazenjeRezervacijaUToku2 = true;
		
		for(Vozila vozilo : listaVozila.getVozila()) {
			boolean dozvola = true;
			for(Rezervacije rezervacija : vozilo.getRezervacije()) {
				if(korisnik.getKorisnickoIme().equals(rezervacija.getKorisnickoImeIznajmljivaca()) &&
						rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
					for(Vozila rezervisanoVozilo : rezervisanaVozila) {
						if(vozilo.getRegistracioniBroj().equals(rezervisanoVozilo.getRegistracioniBroj())) {
							dozvola = false;
						}
					}
					if(dozvola) {
						rezervisanaVozila.add(vozilo);
					}
				}
			}
		}
		

		
		if(rezervisanaVozila.isEmpty()) {
			System.out.println("Nemate aktivnih rezervacija");
		} else {
			HashMap<Integer, Vozila> map = new HashMap<Integer, Vozila>();
			
			System.out.println("Aktivne rezervacije su: ");
			int brojacRezervisanihVozila = 0;
			
			for(Vozila vozilo : rezervisanaVozila) {
				boolean rezervacijePostoje = false;
				for(Rezervacije rezervacija : vozilo.getRezervacije()) {
					if(rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
						if(!rezervacijePostoje) {
							brojacRezervisanihVozila++;
							if(vozilo.getClass().getName().equals("model.Bicikli")) {
								System.out.println(brojacRezervisanihVozila + "." + "Bicikl, registarskih tablica: " +
													vozilo.getRegistracioniBroj() + ".");
							} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
								System.out.println(brojacRezervisanihVozila + "." + "Putni�ko vozilo, registarskih tablica: " +
										vozilo.getRegistracioniBroj() + ".");
							} else {
								System.out.println(brojacRezervisanihVozila + "." + "Teretno vozilo, registarskih tablica: " +
										vozilo.getRegistracioniBroj() + ".");
							}
							map.put(brojacRezervisanihVozila, vozilo);
							rezervacijePostoje = true;
						}
					}
				}		
			}
			int odustaniOpcija = brojacRezervisanihVozila + 1;
			System.out.println(odustaniOpcija + ". Odustani");
			
			while(trazenjeRezervacijaUToku) {
				int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred vozila koje �elite da vratite: ");
				if(!(opcija <= 0 || opcija > odustaniOpcija)) {
					if(opcija == odustaniOpcija) {
						trazenjeRezervacijaUToku = false;
					} else {
						Vozila vozilo = map.get(opcija);
						
						Integer brojacRezervisanihVozila2 = 0;
						HashMap<Integer, Rezervacije> map2 = new HashMap<Integer, Rezervacije>();
						
						for(Rezervacije rezervacija : vozilo.getRezervacije()) {
							if(korisnik.getKorisnickoIme().equals(rezervacija.getKorisnickoImeIznajmljivaca()) &&
									rezervacija.getLogickiObrisanaRezervacija().equals(false)) {
								brojacRezervisanihVozila2++;
								if(vozilo.getClass().getName().equals("model.Bicikli")) {
									System.out.println(brojacRezervisanihVozila2 + "." + "Bicikl, registarskih tablica: " +
														vozilo.getRegistracioniBroj() + ", datum po�etka rezervacije: " + 
														rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
														rezervacija.getDatumKrajaRezervacije() + ".");
								} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
									System.out.println(brojacRezervisanihVozila2 + "." + "Putni�ko vozilo, registarskih tablica: " +
											vozilo.getRegistracioniBroj() + ", datum po�etka rezervacije: " + 
											rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
											rezervacija.getDatumKrajaRezervacije() + ".");
								} else {
									System.out.println(brojacRezervisanihVozila2 + "." + "Teretno vozilo, registarskih tablica: " +
											vozilo.getRegistracioniBroj() + ", datum po�etka rezervacije: " + 
											rezervacija.getDatumPocetkaRezervacije() + ", datum kraja rezervacije: " +
											rezervacija.getDatumKrajaRezervacije() + ".");
								}
								map2.put(brojacRezervisanihVozila2, rezervacija);
							}
						}
						
						int odustaniOpcija2 = brojacRezervisanihVozila2 + 1;
						System.out.println(odustaniOpcija2 + ". Odustani");
						
						while(trazenjeRezervacijaUToku2) {
							int opcija2 = ProveraBroj.proveraBrojInt("Unesite broj ispred rezervacije koju �elite da zaklju�ite ili odaberite opciju odustani: ");
							if(!(opcija2 <= 0 || opcija2 > odustaniOpcija2)) {
								if(opcija2 == odustaniOpcija2) {
									trazenjeRezervacijaUToku = false;
									trazenjeRezervacijaUToku2 = false;
								} else {
									Rezervacije rezervacija = map2.get(opcija2);
									
									double brPredjenihKm = ProveraBroj.proveraBrojDouble("Unesite broj pre�enih km: ");
									
									String StringDatumPocetkaRezervacije = rezervacija.getDatumPocetkaRezervacije();
									String[] parsiranDatumPocetkaRezervacije = StringDatumPocetkaRezervacije.split("-");
									LocalDate DatumPocetkaRezervacije = LocalDate.of(Integer.parseInt(parsiranDatumPocetkaRezervacije[0]), 
																	Integer.parseInt(parsiranDatumPocetkaRezervacije[1]),
																	Integer.parseInt(parsiranDatumPocetkaRezervacije[2]));
									
									String StringDatumKrajaRezervacije = rezervacija.getDatumKrajaRezervacije();
									String[] parsiranDatumKrajaRezervacije = StringDatumKrajaRezervacije.split("-");
									LocalDate DatumKrajaRezervacije = LocalDate.of(Integer.parseInt(parsiranDatumKrajaRezervacije[0]), 
																	Integer.parseInt(parsiranDatumKrajaRezervacije[1]),
																	Integer.parseInt(parsiranDatumKrajaRezervacije[2]));
								
									long brojDanaRezervacije = Duration.between(DatumPocetkaRezervacije.atStartOfDay(), DatumKrajaRezervacije.atStartOfDay()).toDays();
									
									if(vozilo.getClass().getName().equals("model.Bicikli")) {
										if(brPredjenihKm > 60*brojDanaRezervacije) {
											System.out.println("Na biciklu nije bilo mogu�e pre�i " + brPredjenihKm + " km.\n" + 
																"Obratite nam se povodom ovog problema na broj 021/452-698.");
											trazenjeRezervacijaUToku = false;
											break;
										}
									}
									
									double cena = vozilo.cenaPrelaskaRazdaljine(brPredjenihKm);
									
									if(vozilo.sledeciServis(brPredjenihKm)) {
										System.out.println("Servis je ura�unat u cenu.");
										cena = cena + vozilo.getCenaServisiranja();
									}
									
									System.out.println("Ukupno za uplatu: " +  Math.round(cena) + " dinara." + "\n" + 
														"Zakljucite ra�un: " + "\n" +
														"1. Da" + "\n" + "2. Ne - poni�ti ra�un");
									
									while(true) {
										int zakljuciRacunOpcija = ProveraBroj.proveraBrojInt("Odaberite opciju: ");
										
										if(!(zakljuciRacunOpcija < 0 || zakljuciRacunOpcija > 2)) {
											if(zakljuciRacunOpcija == 1) {
												for(Rezervacije rezervacija2 : vozilo.getRezervacije()) {
													if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija2.getKorisnickoImeIznajmljivaca()) &&
															rezervacija.getDatumPocetkaRezervacije().equals(rezervacija2.getDatumPocetkaRezervacije()) &&
															rezervacija.getDatumKrajaRezervacije().equals(rezervacija2.getDatumKrajaRezervacije())) {
														rezervacija2.setLogickiObrisanaRezervacija(true);
													}
												}
												
												for(Rezervacije rezervacija3 : listaRezervacije.getRezervacije()) {
													if(rezervacija.getKorisnickoImeIznajmljivaca().equals(rezervacija3.getKorisnickoImeIznajmljivaca()) &&
															rezervacija.getDatumPocetkaRezervacije().equals(rezervacija3.getDatumPocetkaRezervacije()) &&
															rezervacija.getDatumKrajaRezervacije().equals(rezervacija3.getDatumKrajaRezervacije())) {
														rezervacija3.setLogickiObrisanaRezervacija(true);
													}								
												}
												vozilo.setPredjeniKm(vozilo.getPredjeniKm() + brPredjenihKm);
												
												ArrayList<Servisiranja> listaServisa = new ArrayList<Servisiranja>();
												for (Servisiranja servis : vozilo.getServisiranje()) {
													listaServisa.add(servis);
												}
												LocalDate danasnjiDatum = LocalDate.now();
												Servisiranja noviServis = new Servisiranja(vozilo.getRegistracioniBroj(), danasnjiDatum.toString(), vozilo.getPredjeniKm());
												listaServisa.add(noviServis);
												vozilo.setServisiranje(listaServisa);
												
												listaServisiranja.getServisiranja().add(noviServis);
												
												ObjectMapper obrisiRezervaciju = new ObjectMapper();
												obrisiRezervaciju.enable(SerializationFeature.INDENT_OUTPUT);
												
												obrisiRezervaciju.writeValue(new File("vozila.json"), listaVozila);
												obrisiRezervaciju.writeValue(new File("rezervacije.json"), listaRezervacije);
												obrisiRezervaciju.writeValue(new File("servisiranja.json"), listaServisiranja);
												
												System.out.println("Ra�un je zaklju�en. Vozilo je vra�eno.");
												trazenjeRezervacijaUToku = false;
												trazenjeRezervacijaUToku2 = false;
												break;
											} else if (zakljuciRacunOpcija == 2) {
												System.out.println("Izdavanje ra�una je poni�teno.");
												trazenjeRezervacijaUToku = false;
												trazenjeRezervacijaUToku2 = false;
												break;
											}
										}
									}
								}
							}
						}
					}
				}
			}
		}
	}
	
	public static void brisanjeVozila() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		om.enable(SerializationFeature.INDENT_OUTPUT);
		VozilaLista listaVozila = om.readValue(new File("vozila.json"), VozilaLista.class);
		
		System.out.println("Aktivna slobodna vozila: ");
		HashMap<Integer, Vozila> map = new HashMap<Integer, Vozila>();
		boolean postojeZauzetaVozila = false;
		boolean postojeAktivnaVozila = false;
		Integer brojacAktivnihVozila = 0;
		for(Vozila vozilo : listaVozila.getVozila()) {
			boolean prikaziVozilo = true;
			for(Rezervacije rezervacija : vozilo.getRezervacije()) {
				if(rezervacija.getLogickiObrisanaRezervacija().equals(false) || vozilo.getLogickiObrisanoVozilo().equals(true)) {
					postojeZauzetaVozila = true;
					prikaziVozilo = false;
				} 
			}
			if(prikaziVozilo) {
				if(vozilo.getLogickiObrisanoVozilo().equals(false)) {
					brojacAktivnihVozila++;
					if(vozilo.getClass().getName().equals("model.Bicikli")) {
						System.out.println(brojacAktivnihVozila + ". Bicikli, registarskih tablica: " + vozilo.getRegistracioniBroj());
					} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
						System.out.println(brojacAktivnihVozila + ". Putni�ko vozilo, registarskih tablica: " + vozilo.getRegistracioniBroj());
					} else {
						System.out.println(brojacAktivnihVozila + ". Teretno vozilo, registarskih tablica: " + vozilo.getRegistracioniBroj());
					}
					map.put(brojacAktivnihVozila, vozilo);
					postojeAktivnaVozila = true;
				}
			}
		}
		int odustaniOpcija = brojacAktivnihVozila + 1;
		
		if(!postojeAktivnaVozila) {
			System.out.println("Sva vozila su obrisana ili rezervisana, te njihovo 'brisanje' nije dozvoljeno");
		} else {
			System.out.println(odustaniOpcija + ". Odustani");
			if(postojeZauzetaVozila) {
				System.out.println("Neka od vozila nisu prikazana jer su rezervisana");
			}
		}
		
		
		while(postojeAktivnaVozila) {
			int opcijaVozilo = ProveraBroj.proveraBrojInt("Unesite broj ispred vozila koje �elite da 'obri�ete' ili poslednju opciju za odustajanje: ");
			if(!(opcijaVozilo <= 0 || opcijaVozilo > odustaniOpcija)) {
				if(opcijaVozilo == odustaniOpcija) {
					break;
				} else {
					Vozila vozilo = map.get(opcijaVozilo);
					vozilo.setLogickiObrisanoVozilo(true);
					om.writeValue(new File("vozila.json"), listaVozila);
					System.out.println("Vozilo je 'obrisano'.");
					break;
				}	
			}
		}
	}

	public static void aktiviranjeVozila() throws JsonParseException, JsonMappingException, IOException {
		ObjectMapper om = new ObjectMapper();
		om.enable(SerializationFeature.INDENT_OUTPUT);
		VozilaLista listaVozila = om.readValue(new File("vozila.json"), VozilaLista.class);
		
		System.out.println("Obrisana vozila: ");
		HashMap<Integer, Vozila> map = new HashMap<Integer, Vozila>();
		boolean postojeObrisanaVozila = false;
		Integer brojacObrisanihVozila = 0;
		for(Vozila vozilo : listaVozila.getVozila()) {
			if(vozilo.getLogickiObrisanoVozilo().equals(true)) {
				brojacObrisanihVozila++;
				if(vozilo.getClass().getName().equals("model.Bicikli")) {
					System.out.println(brojacObrisanihVozila + ". Bicikli, registarskih tablica: " + vozilo.getRegistracioniBroj());
				} else if(vozilo.getClass().getName().equals("model.PutnickoVozilo")) {
					System.out.println(brojacObrisanihVozila + ". Putni�ko vozilo, registarskih tablica: " + vozilo.getRegistracioniBroj());
				} else {
					System.out.println(brojacObrisanihVozila + ". Teretno vozilo, registarskih tablica: " + vozilo.getRegistracioniBroj());
				}
				map.put(brojacObrisanihVozila, vozilo);
				postojeObrisanaVozila = true;
			}
		}
		int odustaniOpcija = brojacObrisanihVozila + 1;
		
		if(!postojeObrisanaVozila) {
			System.out.println("Nema obrisanih vozila");
		} else {
			System.out.println(odustaniOpcija + ". Odustani");
		}
		
		while(postojeObrisanaVozila) {
			int opcijaVozilo = ProveraBroj.proveraBrojInt("Unesite broj ispred vozila koje �elite da 'aktivirate' ili poslednju opciju za odustajanje: ");
			if(!(opcijaVozilo <= 0 || opcijaVozilo > odustaniOpcija)) {
				if(opcijaVozilo == odustaniOpcija) {
					break;
				} else {
					Vozila vozilo = map.get(opcijaVozilo);
					vozilo.setLogickiObrisanoVozilo(false);
					om.writeValue(new File("vozila.json"), listaVozila);
					System.out.println("Vozilo je 'aktivirano'.");
					break;
				}
			}
		}
	}
	
}
