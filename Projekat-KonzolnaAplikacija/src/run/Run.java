package run;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.SerializationFeature;

import liste.GorivaLista;
import liste.KorisniciLista;
import liste.RezervacijeLista;
import liste.ServisiranjaLista;
import liste.VozilaLista;
import model.Bicikli;
import model.Goriva;
import model.IznajmljivaciVozila;
import model.Korisnici;
import model.PutnickoVozilo;
import model.Rezervacije;
import model.Servisiranja;
import model.Sluzbenici;
import model.TeretnoVozilo;
import model.Vozila;
public class Run {

	public static void main() throws JsonMappingException, IOException {
		KorisniciLista listaKorisnika = new KorisniciLista(new ArrayList<Korisnici>());
		VozilaLista listaVozila = new VozilaLista(new ArrayList<Vozila>());
		ServisiranjaLista listaServisiranja = new ServisiranjaLista(new ArrayList<Servisiranja>());
		GorivaLista listaGoriva = new GorivaLista(new ArrayList<Goriva>());
		RezervacijeLista listaRezervacija = new RezervacijeLista(new ArrayList<Rezervacije>());
		
		/**
		 * dodavanje korisnika, njihovo citanje i cuvanje
		 */
		Sluzbenici sluzbenikMarko98 = new Sluzbenici("Marko98", "98", 1, "Marko", "Zahorodni");
		IznajmljivaciVozila iznajmljivacVozilaMina89 = new IznajmljivaciVozila("Mina89", "89", 2, "Mina", "Gusman", "Republika Srbija", "0641571023");
		IznajmljivaciVozila iznajmljivacVozilaDavid98 = new IznajmljivaciVozila("David98", "98", 3, "David", "Zahorodni", "Republika Srbija", "0612838162");
		IznajmljivaciVozila iznajmljivacVozilaMaja2001 = new IznajmljivaciVozila("Maja2001", "2001", 4, "Maja", "Zahorodni", "Republika Srbija", "0655172051");
		
		listaKorisnika.add(sluzbenikMarko98);
		listaKorisnika.add(iznajmljivacVozilaMina89);
		listaKorisnika.add(iznajmljivacVozilaDavid98);
		listaKorisnika.add(iznajmljivacVozilaMaja2001);
		
		ObjectMapper omKorisnici = new ObjectMapper();
		omKorisnici.enable(SerializationFeature.INDENT_OUTPUT);
		
		File fajlKorisnici = new File("korisnici.json");
		
		omKorisnici.writeValue(fajlKorisnici, listaKorisnika);
		
//		KorisniciLista listaK = omKorisnici.readValue(fajlKorisnici, KorisniciLista.class);
		
		/**
		 * servisiranja, njihovo citanje i cuvanje
		 */
		// Biciklovi - servisiranja
		ArrayList<Servisiranja> servisiranjeBicikl1 = new ArrayList<Servisiranja>();
		
		Servisiranja servisiranje1Bicikl1 = new Servisiranja("NS000-SR", "2017-04-11", 50.0);
		Servisiranja servisiranje2Bicikl1 = new Servisiranja("NS000-SR", "2018-04-11", 100.0);
		servisiranjeBicikl1.add(servisiranje1Bicikl1);
		servisiranjeBicikl1.add(servisiranje2Bicikl1);
		
		ArrayList<Servisiranja> servisiranjeBicikl2 = new ArrayList<Servisiranja>();
		
		ArrayList<Servisiranja> servisiranjeBicikl3 = new ArrayList<Servisiranja>();
		Servisiranja servisiranje1Bicikl3 = new Servisiranja("NS456-JO", "2018-01-11", 50.0);
		servisiranjeBicikl3.add(servisiranje1Bicikl3);
		
		ArrayList<Servisiranja> servisiranjeBicikl4 = new ArrayList<Servisiranja>();
		
		// Putnicka Vozila - servisiranja
		ArrayList<Servisiranja> servisiranjePutnickoVozilo1 = new ArrayList<Servisiranja>();
		Servisiranja servisiranje1PutnickoVozilo1 = new Servisiranja("NS111-JD", "2017-08-01", 800.0);
		servisiranjePutnickoVozilo1.add(servisiranje1PutnickoVozilo1);
		
		ArrayList<Servisiranja> servisiranjePutnickoVozilo2 = new ArrayList<Servisiranja>();
		
		ArrayList<Servisiranja> servisiranjePutnickoVozilo3 = new ArrayList<Servisiranja>();
		
		// Teretna Vozila - servisiranja
		ArrayList<Servisiranja> servisiranjeTeretnoVozilo1 = new ArrayList<Servisiranja>();
		Servisiranja servisiranje1TeretnoVozilo1 = new Servisiranja("NS254-ED", "2018-02-04", 1000.0);
		servisiranjeTeretnoVozilo1.add(servisiranje1TeretnoVozilo1);
		
		ArrayList<Servisiranja> servisiranjeTeretnoVozilo2 = new ArrayList<Servisiranja>();
		
		ArrayList<Servisiranja> servisiranjeTeretnoVozilo3 = new ArrayList<Servisiranja>();
		
		listaServisiranja.add(servisiranje1Bicikl1);
		listaServisiranja.add(servisiranje2Bicikl1);
		listaServisiranja.add(servisiranje1Bicikl3);
		listaServisiranja.add(servisiranje1PutnickoVozilo1);
		listaServisiranja.add(servisiranje1TeretnoVozilo1);

		ObjectMapper omServisiranja = new ObjectMapper();
		omServisiranja.enable(SerializationFeature.INDENT_OUTPUT);
		
		File fajlServisiranja = new File("servisiranja.json");
		
		omServisiranja.writeValue(fajlServisiranja, listaServisiranja);
		
//		ServisiranjaLista listaS = omServisiranja.readValue(fajlServisiranja, ServisiranjaLista.class);
	
		/**
		 * goriva
		 */
		Goriva gorivo1 = new Goriva("EVRO DIZEL", 150.0);
		Goriva gorivo2 = new Goriva("EVRO PREMIJUM BMB 95", 170.0);
		
		listaGoriva.add(gorivo1);
		listaGoriva.add(gorivo2);
		
		ObjectMapper omGoriva = new ObjectMapper();
		omGoriva.enable(SerializationFeature.INDENT_OUTPUT);
		
		File fajlGoriva = new File("goriva.json");
		
		omGoriva.writeValue(fajlGoriva, listaGoriva);
		
//		GorivaLista listaG = omGoriva.readValue(fajlGoriva, GorivaLista.class);
		
		/**
		 * rezervacije, njihovo citanje i cuvanje
		 */

		Rezervacije rezervacija1BiciklNS444JK = new Rezervacije("David98", "2018-05-15", "2018-05-24", 13500.0, false);
		ArrayList<Rezervacije> rezervacijeBiciklNS444JK = new ArrayList<Rezervacije>();
		rezervacijeBiciklNS444JK.add(rezervacija1BiciklNS444JK);
		
		Rezervacije rezervacija1BiciklNS000SR = new Rezervacije("Mina89", "2018-06-15", "2018-08-24", 12500.0, false);
		Rezervacije rezervacija2BiciklNS000SR = new Rezervacije("Mina89", "2018-09-15", "2018-09-24", 12500.0, true);
		ArrayList<Rezervacije> rezervacijeBiciklNS000SR = new ArrayList<Rezervacije>();
		rezervacijeBiciklNS000SR.add(rezervacija1BiciklNS000SR);
		rezervacijeBiciklNS000SR.add(rezervacija2BiciklNS000SR);
		
		ArrayList<Rezervacije> rezervacijeBiciklNS456JO = new ArrayList<Rezervacije>();
		
		ArrayList<Rezervacije> rezervacijeBiciklNS123BL = new ArrayList<Rezervacije>();
		
		Rezervacije rezervacijaPutnickoVoziloNS111JD = new Rezervacije("Mina89", "2018-08-15", "2018-09-24", 15000.0, true);
		ArrayList<Rezervacije> rezervacijePutnickoVoziloNS111JD  = new ArrayList<Rezervacije>();
		rezervacijePutnickoVoziloNS111JD.add(rezervacijaPutnickoVoziloNS111JD);
		
		ArrayList<Rezervacije> rezervacijePutnickoVoziloNS121MK = new ArrayList<Rezervacije>();

		ArrayList<Rezervacije> rezervacijePutnickoVoziloNS123PV = new ArrayList<Rezervacije>();
	
		Rezervacije rezervacijaTeretnoVoziloNS254ED = new Rezervacije("Mina89", "2018-10-15", "2018-10-24", 12500.0, false);
		ArrayList<Rezervacije> rezervacijeTeretnoVoziloNS254ED  = new ArrayList<Rezervacije>();
		rezervacijeTeretnoVoziloNS254ED.add(rezervacijaTeretnoVoziloNS254ED);
		
		Rezervacije rezervacijaTeretnoVoziloNS333ED = new Rezervacije("Mina89", "2018-07-15", "2018-08-20", 12500.0, false);
		ArrayList<Rezervacije> rezervacijeTeretnoVoziloNS333ED  = new ArrayList<Rezervacije>();
		rezervacijeTeretnoVoziloNS333ED.add(rezervacijaTeretnoVoziloNS333ED);
		
		ArrayList<Rezervacije> rezervacijeTeretnoVoziloNS123TV  = new ArrayList<Rezervacije>();

		listaRezervacija.add(rezervacija1BiciklNS444JK);
		listaRezervacija.add(rezervacija1BiciklNS000SR);
		listaRezervacija.add(rezervacija2BiciklNS000SR);
		listaRezervacija.add(rezervacijaPutnickoVoziloNS111JD);
		listaRezervacija.add(rezervacijaTeretnoVoziloNS254ED);
		listaRezervacija.add(rezervacijaTeretnoVoziloNS333ED);
		
		ObjectMapper omRezervacije = new ObjectMapper();
		omRezervacije.enable(SerializationFeature.INDENT_OUTPUT);
		
		File fajlRezervacije = new File("rezervacije.json");
		
		omRezervacije.writeValue(fajlRezervacije, listaRezervacija);
		
//		RezervacijeLista listaR = omRezervacije.readValue(fajlRezervacije, RezervacijeLista.class);
		
		/**
		 * dodavanje vozila, njihovo citanje i cuvanje
		 */
		Bicikli biciklNS000SR = new Bicikli("NS000-SR", null, servisiranjeBicikl1, rezervacijeBiciklNS000SR, 0, 130.5, 50.0, 500.0, 400.0, 1, 0, false);
		Bicikli biciklNS444JK = new Bicikli("NS444-JK", null, servisiranjeBicikl2, rezervacijeBiciklNS444JK, 0, 20.0, 50.0, 500.0, 600.0, 1, 0, false);
		Bicikli biciklNS456JO = new Bicikli("NS456-JO", null, servisiranjeBicikl3, rezervacijeBiciklNS456JO, 0, 75.0, 50.0, 500.0, 500.0, 1, 0, false);
		Bicikli biciklNS123BL = new Bicikli("NS123-BL", null, servisiranjeBicikl4, rezervacijeBiciklNS123BL, 0, 0.0, 60.0, 500.0, 300.0, 1, 0, false);
		
		listaVozila.add(biciklNS000SR);
		listaVozila.add(biciklNS444JK);
		listaVozila.add(biciklNS456JO);
		listaVozila.add(biciklNS123BL);
		
		PutnickoVozilo putnickoVoziloNS111JD = new PutnickoVozilo("NS111-JD", gorivo1, servisiranjePutnickoVozilo1, rezervacijePutnickoVoziloNS111JD, 8.0, 1000.5, 800.0, 800.0, 800.0, 5, 5, false);
		PutnickoVozilo putnickoVoziloNS121MK = new PutnickoVozilo("NS121-MK", gorivo1, servisiranjePutnickoVozilo2, rezervacijePutnickoVoziloNS121MK, 8.0, 550.0, 800.0, 800.0, 900.0, 5, 5, false);
		PutnickoVozilo putnickoVoziloNS123PV = new PutnickoVozilo("NS123-PV", gorivo2, servisiranjePutnickoVozilo3, rezervacijePutnickoVoziloNS123PV, 6.0, 0.0, 800.0, 800.0, 700.0, 4, 4, true);
		
		listaVozila.add(putnickoVoziloNS111JD);
		listaVozila.add(putnickoVoziloNS121MK);
		listaVozila.add(putnickoVoziloNS123PV);
		
		TeretnoVozilo teretnoVoziloNS254ED = new TeretnoVozilo("NS254-ED", gorivo2, servisiranjeTeretnoVozilo1, rezervacijeTeretnoVoziloNS254ED, 15.0, 1500.5, 1000.0, 1000.0, 1000.0, 5, 5, false, 500.0, 5.75);
		TeretnoVozilo teretnoVoziloNS333ED = new TeretnoVozilo("NS333-ED", gorivo2, servisiranjeTeretnoVozilo2, rezervacijeTeretnoVoziloNS333ED, 17.0, 200.5, 1000.0, 1400.0, 1200.0, 5, 5, false, 800.0, 6.75);
		TeretnoVozilo teretnoVoziloNS123TV = new TeretnoVozilo("NS123-TV", gorivo1, servisiranjeTeretnoVozilo3, rezervacijeTeretnoVoziloNS123TV, 50.0, 0.0, 2000.0, 1500.0, 1000.0, 3, 2, false, 2000.0, 6.0);

		listaVozila.add(teretnoVoziloNS254ED);
		listaVozila.add(teretnoVoziloNS333ED);
		listaVozila.add(teretnoVoziloNS123TV);
		
		ObjectMapper omVozila = new ObjectMapper();
		omVozila.enable(SerializationFeature.INDENT_OUTPUT);
		
		File fajlVozila = new File("vozila.json");
		
		omVozila.writeValue(fajlVozila, listaVozila);
		
//		VozilaLista listaV = omVozila.readValue(fajlVozila, VozilaLista.class);
		
	}
}
