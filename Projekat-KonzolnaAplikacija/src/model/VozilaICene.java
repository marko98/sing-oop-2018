package model;

import java.util.Comparator;

public class VozilaICene {
	private double cena;
	private Vozila vozilo;
	
	public VozilaICene() {
		super();
	}
	
	public VozilaICene(double cena, Vozila vozilo) {
		super();
		this.cena = cena;
		this.vozilo = vozilo;
	}

	public double getCena() {
		return cena;
	}

	public void setCena(double cena) {
		this.cena = cena;
	}

	public Vozila getVozilo() {
		return vozilo;
	}

	public void setVozilo(Vozila vozilo) {
		this.vozilo = vozilo;
	}
	
	// COMPARATOR---------------------------
	
    public static java.util.Comparator<VozilaICene> comparator = new Comparator<VozilaICene>() {

        public int compare(VozilaICene vc1, VozilaICene vc2) {
    		if(vc1.getCena() < vc2.getCena()) {
    			return -1;
    		} else if(vc1.getCena() > vc2.getCena()) {
    			return 1;
    		} else {
    			return 0;
    		}
    	}
    };
    
    // -------------------------------------

}
