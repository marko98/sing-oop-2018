/**
 * 
 */
package model;

import com.fasterxml.jackson.annotation.JsonSubTypes;
import com.fasterxml.jackson.annotation.JsonTypeInfo;
import com.fasterxml.jackson.annotation.JsonSubTypes.Type;


@JsonTypeInfo(
	    use = JsonTypeInfo.Id.NAME, 
		include = JsonTypeInfo.As.PROPERTY, 
	    property = "type")
	@JsonSubTypes({ 
		@Type(value = IznajmljivaciVozila.class, name = "iznajmljivac vozila"), 
		@Type(value = Sluzbenici.class, name = "sluzbenik")
	})
public abstract class Korisnici {
	private String korisnickoIme;
	private String sifra;
	private Integer jmbg; 
	private String ime; 
	private String prezime; 
	
	public Korisnici() {
		super();
	}
	public Korisnici(String korisnickoIme, String sifra, Integer jmbg, String ime, String prezime) {
		super();
		this.korisnickoIme = korisnickoIme;
		this.sifra = sifra;
		this.jmbg = jmbg;
		this.ime = ime;
		this.prezime = prezime;
	}
	
	@Override
	public String toString() {
		return "Korisnici [korisnickoIme=" + korisnickoIme + ", sifra=" + sifra + ", JMBG=" + jmbg + ", ime=" + ime
				+ ", prezime=" + prezime + "]";
	}
	public String getKorisnickoIme() {
		return korisnickoIme;
	}
	public void setKorisnickoIme(String korisnickoIme) {
		this.korisnickoIme = korisnickoIme;
	}
	public String getSifra() {
		return sifra;
	}
	public void setSifra(String sifra) {
		this.sifra = sifra;
	}
	public Integer getJmbg() {
		return jmbg;
	}
	public void setJmbg(Integer jmbg) {
		this.jmbg = jmbg;
	}
	public String getIme() {
		return ime;
	}
	public void setIme(String ime) {
		this.ime = ime;
	}
	public String getPrezime() {
		return prezime;
	}
	public void setPrezime(String prezime) {
		this.prezime = prezime;
	}

}
