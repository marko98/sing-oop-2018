package model;

public class Sluzbenici extends Korisnici {

	public Sluzbenici() {
		super();
	}
	
	public Sluzbenici(String korisnickoIme, String sifra, Integer jmbg, String ime, String prezime) {
		super(korisnickoIme, sifra, jmbg, ime, prezime);
	}

	@Override
	public String toString() {
		return "Sluzbenici []";
	}
}
