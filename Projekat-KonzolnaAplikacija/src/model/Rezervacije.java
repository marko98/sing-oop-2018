package model;

public class Rezervacije {
//	private IznajmljivaciVozila iznajmljivac;
//	private Vozila rezervisanoVozilo;
	private String datumPocetkaRezervacije, datumKrajaRezervacije, korisnickoImeIznajmljivaca;
	private double cenaRezervacije;
	private Boolean logickiObrisanaRezervacija;
	
	public Rezervacije() {
		super();
	}
	
	public Rezervacije(String korisnickoImeIznajmljivaca, String datumPocetkaRezervacije, String datumKrajaRezervacije, 
			double cenaRezervacije, Boolean logickiObrisanaRezervacija) {
		super();
		this.korisnickoImeIznajmljivaca = korisnickoImeIznajmljivaca;
		this.datumPocetkaRezervacije = datumPocetkaRezervacije;
		this.datumKrajaRezervacije = datumKrajaRezervacije;
		this.cenaRezervacije = cenaRezervacije;
		this.logickiObrisanaRezervacija = logickiObrisanaRezervacija;
	}
	
	public void obrisiRezervaciju(Rezervacije rezervacija) {
		rezervacija.setLogickiObrisanaRezervacija(true);
	} 
	
	@Override
	public String toString() {
		return "Rezervacije [korisnickoImeIznajmljivaca=" + korisnickoImeIznajmljivaca
				 + ", datumPocetkaRezervacije=" + datumPocetkaRezervacije + ", datumKrajaRezervacije="
				+ datumKrajaRezervacije + ", cenaRezervacije=" + cenaRezervacije + ", logickiObrisanaRezervacija="
				+ logickiObrisanaRezervacija + "]";
	}

	public String getKorisnickoImeIznajmljivaca() {
		return korisnickoImeIznajmljivaca;
	}

	public void setKorisnickoImeIznajmljivaca(String korisnickoImeIznajmljivaca) {
		this.korisnickoImeIznajmljivaca = korisnickoImeIznajmljivaca;
	}

	public String getDatumPocetkaRezervacije() {
		return datumPocetkaRezervacije;
	}

	public void setDatumPocetkaRezervacije(String datumPocetkaRezervacije) {
		this.datumPocetkaRezervacije = datumPocetkaRezervacije;
	}

	public String getDatumKrajaRezervacije() {
		return datumKrajaRezervacije;
	}

	public void setDatumKrajaRezervacije(String datumKrajaRezervacije) {
		this.datumKrajaRezervacije = datumKrajaRezervacije;
	}

	public double getCenaRezervacije() {
		return cenaRezervacije;
	}

	public void setCenaRezervacije(double cenaRezervacije) {
		this.cenaRezervacije = cenaRezervacije;
	}

	public Boolean getLogickiObrisanaRezervacija() {
		return logickiObrisanaRezervacija;
	}

	public void setLogickiObrisanaRezervacija(Boolean logickiObrisanaRezervacija) {
		this.logickiObrisanaRezervacija = logickiObrisanaRezervacija;
	}
	
}
