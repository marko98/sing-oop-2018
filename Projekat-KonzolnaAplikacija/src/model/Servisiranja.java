package model;


public class Servisiranja {
	private String registracioniBrojServisiranje;
	private String datumServisiranja;
	private double brPredjenihKmUTrenutkuServisiranja;
	
	public Servisiranja() {
		super();
	}
	
	public Servisiranja(String registracioniBrojServisiranje, String datumServisiranja, double brPredjenihKmUTrenutkuServisiranja) {
		super();
		this.registracioniBrojServisiranje = registracioniBrojServisiranje;
		this.datumServisiranja = datumServisiranja;
		this.brPredjenihKmUTrenutkuServisiranja = brPredjenihKmUTrenutkuServisiranja;
	}

	@Override
	public String toString() {
		return "Servisiranja [registracioniBrojServisiranje=" + registracioniBrojServisiranje + ", datumServisiranja=" + datumServisiranja
				+ ", brPredjenihKmUTrenutkuServisiranja=" + brPredjenihKmUTrenutkuServisiranja + "]";
	}

	public String getRegistracioniBrojServisiranje() {
		return registracioniBrojServisiranje;
	}

	public void setRegistracioniBrojServisiranje(String registracioniBrojServisiranje) {
		this.registracioniBrojServisiranje = registracioniBrojServisiranje;
	}

	public String getDatumServisiranja() {
		return datumServisiranja;
	}

	public void setDatumServisiranja(String datumServisiranja) {
		this.datumServisiranja = datumServisiranja;
	}

	public double getBrPredjenihKmUTrenutkuServisiranja() {
		return brPredjenihKmUTrenutkuServisiranja;
	}

	public void setBrPredjenihKmUTrenutkuServisiranja(double brPredjenihKmUTrenutkuServisiranja) {
		this.brPredjenihKmUTrenutkuServisiranja = brPredjenihKmUTrenutkuServisiranja;
	}
	
}
