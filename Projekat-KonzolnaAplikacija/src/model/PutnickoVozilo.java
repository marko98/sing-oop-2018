package model;

import java.util.List;

public class PutnickoVozilo extends Vozila {

	public PutnickoVozilo() {
		super();
	}
	
	public PutnickoVozilo(String registracioniBroj, Goriva gorivo, List<Servisiranja> servisiranje, List<Rezervacije> rezervacije, double potrosnjaGoriva,
			double predjeniKm, double brKmPreServisiranja, double cenaServisiranja, double cenaIznajmljivanjaPoDanu,
			Integer brojSedista, Integer brojVrata, Boolean logickiObrisanoVozilo) {
		super(registracioniBroj, gorivo, servisiranje, rezervacije, potrosnjaGoriva, predjeniKm, brKmPreServisiranja, cenaServisiranja,
				cenaIznajmljivanjaPoDanu, brojSedista, brojVrata, logickiObrisanoVozilo);
	}

	@Override
	public String toString() {
		return "PutnickoVozilo []";
	}
	
	@Override
	public double cenaPrelaskaRazdaljine(double kilometri) {
		double potrosnjaGorivaPoJednomKm = this.getPotrosnjaGoriva() / 100;
		double potrosnjaLitaraGoriva = potrosnjaGorivaPoJednomKm * kilometri;
		double cenaGorivaPoLitri = this.getGorivo().getCenaGoriva();
		double cenaPrelaskaRazdaljineGorivo = potrosnjaLitaraGoriva * cenaGorivaPoLitri;
		return cenaPrelaskaRazdaljineGorivo;
	}
	
	@Override
	public boolean sledeciServis(double kilometri) {
		if (!this.getServisiranje().isEmpty()) {
//			System.out.println("Postoje servisiranja na vozilu.");
			double brojPredjenihKm = this.getPredjeniKm();
//			System.out.println("Predjeni km putnickog vozila su: " + brojPredjenihKm);
			
			List<Servisiranja> servisiranje = this.getServisiranje();
			int velicinaListe = servisiranje.size();
			double kmUTrenutkuPoslednjegServisa = servisiranje.get(velicinaListe-1).getBrPredjenihKmUTrenutkuServisiranja();
//			System.out.println("Broj predjenih kilometara u trenutku poslednjeg servisiranja su: " + kmUTrenutkuPoslednjegServisa); 
			
			double brKmDoPrvogSledecegServisiranja = this.getBrKmPreServisiranja() - (brojPredjenihKm - kmUTrenutkuPoslednjegServisa);
//			System.out.println("Broj km do sledeceg servisiranja je: " + brKmDoPrvogSledecegServisiranja);
			
			if (kilometri >= brKmDoPrvogSledecegServisiranja) {
//				System.out.println("Vozilo ce morati da ide na servis.");
				boolean servisJePotreban = true;
				return servisJePotreban;
			} else {
//				System.out.println("Nece biti potrebe za servisiranjem.");
				boolean servisJePotreban = false;
				return servisJePotreban;
			}
			
		} else {
//			System.out.println("Ne postoje servisiranja na vozilu.");
			double brojPredjenihKm = this.getPredjeniKm();
//			System.out.println("Predjeni km putnickog vozila su: " + brojPredjenihKm);
			
			if((kilometri + brojPredjenihKm) >= this.getBrKmPreServisiranja()) {
//				System.out.println("Vozilo ce morati da ide na servis.");
				boolean servisJePotreban = true;
				return servisJePotreban;
			} else {
//				System.out.println("Nece biti potrebe za servisiranjem.");
				boolean servisJePotreban = false;
				return servisJePotreban;
			}
		}
	}
	
}
