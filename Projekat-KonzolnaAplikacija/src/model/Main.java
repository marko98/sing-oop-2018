package model;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import funkcionalnosti.Login;
import funkcionalnosti.Meni;
import funkcionalnosti.ProveraBroj;
import run.Run;

public class Main {

	public static void main(String[] args) throws JsonParseException, JsonMappingException, IOException {
		boolean pokreniProgram = true;
		
		while(pokreniProgram) {
			boolean korisnikJeUlogovan = true;
			System.out.println("Vaše opcije su: \n1. Ulogujte se \n2. Ugasi program \n3. Vratite početne vrednosti "
					+ "\n4. Sluzbenici \n5. Iznajmljivači vozila " );
			
			
			while(korisnikJeUlogovan) {
				int opcija = ProveraBroj.proveraBrojInt("Unesite broj ispred jedne od ponuđenih opcija: ");
				if(opcija == 1) {
					Korisnici korisnik = Login.logovanje();
					
					while(korisnikJeUlogovan) {
						korisnikJeUlogovan = Meni.meni(korisnik);
					}
				} else if(opcija == 2) {
					System.out.println("Program je ugašen.");
					pokreniProgram = false;
					break;
				} else if(opcija == 3) {
					Run.main();
					korisnikJeUlogovan = false;
					System.out.println("Vraćene su početne vrednosti.\n");
				} else if(opcija == 4) {
					funkcionalnosti.PrikaziKorisnike.prikaziKorisnike("Sluzbenici");
					korisnikJeUlogovan = false;
				} else if(opcija == 5) {
					funkcionalnosti.PrikaziKorisnike.prikaziKorisnike("Iznajmljivaci");
					korisnikJeUlogovan = false;
				}
			}
		}
		
	}
	
}

