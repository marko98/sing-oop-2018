package model;

public class IznajmljivaciVozila extends Korisnici {
	private String drzavljanstvo;
	private String brojTelefona;

	public IznajmljivaciVozila() {
		super();
	}
	
	public IznajmljivaciVozila(String korisnickoIme, String sifra, Integer jmbg, String ime, String prezime, String drzavljanstvo, String brojTelefona) {
		super(korisnickoIme, sifra, jmbg, ime, prezime);
		this.drzavljanstvo = drzavljanstvo;
		this.brojTelefona = brojTelefona;
	}

	@Override
	public String toString() {
		return "IznajmljivaciVozila [drzavljanstvo=" + drzavljanstvo + ", brojTelefona=" + brojTelefona + "]";
	}

	public String getDrzavljanstvo() {
		return drzavljanstvo;
	}

	public void setDrzavljanstvo(String drzavljanstvo) {
		this.drzavljanstvo = drzavljanstvo;
	}

	public String getBrojTelefona() {
		return brojTelefona;
	}
	public void setBrojTelefona(String brojTelefona) {
		this.brojTelefona = brojTelefona;
	}
}
