package model;

public class Goriva {
	private String nazivGoriva;
	private double cenaGoriva;

	public Goriva() {
		super();
	}
	
	public Goriva(String nazivGoriva, double cenaGoriva) {
		super();
		this.nazivGoriva = nazivGoriva;
		this.cenaGoriva = cenaGoriva;
	}

	@Override
	public String toString() {
		return "Goriva [nazivGoriva=" + nazivGoriva + ", cenaGoriva=" + cenaGoriva + "]";
	}

	public String getNazivGoriva() {
		return nazivGoriva;
	}

	public void setNazivGoriva(String nazivGoriva) {
		this.nazivGoriva = nazivGoriva;
	}

	public double getCenaGoriva() {
		return cenaGoriva;
	}

	public void setCenaGoriva(double cenaGoriva) {
		this.cenaGoriva = cenaGoriva;
	}

}
